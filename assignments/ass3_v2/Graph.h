#ifndef _GRAPH_H
#define _GRAPH_H

#include <iostream>     // IO
#include <memory>       // pointers
#include <algorithm>    // sort, equal_range
#include <map>          // map
#include <list>         // lists
#include <set>
#include <stdexcept>    // std exceptions

namespace cs6771 {

    // NodeIterator
    template<typename N>
        class NodeIterator
        {
            public:
                
                // define the iterator traits /properties
                typedef std::input_iterator_tag          iterator_category;
                typedef std::ptrdiff_t                   difference_type;
                typedef const N                                value_type;
                typedef const N*                               pointer;
                typedef const N&                               reference;

                reference operator*() const;        
                pointer operator->() const;        

                //preincrement
                NodeIterator& operator++(); 
                
                bool operator==(const NodeIterator& other) const;
                bool operator!=(const NodeIterator& other) const;

                inline bool equalN(const N& a, const N& b) const {return !( a < b || b < a);}
                
                NodeIterator(std::shared_ptr<std::list<std::shared_ptr<N> > > n = nullptr);

            private:
                std::shared_ptr <std::list<std::shared_ptr<N> > > sortedPointers;       

        };  // END NODE-ITERATOR TEMPLATE

        // constructor
        template <typename N>       
            NodeIterator<N>::NodeIterator(std::shared_ptr<std::list<std::shared_ptr<N> > > n) :sortedPointers{n} {}
                
            
        template<typename N>
            typename NodeIterator<N>::reference NodeIterator<N>::operator*() const
            {
                return *(sortedPointers->front());
            }

        template <typename N> 
            typename NodeIterator<N>::pointer NodeIterator<N>::operator->() const
            {
                return &(operator*());
            }

        
        template <typename N> 
            bool NodeIterator<N>::operator==(const NodeIterator& other) const
            {
                if ( !( sortedPointers < other.sortedPointers || other.sortedPointers < sortedPointers) ) return true;
                if (sortedPointers == nullptr || other.sortedPointers == nullptr) return false;

                return !( *sortedPointers < *other.sortedPointers || *other.sortedPointers < *sortedPointers);
            }

        template <typename N> 
            bool NodeIterator<N>::operator!=(const NodeIterator& other) const
            {
                return !operator==(other);
            }

        template <typename N> 
            NodeIterator<N>& NodeIterator<N>::operator++()
            {
                sortedPointers->pop_front();
                if (sortedPointers->size() == 0) sortedPointers = nullptr;
                return *this;
            }


    // EdgeIterator
    template<typename N, typename E>
        class EdgeIterator
        {
            public:
                
                // define the iterator traits

                typedef std::input_iterator_tag          iterator_category;
                typedef std::ptrdiff_t                   difference_type;
                typedef const std::pair<N,E>                  value_type;
                typedef const std::pair<N,E>*                 pointer;
                typedef const std::pair<N,E>&                 reference;

                reference operator*() const;        
                pointer operator->() const;        

                //preincrement
                EdgeIterator& operator++(); 
                
                // post increment (does not return a reference)
                EdgeIterator operator++(int);

                bool operator==(const EdgeIterator& other) const;
                bool operator!=(const EdgeIterator& other) const;

                inline bool equalN(const N& a, const N& b) const {return !( a < b || b < a);}
                inline bool equalE(const E& a, const E& b) const {return !( a < b || b < a);};

                EdgeIterator(std::shared_ptr<std::list<std::pair<N, E> > > v = nullptr);

            private:
                std::shared_ptr<std::list<std::pair<N, E> > > sortedPointers;       



        };  // END EDGEITERATOR TEMPLATE

        // constructor
        template <typename N, typename E>       
            EdgeIterator<N, E>::EdgeIterator(std::shared_ptr<std::list<std::pair<N, E> > > v):sortedPointers{v} {}
                
            
        template <typename N, typename E>
            typename EdgeIterator<N, E>::reference EdgeIterator<N, E>::operator*() const
            {
                return sortedPointers->front();
            }

        template <typename N, typename E> 
            typename EdgeIterator<N, E>::pointer EdgeIterator<N, E>::operator->() const
            {
                return &(operator*());
            }

        
        template <typename N, typename E> 
            bool EdgeIterator<N, E>::operator==(const EdgeIterator& other) const
            {
                if ( !( sortedPointers < other.sortedPointers || other.sortedPointers < sortedPointers) ) return true;
                if (sortedPointers == nullptr || other.sortedPointers == nullptr) return false;

                return !( *sortedPointers < *other.sortedPointers || *other.sortedPointers < *sortedPointers);
                    
            }

        template <typename N, typename E> 
            bool EdgeIterator<N, E>::operator!=(const EdgeIterator& other) const
            {
                return !operator==(other);
            }

        template <typename N, typename E> 
            EdgeIterator<N, E>& EdgeIterator<N, E>::operator++()
            {
                sortedPointers->pop_front();
                if (sortedPointers->size() == 0) sortedPointers = nullptr;
                return *this;
            }


    template <typename N, typename E>
        class Graph
        {

            // public member variables and functions
            public:
 
                Graph(){};
                Graph(const Graph&);
                Graph(const Graph&&);
                Graph& operator=(const Graph&);
                Graph& operator=(const Graph&&);

                bool addNode(const N&);
                bool addEdge(const N&, const N&, const E&);
                bool replace(const N&, const N&);
                void mergeReplace(const N&, const N&);
                void deleteNode(const N&) noexcept;
                void deleteEdge(const N&, const N&, const E&) noexcept;
                bool isNode(const N&) const;
                bool isConnected(const N&, const N&) const;
                void clear() noexcept;
                NodeIterator<N> begin() const;
                NodeIterator<N> end() const;
                EdgeIterator<N, E> edgeIteratorBegin(const N&) const;
                EdgeIterator<N, E> edgeIteratorEnd() const;
                void printNodes() const;
                void printEdges(const N&) const;

            // private member functions and variables
            private:

                inline bool equalN(const N& a, const N& b) const {return !( a < b || b < a);}
                inline bool equalE(const E& a, const E& b) const {return !( a < b || b < a);};

                // private inner value-like class for Edge
                class Edge
                {

                    public:
                        Edge(const std::shared_ptr<N>&, const std::shared_ptr<N>&, const std::shared_ptr<E>&);
                        std::weak_ptr<N> origin_ptr_;
                        std::weak_ptr<N> dest_ptr_;
                        std::shared_ptr<E> value_ptr_;

                        bool operator< (const Edge& rhs) const;

                    private:
                };

                // private inner value-like class for Node
                class Node
                {

                    public:
                        Node(const N& n);
                        bool operator< (const Node& rhs) const;
                        
                        std::shared_ptr<N> origin_ptr_;
                        std::set<Edge> connections_;

                    private:
                };

                // node set
                std::map<N, std::shared_ptr<Node> > nodes_;

                // map of E values - avoid duplication issues
                std::map<E, std::weak_ptr<E> > E_map_;


        }; // END GRAPH TEMPLATE



    // -------------------------------------------------------------------------------
    // Implementation of Graph class
    // -------------------------------------------------------------------------------

        // copy constructor
        template <typename N, typename E>
            Graph<N,E>::Graph(const Graph &g)
            {
                // populate N map
                for (auto oldNode = g.nodes_.begin(); oldNode!= g.nodes_.end(); ++ oldNode){
                    auto sh_n = std::make_shared<N>(*oldNode->second->origin_ptr_);
                    Node newNode(*oldNode->second->origin_ptr_);

                    std::pair<N, std::shared_ptr<Node>> newPair(*oldNode->second->origin_ptr_, std::make_shared<Node>(newNode));
                    nodes_.insert(newPair);
                }

                // now that N assets are in place, update edges for each node.
                for (auto oldNode = g.nodes_.begin(); oldNode!= g.nodes_.end(); ++ oldNode){
                    
                    auto new_node_it = nodes_.find(*oldNode->second->origin_ptr_);

                    for(auto old_edge = oldNode->second->connections_.begin(); old_edge != oldNode->second->connections_.end(); ++old_edge){
                        // get appropriate pointers for each edge
                        auto old_origin_wp = old_edge->origin_ptr_;
                        auto old_dest_wp = old_edge->dest_ptr_;

                        auto old_origin_sp = old_origin_wp.lock();
                        auto old_dest_sp = old_dest_wp.lock();

                        // generate appropriate weak pointers from new N set 
                        std::shared_ptr<N> new_origin_sp = nodes_.find(*old_origin_sp)->second->origin_ptr_;
                        std::shared_ptr<N> new_dest_sp = nodes_.find(*old_dest_sp)->second->origin_ptr_;

                        // need to grab or insert the E value
                        std::shared_ptr<E> new_e_sp;
                        if(E_map_.find(*(old_edge->value_ptr_)) == E_map_.end()){
                            // E is not in set
                            new_e_sp = std::make_shared<E>(*(old_edge->value_ptr_));
                            std::weak_ptr<E> new_value_wp = new_e_sp;
                            
                            E_map_.insert(std::pair<E, std::weak_ptr<E> >( *(old_edge->value_ptr_), new_value_wp) );

                        } else {
                            //E was in set
                            new_e_sp = E_map_.find(*(old_edge->value_ptr_))->second.lock();
                        }

                        // create a new edge with the above assets
                        Edge newEdge(new_origin_sp, new_dest_sp, new_e_sp);

                        // push new edge to the current node connection set
                        new_node_it->second->connections_.insert(newEdge);
                    }
                }
            }

        //move  constructor
        template <typename N, typename E>
            Graph<N,E>::Graph(const Graph &&g)
            {
                nodes_ = std::move(g.nodes_);
                E_map_ = std::move(g.E_map_);    
            }

        // copy assignment
        template <typename N, typename E>
            Graph<N,E>& Graph<N,E>::operator=(const Graph &g)
            {
                Graph newGraph(g);
                if(this != &g){
                    nodes_ = newGraph.nodes_;
                    E_map_=newGraph.E_map_;
                }
                return *this;
            }

        // move assignment
        template <typename N, typename E>
            Graph<N,E>& Graph<N,E>::operator=(const Graph &&g)
            {
                if(this != &g){
                   nodes_ = std::move(g.nodes_);
                   E_map_ = std::move(g.E_map_);
                }
                return *this;
            }

        // add a node
        template <typename N, typename E>
            bool Graph<N,E>::addNode(const N& n)
            {
                std::pair<typename std::map<N, std::shared_ptr<Node> >::iterator, bool> N_it = nodes_.insert(std::make_pair(n, std::make_shared<Node>( Node(n) )));
                return N_it.second;
            }

        // add an edge
        template <typename N, typename E>
            bool Graph<N,E>::addEdge(const N& origin, const N& dest, const E& e)
            {

                // find iterator to/check if elements are in Node set
                auto origin_it = nodes_.find(origin);
                if(origin_it == nodes_.end()) throw std::runtime_error("ERROR: Graph::addEdge(): origin not in set.");
                auto dest_it = nodes_.find(dest);
                if(dest_it == nodes_.end()) throw std::runtime_error("ERROR: Graph::addEdge(): dest not in set.");

                auto E_it = E_map_.find(e);

                std::shared_ptr<E> sh_e = std::make_shared<E>(e);
                std::weak_ptr<E> wp_e = sh_e;
                
                if(E_it == E_map_.end()){
                    E_map_.insert(std::make_pair(e, wp_e));    
                }

                auto E_it_2 = E_map_.find(e);
                auto e_sp = E_it_2->second.lock();
                
                // create a new edge object
                Edge newEdge{origin_it->second->origin_ptr_, dest_it->second->origin_ptr_, e_sp};

                // update the origin node with new edge 
                auto is_inserted_node = origin_it->second->connections_.insert(newEdge);

                // need to check for expired edges and remove/reinsert edge if they are expired
                // this will stop a previous removed edge from stopping a new edge from being inserted
                if(is_inserted_node.second == false){
                    if( is_inserted_node.first->origin_ptr_.expired() || is_inserted_node.first->dest_ptr_.expired() ){
                        origin_it->second->connections_.erase(is_inserted_node.first);
                        origin_it->second->connections_.insert(newEdge);                        
                    }else{
                        return false;
                    }

                }

                return true;
            }


        template <typename N, typename E>
            bool Graph<N,E>::replace(const N& old_n, const N& new_n)
            {
                // check that node is in set
                auto old_n_it = nodes_.find(old_n);
                if(old_n_it == nodes_.end()) throw std::runtime_error("Graph<N,E>::replace(): old_n not in set.");
                auto new_n_it = nodes_.find(new_n);
                if(new_n_it != nodes_.end()) return false;
                
                // use pointer to modify value
                *(old_n_it->second->origin_ptr_) = new_n;
                Node newNode = *(old_n_it->second);
                nodes_.erase(old_n);
                nodes_.insert(std::make_pair(new_n, std::make_shared<Node>( newNode )));
                    
                return true;
            }


        template <typename N, typename E>
            void Graph<N,E>::mergeReplace(const N& old_n, const N& new_n)
            {

                auto old_node = nodes_.find(old_n);
                if(old_node == nodes_.end()) throw std::runtime_error("ERROR: Graph::mergeReplace(): old_n not in set.");
                auto new_node = nodes_.find(new_n);
                if(new_node == nodes_.end()) throw std::runtime_error("ERROR: Graph::mergeReplace(): new_n not in set.");

                // copy over new edge from old node
                for(auto old_it = old_node->second->connections_.begin(); old_it != old_node->second->connections_.end(); ++old_it){
                    
                    // if any edges have expired N pointers, do not add them
                    if(old_it->origin_ptr_.expired()){
                        continue;
                    } 
                    if(old_it->dest_ptr_.expired()){
                        continue;
                    }

                    auto dest_sp =  old_it->dest_ptr_.lock();
                    

                    // if the edge exists between the two merging nodes, do not add them
                    if (equalN( *dest_sp, *(new_node->second->origin_ptr_) )){
                        continue;
                    } 

                    Edge newEdge{new_node->second->origin_ptr_, dest_sp, old_it->value_ptr_};
                    
                    // insert will not insert duplicate edges
                    new_node->second->connections_.insert(newEdge);

                }

                // clean up and remove old node. 
                // (could probably not worry about reset/clear, but just to be safe)
                old_node->second->origin_ptr_.reset();
                old_node->second->connections_.clear();
                nodes_.erase(old_node);
            }


        template <typename N, typename E>
            void Graph<N,E>::deleteNode(const N& n) noexcept
            {
                auto n_it = nodes_.find(n);
                if(n_it != nodes_.end()){
                    try{
                        n_it->second->origin_ptr_.reset();
                        n_it->second->connections_.clear();
                        //n_it->second->adjacentNodes_.clear();

                        nodes_.erase(n);
                        
                    } catch(...){
                        // catch any exceptions
                    }

                }
            }

            
        template <typename N, typename E>
            void Graph<N,E>::deleteEdge(const N& origin, const N& dest, const E& e) noexcept
            {
                // find values to create dummy edge
                auto origin_it = nodes_.find(origin);
                auto dest_it = nodes_.find(dest);
                auto e_it = E_map_.find(e);
                if( (origin_it == nodes_.end()) || (dest_it == nodes_.end()) || (e_it == E_map_.end())){
                    return;
                }
 
                auto e_sp = e_it->second.lock();
                
                // create new 'dummy' edge
                Edge newEdge{origin_it->second->origin_ptr_, dest_it->second->origin_ptr_, e_sp};

                // erase dummy edge from set
                origin_it->second->connections_.erase(newEdge);

            }

                
        template <typename N, typename E>
            bool Graph<N,E>::isNode(const N& n) const
            {
                // check if set contains node with N value
                if(nodes_.find(n) == nodes_.end()) return false;
                return true;
            }


        template <typename N, typename E>
            bool Graph<N,E>::isConnected(const N& origin, const N& dest) const
            {   
                // make sure that origin and dest. nodes exist in set
                auto origin_it = nodes_.find(origin);
                if(origin_it == nodes_.end()) throw std::runtime_error("ERROR: Graph::isConnected(): origin not in set.");
                auto dest_it = nodes_.find(dest);
                if(dest_it == nodes_.end()) throw std::runtime_error("ERROR: Graph::isConencted(): dest not in set.");   

                // a dummy E ptr
                std::shared_ptr<E> dummy_ptr;
                Edge newEdge{origin_it->second->origin_ptr_, dest_it->second->origin_ptr_, dummy_ptr };
                
                // use equal range to scan through the origin nodes connections_ set, find appropriate edges.
                auto is_conn = std::equal_range(origin_it->second->connections_.begin(), origin_it->second->connections_.end(), newEdge,
                        [] (const Edge& a, const Edge& b)
                            {
                                if( a.dest_ptr_.expired() || b.dest_ptr_.expired() ){ 
                                    return false;
                                } 
                                
                                auto a_sp = a.dest_ptr_.lock();
                                auto b_sp = b.dest_ptr_.lock();

                                return  *b_sp < *a_sp;
                            }
                            );
               
                // depending on the bounds set by equal range, there may or may not be 
                // matching edges in the set
                if( (is_conn.first == origin_it->second->connections_.begin()) 
                    && (is_conn.second == origin_it->second->connections_.end())){
                    
                    //std::cout << "checking bounds of equal range" << std::endl;
                    for (auto check_it = is_conn.first; check_it != is_conn.second; ++check_it){
                        if( !(check_it->dest_ptr_.expired())){
                            //std::cout << "found unexpired pointer" << std::endl;
                            return true;  
                        } 
                    }
                    return false;
                }


                if(is_conn.first == origin_it->second->connections_.begin() 
                    && is_conn.second == origin_it->second->connections_.begin()){
                    //std::cout << "first == end" << std::endl;
                    return false;
                }


                if(is_conn.first == origin_it->second->connections_.end() ) {
                    //std::cout << "first == end" <<std::endl;
                    return false;
                }

                return true;
            
            }


        template <typename N, typename E>
            void Graph<N,E>::clear() noexcept
            {
                try{
                    nodes_.clear();
                    E_map_.clear();
                }
                catch (...){
                    // catch any exceptions that may be thrown, do nothing
                }
            }


        // iterator types
        template <typename N, typename E>
            void Graph<N,E>::printNodes() const
            {
                // take advantage of node iterator
                for (auto first = begin(); first != end(); ++first){
                    std::cout << *first << std::endl;
                } 
            }
        

        template <typename N, typename E>
            NodeIterator<N> Graph<N,E>::begin() const
            {
                std::list<std::shared_ptr<N> > sortedPointers;
                std::set<Node> sortedSet;

                for (auto elt = nodes_.begin(); elt != nodes_.end(); ++elt ){
                    sortedSet.insert(*(elt->second));
                }

                for (auto elt = sortedSet.begin(); elt != sortedSet.end(); ++elt ){
                    sortedPointers.push_back(elt->origin_ptr_);
                }
                if(sortedPointers.size() == 0) return NodeIterator<N>();
                return NodeIterator<N>(std::make_shared< std::list<std::shared_ptr<N> > >(sortedPointers));
            }
        
       
        template <typename N, typename E>
            NodeIterator<N> Graph<N,E>::end() const
            {
                return NodeIterator<N>();
            }                


        template <typename N, typename E>
            void Graph<N,E>::printEdges(const N& n) const
            {
                auto node = nodes_.find(n); 
                if(node == nodes_.end()) throw std::runtime_error("Graph<N,E>::printEdges(): n not in set.");
                
                std::cout << "Edges attached to Node " << n << std::endl;
            
                auto itStart = edgeIteratorBegin(n);
                auto itEnd =  edgeIteratorEnd();
                
                if( itStart == itEnd ){
                    std::cout << "(null)" << std::endl;
                    return;    
                }
                
                for (auto elt = itStart; elt != itEnd; ++elt){
                    std::cout << elt->first << " " << elt->second << std::endl;
                } 
            }


        template <typename N, typename E>
            EdgeIterator<N, E> Graph<N,E>::edgeIteratorBegin(const N& n) const
            {
                auto node = nodes_.find(n); 
                if(node == nodes_.end()) throw std::runtime_error("Graph<N,E>::edgeIteratorBegin(): n not in set.");

                std::list<std::pair<N, E> > sortedPairs;

                for(auto elt = node->second->connections_.begin(); elt != node->second->connections_.end(); ++elt){
                    if(elt->dest_ptr_.expired() == true) continue;
                    auto e_sh = elt->dest_ptr_.lock();
                    
                    sortedPairs.push_back(std::pair<N,E>(*e_sh, *(elt->value_ptr_) ));
                }

                if(sortedPairs.size() == 0) return EdgeIterator<N, E>();

                return EdgeIterator<N, E>(std::make_shared< std::list<std::pair<N,E> > >(sortedPairs));
            }


        template <typename N, typename E>
            EdgeIterator<N, E> Graph<N,E>::edgeIteratorEnd() const
            {
                return EdgeIterator<N, E>();
            }


    // -------------------------------------------------------------------------------
    // Implementation of Node class
    // -------------------------------------------------------------------------------


        // node constructor
        template <typename N, typename E>
            Graph<N,E>::Node::Node(const N& n)
            {
                origin_ptr_ = std::make_shared<N>(n);
            }

        template <typename N, typename E>
            bool Graph<N,E>::Node::operator< (const Node& rhs) const
            {
                // allow us to take advantage of a sets natural ordering for iterator 
                if (connections_.size() == rhs.connections_.size()){
                    return  *origin_ptr_ < *(rhs.origin_ptr_);
                }

                return rhs.connections_.size() < connections_.size();
            }
                        

    // -------------------------------------------------------------------------------
    // Implementation of Edge class
    // -------------------------------------------------------------------------------

        template <typename N, typename E>
            Graph<N,E>::Edge::Edge(const std::shared_ptr<N>& origin, const std::shared_ptr<N>& dest, const std::shared_ptr<E>& e)
            {
                origin_ptr_ = origin;
                dest_ptr_ = dest;
                value_ptr_ = e;
            }


        template <typename N, typename E>
            bool Graph<N,E>::Edge::operator<(const Edge& rhs) const 
            {
                // this will allow us to use the sets order for the iterator 
                if(dest_ptr_.expired()) return false;
                if(rhs.dest_ptr_.expired()) return false;

                auto sh_dest = dest_ptr_.lock();
                auto rhs_sh_dest = rhs.dest_ptr_.lock();

                return *value_ptr_ < *(rhs.value_ptr_) 
                    || (!(*(rhs.value_ptr_) < *value_ptr_) && *sh_dest < *(rhs_sh_dest));
            }


} // end namespace: cs6771
#endif  // _GRAPH_H
