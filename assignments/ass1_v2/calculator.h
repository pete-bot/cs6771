// h file for ass1: calculator.cpp

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <stack>
#include <queue>
#include <vector>
#include <cmath>

// typedefs to improve readability
typedef std::stack <bool> boolStack;
typedef std::stack <int> intStack;
typedef std::stack <double> doubleStack;



// function to determine if the given string contains a double
inline bool isDouble(std::string token){ return token.find('.') != std::string::npos; }

void parseToken(boolStack isInt, intStack integers, doubleStack doubles, 
            std::string token, std::ifstream& inputFile);

void addHandler(boolStack isInt, intStack integers, doubleStack doubles);

