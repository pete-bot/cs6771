// peter kydd
// a program that reads instructions and values in from a file and executes
// uses the stack STL

// assignment v2 - use three stacks (as per brad's suggestion). 
// seems like a more interesting approach. previously, I was simply storing strings on 
// a single stack and converting when I needed them.

#include "calculator.h"


int main(int argc, char* argv[]){

    // setup the print out format for the precision required.
    std::cout.setf(std::ios::fixed,std::ios::floatfield);
    std::cout.precision(3);
    std::string firstArg;

    if (argc > 1){
        firstArg = argv[1];

        // create the three stacks that we will use
        boolStack isInt;
        intStack integers;
        doubleStack doubles;
        std::string token;

        std::cout << firstArg << std::endl;

        std::ifstream inputFile(firstArg);
        if(inputFile.is_open()){

            while (inputFile >> token ){
                parseToken(isInt, integers, doubles, token,  inputFile);
            }

            inputFile.close();
        } else {
            std::cout << "Unable to open file." << std::endl;
        }

    } else {
        std::cout << "No file entered. Please enter a file name as an argument" << std::endl;
    }

    return EXIT_SUCCESS;
}


void parseToken(boolStack isInt, intStack integers, doubleStack doubles, 
            std::string token, std::ifstream& inputFile){
    
    // check if number
    if(isdigit(token.front())){
        
        // int case
        if( !isDouble(token)){
            isInt.push(true);
            integers.push(std::stoi(token));
            std::cout << "int: " << token << std::endl;
        } else {
            isInt.push(false);
            integers.push(std::stod(token));
            std::cout << "double: " << token << std::endl;
        }            
              
    }else if(token.find("add") != std::string::npos ){
        addHandler(isInt, integers, doubles);    

    
    /*

    }else if(token.find("sub") != std::string::npos ){
        subHandler(isInt, integers, doubles);    
        
    }else if(token.find("mult") != std::string::npos ){
        multHandler(isInt, integers, doubles);

    }else if(token.find("div") != std::string::npos ){
        divisionHandler(isInt, integers, doubles);    

    }else if(token.find("sqrt") != std::string::npos ){
        sqrtHandler(isInt, integers, doubles);    
        
    }else if(token.find("pop") != std::string::npos ){
        popHandler(isInt, integers, doubles);    
        
    }else if(token.find("reverse") != std::string::npos ){
        reverseHandler(isInt, integers, doubles);    
        
    }else if(token.find("repeat") != std::string::npos ){
        repeatHandler(isInt, integers, doubles, inputFile);
        
    }else{
        std::cout << ">>> unknown instruction detected." << std::endl;
    */
    }
}

void addHandler(boolStack isInt, intStack integers, doubleStack doubles){
    // make sure that the stack has enough variables on it to complete operation
    if (isInt.size() >= 2){
        
        
    } else {
        std::cout << "Insufficient variables on stack to complete add operation." << std::endl;
    }

}


