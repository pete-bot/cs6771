// peter kydd  pkydd@cse.unsw.edu.au
// euclidean vector .cpp file. function implementations etc.
// 24/08/15

#include "EuclideanVector.h"

namespace cs6771{

    double EuclideanVector::get(unsigned int i) const
    {
        double return_value = 0;
        try {
            return_value = elements.at(i);
        } catch(const std::out_of_range& e) {
            std::cerr << "exception caught: " << e.what() << std::endl;
        }
        return return_value;
    }


    // calc norm and cache. otherwise, return previously calc-ed value.
    double EuclideanVector::getEuclideanNorm() const
    {
        if(norm < 0){
            double sum_squares = 0;
            for( auto element : elements){
                sum_squares += element*element;
            }
            norm = sqrt(sum_squares);
        }

        return norm;
    }

    // create unit vector
    EuclideanVector EuclideanVector::createUnitVector() const
    {
        EuclideanVector newVector = EuclideanVector(elements.size());

        // calculate and store the norm
        if(norm ==-1){
            getEuclideanNorm();
        }

        for(unsigned int i = 0; i< elements.size(); i++){
            newVector.elements.at(i)+=elements.at(i)/norm;
        }
        return newVector;
    }


    // adding vectors
    EuclideanVector& EuclideanVector::operator+=(const EuclideanVector &ev )
    {
        norm = -1;
        if(elements.size() == ev.elements.size()){
            for(unsigned int i = 0; i< elements.size(); i++){
                elements.at(i)+=ev.elements.at(i);
            }
        }else{
            throw std::range_error("invalid range argument");
        }
        return *this;
    }


    EuclideanVector EuclideanVector::operator+(const EuclideanVector &ev ) const
    {
        EuclideanVector newVec = EuclideanVector(this->elements.size());
        if(this->elements.size() == ev.elements.size()){

            for(unsigned int i = 0; i< this->elements.size(); i++){
                newVec.elements.at(i)= this->elements.at(i) + ev.elements.at(i);
            }
        }else{
            throw std::out_of_range("invalid range argument");
        }

        return newVec;
    }


    // subtracting vectors
    EuclideanVector& EuclideanVector::operator-=(const EuclideanVector &ev)
    {
        norm = -1;
        if(elements.size() == ev.elements.size()){
            for(unsigned int i = 0; i< elements.size(); i++){
                elements.at(i)-=ev.elements.at(i);
            }
        }else{
            throw std::range_error("invalid range argument");
        }
        return *this;
    }


    EuclideanVector EuclideanVector::operator-(const EuclideanVector &ev ) const 
    {
        EuclideanVector newVec = EuclideanVector(this->elements.size());
        if(this->elements.size() == ev.elements.size()){

            for(unsigned int i = 0; i< this->elements.size(); i++){
                newVec.elements.at(i)= this->elements.at(i) - ev.elements.at(i);
            }
        }else{
            throw std::range_error("invalid range argument");
        }

        return newVec;
    }


    // scalar multiplication
    EuclideanVector& EuclideanVector::operator*=(double scalar)
    {
        norm = -1;
        for(unsigned int i = 0; i< elements.size(); i++){
            elements.at(i)*=scalar;
        }
        return *this;
    }


    // scalar multiplication
    EuclideanVector EuclideanVector::operator*(double scalar) const 
    {
        EuclideanVector newVec = EuclideanVector(elements.size());

        for(unsigned int i = 0; i< elements.size(); i++){
            newVec.elements.at(i)= elements.at(i) * scalar;
        }

        return newVec;
    }


    // scalar product (dot product)
    double EuclideanVector::operator*(const EuclideanVector &ev) const
    {
        double scalar = 0;
        if(elements.size() == ev.elements.size()){
            
            for(unsigned int i = 0; i< this->elements.size(); i++){
                scalar += elements.at(i) * ev.elements.at(i);
            }

        }else{
            throw std::range_error("invalid range argument");
        }
        return scalar;
    }


    // scalar division
    EuclideanVector& EuclideanVector::operator/=(double scalar)
    {
        norm = -1;
        if(scalar != 0){
            for(unsigned int i = 0; i< elements.size(); i++){
                elements.at(i)/=scalar;
            }
        }else{
            throw std::range_error("attempted division by zero error");
        }
        return *this;
    }


    EuclideanVector EuclideanVector::operator/(double scalar) const
    {
        EuclideanVector newVec = EuclideanVector(this->elements.size());
        if(scalar != 0){

            for(unsigned int i = 0; i< this->elements.size(); i++){
                newVec.elements.at(i)= this->elements.at(i) / scalar;
            }
        }else{
            throw std::range_error("attempted division by zero error");
        }

        return newVec;
    }

    // copy constructor
    EuclideanVector::EuclideanVector(const EuclideanVector &ev)
    {
        norm = ev.norm;
        elements = ev.elements;
    }


    //move  constructor
    EuclideanVector::EuclideanVector(const EuclideanVector &&ev)
    {
        elements = std::move(ev.elements);
        norm = std::move(ev.norm);
    }

    // copy assignment
    EuclideanVector& EuclideanVector::operator=(const EuclideanVector &s){
        if(this != &s){
            elements = s.elements;
            norm = s.norm;
        }
        return *this;
    }

    // move assignment
    EuclideanVector& EuclideanVector::operator=(const EuclideanVector &&s){
        if(this != &s){
            elements = std::move(s.elements);
            norm = std::move(s.norm);
        }
        return *this;
    }

    std::ostream& operator<<(std::ostream &os, const EuclideanVector &ev){
        os<< "[";
        for( unsigned int i = 0; i<ev.elements.size(); i++) 
        {
            os << ev.elements.at(i);
            if(i!=ev.elements.size()-1){
                os << " ";
            }
        };
        
        os<< "]";
        return os;
    }


    // destructor. this should be in the .cpp file since it could be changed
    EuclideanVector::~EuclideanVector(){};

}
