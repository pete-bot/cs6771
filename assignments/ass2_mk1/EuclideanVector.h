// peter kydd  pkydd@cse.unsw.edu.au
// euclidean vector .h file. function definitions etc.
// 24/08/15

#ifndef EUCLIDEAN_VECTOR_H
#define EUCLIDEAN_VECTOR_H

#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <stdexcept>
#include <initializer_list>
#include <cmath>


namespace cs6771 {

    class EuclideanVector {

        //friend functions
        friend std::ostream& operator<<(std::ostream&, const EuclideanVector&); // output overload

        public:

            // basic constructors
            EuclideanVector( unsigned int dim, double mag ): elements(dim, mag){};
            EuclideanVector( unsigned int dim) : EuclideanVector( dim, 0.0) { };
            EuclideanVector(int dim, int mag) : EuclideanVector(static_cast<unsigned int>(dim),static_cast<double>(mag)) {};
            EuclideanVector(int dim, double mag) : EuclideanVector(static_cast<unsigned int>(dim), mag) {};

            // iterator constructor
            template <class InputIterator>
            EuclideanVector(const InputIterator &begin, const InputIterator &end){
                for(auto first = begin; first != end; ++first ){
                    elements.push_back(*first);
                }
            }

            // return the size of the vector
            inline unsigned int getNumDimensions() const {return elements.size();};

            // returns the ith element of the ev
            double get(unsigned int i) const;

            // return the euclidean norm - calc and cache if not already cached
            // this function is const, but the euclid. norm value is mutable
            double getEuclideanNorm() const;

            // return vector where each magnitude is *this/norm
            // this function is const, but the euclid. norm value is mutable
            EuclideanVector createUnitVector() const;

            // bool comparisons
            inline bool operator==(EuclideanVector ev) const { return elements == ev.elements; }
            inline bool operator!=(EuclideanVector ev) const { return elements != ev.elements; }

            // the internal const here means that the ref is const
            EuclideanVector& operator+=(const EuclideanVector& );
            EuclideanVector operator+(const EuclideanVector& ) const;

            // subtracting vectors
            EuclideanVector& operator-=(const EuclideanVector& );
            EuclideanVector operator-(const EuclideanVector& ) const;

            // dot product (defined between two vectors)
            double operator*(const EuclideanVector&) const;

            // scalar multiplication
            EuclideanVector& operator*=(double scalar);
            EuclideanVector operator*(double scalar) const;

            // scalar division
            EuclideanVector& operator/=(double scalar);
            EuclideanVector operator/(double scalar) const;

            // set the [] operator overloads
            inline double &operator[](int i) { return elements[i]; }

            inline double operator[](int i)const { return elements[i]; }

            // copy constructor
            EuclideanVector(const EuclideanVector &v);

            // move constructor
            EuclideanVector(const EuclideanVector &&v);

            // copy assignment
            EuclideanVector& operator=(const EuclideanVector &s);

            // move assignment
            EuclideanVector& operator=(const EuclideanVector &&i);

            // destructor (not used. Will leave here however, in case imp. changes.)
            ~EuclideanVector();

            // type conversion
            inline operator std::vector<double>() const {return std::vector<double>(elements);}
            inline operator std::list<double>() const {return std::list<double>(elements.begin(), elements.end());}

        private:
            std::vector<double> elements;
            // norm of a vector is always >=0
            mutable double norm = -1;

    };

    // free functions
    inline EuclideanVector operator*(double scalar, const EuclideanVector &ev) { return ev*scalar; }


}

#endif // EUCLIDEAN_VECTOR_H
