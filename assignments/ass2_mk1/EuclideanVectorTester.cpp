// EuclideanvectorTester.cpp
// COMP6771
// Callum Howard 2015

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <sstream>      // std::stringstream
#include <string>
#include <vector>
#include <list>
#include <array>

#include "EuclideanVector.h"

void testPrint();
void sampleTests();
void testConstIterConstructor();
void testCompileErrors();
void testEuclideanNorm();
void testOperators();
void testScalarOperators();
void testUnitVector();
void testDotProduct();
void testOperatorCommutivityCases();
void testImplicitConstructor();
void sampleConstCorrectnessTest();
void sampleConstructorTest();


int main (int argc, char *argv[]) {

    testConstIterConstructor();
    testPrint();
    testCompileErrors();
    testEuclideanNorm();
    testOperators();
    testScalarOperators();
    testUnitVector();
    testOperatorCommutivityCases();
    testImplicitConstructor();
    sampleTests();
    sampleConstCorrectnessTest();
    sampleConstructorTest();

    std::cout << "All tests passed." << std::endl;

    return EXIT_SUCCESS;
}

void testPrint() {
    // setup the stringstream out format for the precision required.
    std::stringstream ss;
    ss.setf(std::ios::fixed, std::ios::floatfield);
    ss.precision(7);

    {   // test 01: single dimension, no magnitude
        cs6771::EuclideanVector ev{1};
        assert(ev.getNumDimensions() == 1);
        ss << ev;
        //std::cout << ss.str() << std::endl; 
        assert(ss.str() == "[0.0000000]");
        ss.str(""); // clear the stream
    }

    {   // test 02: five dimensions, no magnitude
        cs6771::EuclideanVector ev{5};
        assert(ev.getNumDimensions() == 5);
        ss << ev;
        assert(ss.str() == "[0.0000000 0.0000000 0.0000000 0.0000000 0.0000000]");
        ss.str(""); // clear the stream
    }

    {   // test 03: zero dimensions, no magnitude
        cs6771::EuclideanVector ev{0};
        assert(ev.getNumDimensions() == 0);
        ss << ev;
        assert(ss.str() == "[]");
        ss.str(""); // clear the stream
    }

    {   // test 04: as above with parenthesis
        cs6771::EuclideanVector ev(0);
        assert(ev.getNumDimensions() == 0);
        ss << ev;
        assert(ss.str() == "[]");
        ss.str(""); // clear the stream
    }

/*
    {   // test 05: five dimensions with magnitude
        cs6771::EuclideanVector ev{5, 3.1415926};
        assert(ev.getNumDimensions() == 5);
        ss << ev;
        assert(ss.str() == "[3.1415926 3.1415926 3.1415926 3.1415926 3.1415926]");
        ss.str(""); // clear the stream
    }
    {   // test 06: as above with parenthesis
        cs6771::EuclideanVector ev(5, 3.1415926);
        assert(ev.getNumDimensions() == 5);
        ss << ev;
        assert(ss.str() == "[3.1415926 3.1415926 3.1415926 3.1415926 3.1415926]");
        ss.str(""); // clear the stream
    }

    {   // test 07: declared as unnamed
        ss << cs6771::EuclideanVector{5, 3.1415926};
        assert(ss.str() == "[3.1415926 3.1415926 3.1415926 3.1415926 3.1415926]");
        ss.str(""); // clear the stream
    }

    {   // test 08: as above with parenthesis
        ss << cs6771::EuclideanVector(5, 3.1415926);
        assert(ss.str() == "[3.1415926 3.1415926 3.1415926 3.1415926 3.1415926]");
        ss.str(""); // clear the stream
    }

    {   // test 09: declared with auto
        auto ev = cs6771::EuclideanVector{5, 3.1415926};
        assert(ev.getNumDimensions() == 5);
        ss << ev;
        assert(ss.str() == "[3.1415926 3.1415926 3.1415926 3.1415926 3.1415926]");
        ss.str(""); // clear the stream
    }
*/

    {   // test 10: constructed with vector iterator
        auto v = std::vector<double>{1.234, 2.345, 3.456, 4.567};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        assert(ev.getNumDimensions() == 4);
        ss << ev;
        assert(ss.str() == "[1.2340000 2.3450000 3.4560000 4.5670000]");
        ss.str(""); // clear the stream
    }

    {   // test 11: as above with parenthesis
        auto v = std::vector<double>{1.234, 2.345, 3.456, 4.567};
        auto ev = cs6771::EuclideanVector(v.begin(), v.end());
        assert(ev.getNumDimensions() == 4);
        ss << ev;
        assert(ss.str() == "[1.2340000 2.3450000 3.4560000 4.5670000]");
        ss.str(""); // clear the stream
    }

    {   // test 12: constructed with list iterator
        auto v = std::list<double>{1.234, 2.345, 3.456, 4.567};
        auto ev = cs6771::EuclideanVector(v.begin(), v.end());
        assert(ev.getNumDimensions() == 4);
        ss << ev;
        assert(ss.str() == "[1.2340000 2.3450000 3.4560000 4.5670000]");
        ss.str(""); // clear the stream
    }
}

void testConstIterConstructor() {
    {   // test 01: constructed with vector iterator
        auto v = std::vector<double>{1.234, 2.345, 3.456, 4.567};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        assert(ev.getNumDimensions() == 4);
        auto i = 0;
        for (auto s : {1.234, 2.345, 3.456, 4.567}) {
            assert(ev.get(i) == s);
            ++i;
        }
    }

    {   // test 02: as above with parenthesis
        auto v = std::vector<double>{1.234, 2.345, 3.456, 4.567};
        auto ev = cs6771::EuclideanVector(v.begin(), v.end());
        assert(ev.getNumDimensions() == 4);
        auto i = 0;
        for (auto s : {1.234, 2.345, 3.456, 4.567}) {
            assert(ev.get(i) == s);
            ++i;
        }
    }

    {   // test 03: constructed with list iterator
        auto v = std::list<double>{1.234, 2.345, 3.456, 4.567};
        auto ev = cs6771::EuclideanVector(v.begin(), v.end());
        assert(ev.getNumDimensions() == 4);
        auto i = 0;
        for (auto s : {1.234, 2.345, 3.456, 4.567}) {
            assert(ev.get(i) == s);
            ++i;
        }
    }
}

void testCompileErrors() {
    // these lines should all cause compile errors (-Wall -Werror) when uncommented
    //cs6771::EuclideanVector ev{-1};
    //cs6771::EuclideanVector ev{0.0};
    //cs6771::EuclideanVector ev{0.1};
    //cs6771::EuclideanVector ev{"0"};
    //cs6771::EuclideanVector ev{'\0'};
    //cs6771::EuclideanVector ev{nullptr};

    // these cases should be handled by try/catch or otherwise (not segfault)
    cs6771::EuclideanVector ev{0};
    /*
    try {
        ev.get(0);
        assert(false);
    } catch(std::out_of_range e) {
    }
    */
}

void testEuclideanNorm() {
    const auto epsilon = 0.0001;

    {   // test 01: sample test from spec with increased precision
        auto v = std::vector<double>{1, 2, 3};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        assert(std::abs(ev.getEuclideanNorm() - 3.74165) < epsilon);
    }

    {   // test 02: norm for vector with 4 dimesions
        auto v = std::vector<double>{1.234, 2.345, 3.456, 4.567};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        assert(std::abs(ev.getEuclideanNorm() - 6.310563049) < epsilon);
    }
}

void testUnitVector() {
    const auto epsilon = 0.001;

    {   // test 01: create unit vector test
        auto v = std::vector<double>{1.234, 2.345, 3.456, 4.567};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        auto uv = ev.createUnitVector();
        auto i = 0;
        for (auto s : {0.195545, 0.371599, 0.547653, 0.723707}) {
            assert(std::abs(uv.get(i) - s) < epsilon);
            ++i;
        }
    }

    {   // test 02: sample test from spec with increased precision
        auto v = std::vector<double>{1, 2, 3};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        auto uv = ev.createUnitVector();
        auto i = 0;
        for (auto s : {0.26726, 0.53451, 0.80178}) {
            assert(std::abs(uv.get(i) - s) < epsilon);
            ++i;
        }
    }
}

void testOperators() {
    const auto epsilon = 0.0001;

    {   // test 01: test the + operator
        auto v = std::vector<double>{1.234, 2.345, 3.456, 4.567};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        auto v2 = std::vector<double>{2.345, 3.456, 4.567, 1.234};
        auto ev2 = cs6771::EuclideanVector{v2.begin(), v2.end()};
        auto nv = ev + ev2;
        auto i = 0;
        for (auto s : {3.579, 5.801, 8.023, 5.801}) {
            assert(std::abs(nv.get(i) - s) < epsilon);
            assert((nv[i] - s) < epsilon);
            ++i;
        }
    }

    {   // test 02: test the - operator
        auto v = std::vector<double>{3.579, 5.801, 8.023, 5.801};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        auto v2 = std::vector<double>{2.345, 3.456, 4.567, 1.234};
        auto ev2 = cs6771::EuclideanVector{v2.begin(), v2.end()};
        auto nv = ev - ev2;
        auto i = 0;
        for (auto s : {1.234, 2.345, 3.456, 4.567}) {
            assert(std::abs(nv.get(i) - s) < epsilon);
            assert((nv[i] - s) < epsilon);
            ++i;
        }
    }

    {   // test 03: test the += assignment operator
        auto v = std::vector<double>{1.234, 2.345, 3.456, 4.567};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        auto v2 = std::vector<double>{2.345, 3.456, 4.567, 1.234};
        auto ev2 = cs6771::EuclideanVector{v2.begin(), v2.end()};
        ev += ev2;
        auto i = 0;
        for (auto s : {3.579, 5.801, 8.023, 5.801}) {
            assert(std::abs(ev.get(i) - s) < epsilon);
            assert((ev[i] - s) < epsilon);
            ++i;
        }
    }

    {   // test 04: test the -= assignment operator
        auto v = std::vector<double>{3.579, 5.801, 8.023, 5.801};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        auto v2 = std::vector<double>{2.345, 3.456, 4.567, 1.234};
        auto ev2 = cs6771::EuclideanVector{v2.begin(), v2.end()};
        ev -= ev2;
        auto i = 0;
        for (auto s : {1.234, 2.345, 3.456, 4.567}) {
            assert(std::abs(ev.get(i) - s) < epsilon);
            assert((ev[i] - s) < epsilon);
            ++i;
        }
    }

    {   // test 05: test the == conditional operator
        auto v = std::vector<double>{3.579, 5.801, 8.023, 5.801};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        auto v2 = std::vector<double>{3.579, 5.801, 8.023, 5.801};
        auto ev2 = cs6771::EuclideanVector{v2.begin(), v2.end()};
        assert(ev == ev2);
    }

    {   // test 06: test the != conditional operator with one inequal magnitude
        auto v = std::vector<double>{3.579, 5.801, 8.023, 5.800};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        auto v2 = std::vector<double>{3.579, 5.801, 8.023, 5.801};
        auto ev2 = cs6771::EuclideanVector{v2.begin(), v2.end()};
        assert(ev != ev2);
    }

    {   // test 07: test the != conditional operator with inequal dimensions
        auto v = std::vector<double>{3.579, 5.801, 8.023, 5.800};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        auto v2 = std::vector<double>{3.579, 5.801, 8.023};
        auto ev2 = cs6771::EuclideanVector{v2.begin(), v2.end()};
        assert(ev != ev2);
    }

    {   // test 08: test the operators for vectors of 0 dimensions
        auto ev = cs6771::EuclideanVector{0};
        auto ev2 = cs6771::EuclideanVector{0};
        auto nv = ev + ev2;
        assert(nv.getNumDimensions() == 0);

        nv = ev - ev2;
        assert(nv.getNumDimensions() == 0);

        nv = ev * ev2;
        assert(nv.getNumDimensions() == 0);
    }

}

void testScalarOperators() {
    const auto epsilon = 0.0001;

    {   // test 01: test the *= assignment operator
        auto v = std::vector<double>{1.234, 2.345, 3.456, 4.567};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        ev *= 2.0;
        auto i = 0;
        for (auto s : v) {
            assert(std::abs(ev.get(i) - (2.0*s)) < epsilon);
            assert((ev[i] - (2.0*s)) < epsilon);
            ++i;
        }
    }

    {   // test 02: test the /= assignment operator
        auto v = std::vector<double>{1.234, 2.345, 3.456, 4.567};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        ev /= 2.0;
        auto i = 0;
        for (auto s : v) {
            assert(std::abs(ev.get(i) - (s/2.0)) < epsilon);
            assert((ev[i] - (s/2.0)) < epsilon);
            ++i;
        }
    }

    {   // test 03: test the * operator
        auto v = std::vector<double>{3.579, 5.801, 8.023, 5.801};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        cs6771::EuclideanVector nv = ev * 2.0;
        auto i = 0;
        for (auto s : {2*3.579, 2*5.801, 2*8.023, 2*5.801}) {
            assert(std::abs(nv.get(i) - s) < epsilon);
            assert((nv[i] - s) < epsilon);
            ++i;
        }
    }

    {   // test 04: test the * operator reversed
        auto v = std::vector<double>{3.579, 5.801, 8.023, 5.801};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        cs6771::EuclideanVector nv = 2.0 * ev;
        auto i = 0;
        for (auto s : {2*3.579, 2*5.801, 2*8.023, 2*5.801}) {
            assert(std::abs(nv.get(i) - s) < epsilon);
            assert((nv[i] - s) < epsilon);
            ++i;
        }
    }

    {   // test 05: test the / operator
        auto v = std::vector<double>{2*3.579, 2*5.801, 2*8.023, 2*5.801};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        cs6771::EuclideanVector nv = ev / 2.0;
        auto i = 0;
        for (auto s : {3.579, 5.801, 8.023, 5.801}) {
            assert(std::abs(nv.get(i) - s) < epsilon);
            assert((nv[i] - s) < epsilon);
            ++i;
        }
    }
}

void testOperatorCommutivityCases() {
/*
   const auto epsilon = 0.0001;

   {   // test 06: test the / operator reversed
       auto v = std::vector<double>{3.579, 5.801, 8.023, 5.801};
       auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
       cs6771::EuclideanVector nv = 2.0 / ev;
       auto i = 0;
       for (auto s : {2.0/3.579, 2.0/5.801, 2.0/8.023, 2.0/5.801}) {
           std::cout << nv.get(i) << s << std::endl;
           assert(std::abs(nv.get(i) - s) < epsilon);
           ++i;
       }
   }

   {   // test 07: test the =/ operator reversed
       auto v = std::vector<double>{3.579, 5.801, 8.023, 5.801};
       auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
       cs6771::EuclideanVector nv = 2.0 /= ev;
       auto i = 0;
       for (auto s : {2.0/3.579, 2.0/5.801, 2.0/8.023, 2.0/5.801}) {
           std::cout << nv.get(i) << s << std::endl;
           assert(std::abs(nv.get(i) - s) < epsilon);
           ++i;
       }
   }

   {   // test 08: test the *= assignment operator reversed
       auto v = std::vector<double>{1.234, 2.345, 3.456, 4.567};
       auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
       2.0 *= ev;
       auto i = 0;
       for (auto s : v) {
           assert(std::abs(ev.get(i) - (2.0*s)) < epsilon);
           ++i;
       }
   }
*/
}

void testImplicitConstructor() {
    std::stringstream ss;
    ss.setf(std::ios::fixed, std::ios::floatfield);
    ss.precision(7);

    {   // test 05: five dimensions with magnitude
        cs6771::EuclideanVector ev = 5;
        assert(ev.getNumDimensions() == 5);
        ss << ev;
        assert(ss.str() == "[0.0000000 0.0000000 0.0000000 0.0000000 0.0000000]");
        ss.str(""); // clear the stream
    }
}

void testImplicitConversion() {
    const auto epsilon = 0.0001;

    {   // test 01: test implicit conversion to vector
        auto v = std::vector<double>{1.234, 2.345, 3.456, 4.567};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        auto nv = std::vector<double>(ev);
        assert(nv == v);
        auto i = 0;
        for (auto s : v) {
            assert(std::abs(nv.at(i) - s) < epsilon);
            assert((ev[i] - s) < epsilon);
            ++i;
        }
    }

    {   // test 02: test implicit conversion to vector negative equality
        auto v = std::vector<double>{1.234, 2.345, 3.456, 4.568};
        auto ev = cs6771::EuclideanVector{v.begin(), v.end()};
        auto nv = std::vector<double>(ev); //TODO is doesn't like curly {} constructor
        assert(nv != v);
    }
}

void sampleTests() {
    cs6771::EuclideanVector a(2);	// use a constructor

    std::list<double> l;
    l.push_back(1);
    l.push_back(2);
    l.push_back(3);

    cs6771::EuclideanVector b(l.begin(),l.end());

    std::vector<double> v2;
    v2.push_back(4);
    v2.push_back(5);
    v2.push_back(6);
    v2.push_back(7);

    cs6771::EuclideanVector c(v2.begin(),v2.end());

    std::array<double,5> a1 = {{5,4,3,2,1}};

    cs6771::EuclideanVector d(a1.begin(),a1.end());

    std::array<double,5> a2 = {{9,0,8,6,7}};

    cs6771::EuclideanVector e(a2.begin(),a2.end());

    // use the copy constructor
    cs6771::EuclideanVector f(e);

    std::cout << a.getNumDimensions() << ": " << a << std::endl;
    std::cout << "D1:" << b.get(1) << " " << b << std::endl;
    std::cout << c << " Euclidean Norm = " << c.getEuclideanNorm() << std::endl;
    std::cout << d << " Unit Vector: " << d.createUnitVector() << " L = " << d.createUnitVector().getEuclideanNorm() << std::endl;
    std::cout << e << std::endl;
    std::cout << f << std::endl;

    // test the move constructor
    cs6771::EuclideanVector g = std::move(f);
    std::cout << g << std::endl;

    // try operator overloading
    e += d;
    std::cout << e << std::endl;

    cs6771::EuclideanVector h = e - g;
    std::cout << h << std::endl;

    // test scalar multiplication
    h *= 2;
    std::cout << h << std::endl;

    cs6771::EuclideanVector j = b / 2;
    std::cout << j << std::endl;

    std::cout << "dot product = " << j * b << std::endl;

    if (g == (e - d)) std::cout << "true" << std::endl;
    if (j != b ) std::cout << "false" << std::endl;

    j[0] = 1;
    std::cout << j << std::endl;

    // type cast from EuclideanVector to a std::vector
    std::vector<double> vj = j;

    // type cast from EuclideanVector to a std::vector
    std::list<double> lj = j;

    for (auto d : lj) {
        std::cout << d << std::endl;
    }
}

void sampleConstCorrectnessTest() {

    std::list<double> l{1.0,2.0,3.0,4.0};

    const cs6771::EuclideanVector e{ (l.begin())++, l.end()};
    std::cout << e << std::endl;
    std::cout << e.getNumDimensions() << std::endl;
    std::cout << e.get(1) << " " << e[2] << std::endl;
    std::cout << e.getEuclideanNorm() << std::endl;
    std::cout << e.createUnitVector() << std::endl;
    cs6771::EuclideanVector f = 6;
    std::cout << std::boolalpha << (f == e) << std::endl;
    f = cs6771::EuclideanVector{4,5.0} + e;
    std::cout << f << std::endl;
    std::cout << static_cast<std::list<double>>(e).front() << std::endl;

}

void sampleConstructorTest() {
    cs6771::EuclideanVector a_rval{1};

    unsigned int ui = 2;
    cs6771::EuclideanVector a2{ui};

    const int ci = 3;
    cs6771::EuclideanVector a3{ci};

    const unsigned int cui = 4;
    cs6771::EuclideanVector a4{cui};

    short unsigned si = 5;
    cs6771::EuclideanVector a5{si};

    std::list<int> l {1,2,4};
    cs6771::EuclideanVector b{l.begin(),l.end()};

    std::vector<unsigned int> v2 {4,5,6,7};
    cs6771::EuclideanVector c{v2.begin(),v2.begin()+3};

    std::vector<double> a1 {5,4,3.1,2,1.0};
    cs6771::EuclideanVector d{a1.begin(),a1.end()};

    cs6771::EuclideanVector e{1,4};  // see course newsfeed if failing this test

    const int mag = 5;
    cs6771::EuclideanVector e2{ci,mag};

    cs6771::EuclideanVector e3{ui,4.9};

    double d1 = 3.14;
    cs6771::EuclideanVector e4{cui,d1};
}
