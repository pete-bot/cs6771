#include <iostream>
#include <vector>
#include <list>
#include <array>
#include <cstdlib>
#include <cassert>

#include "EuclideanVector.h"

// function definitions
void official_test_one();
void constructor_tests();
void official_test_two();


int main(int argc, char* argv[])
{
    official_test_one();
    official_test_two();
    constructor_tests();

    return EXIT_SUCCESS;
}

void official_test_one()
{

    cs6771::EuclideanVector a{2};   // use a constructor

    std::list<double> l {1,2,3};
    cs6771::EuclideanVector b{l.begin(),l.end()};


    std::vector<double> v2 {4,5,6,7};
    cs6771::EuclideanVector c{v2.begin(),v2.end()};

    std::vector<double> a1 {5,4,3,2,1};
    cs6771::EuclideanVector d{a1.begin(),a1.end()};

    std::list<double> a2 {9,0,8,6,7};
    cs6771::EuclideanVector e{a2.begin(),a2.end()};

    // use the copy constructor
    cs6771::EuclideanVector f{e};

    std::cout << a.getNumDimensions() << ": " << a << std::endl;
    std::cout << "D1:" << b.get(1) << " " << b << std::endl;
    std::cout << c << " Euclidean Norm = " << c.getEuclideanNorm() << std::endl;
    std::cout << d << " Unit Vector: " << d.createUnitVector() << " L = " << d.createUnitVector().getEuclideanNorm() << std::endl;
    std::cout << e << std::endl;
    std::cout << f << std::endl;

    // test the move constructor
    cs6771::EuclideanVector g = std::move(f);
    std::cout << g << std::endl;

    // try operator overloading
    e += d;
    std::cout << e << std::endl;

    cs6771::EuclideanVector h = e - g;
    std::cout << h << std::endl;

    // test scalar multiplication
    h *= 2;
    std::cout << h << std::endl;

    cs6771::EuclideanVector j = b / 2;
    std::cout << j << std::endl;

    std::cout << "dot product = " << j * b << std::endl;

    if (g == (e - d)) std::cout << "true" << std::endl;
    if (j != b ) std::cout << "false" << std::endl;

    j[0] = 1;
    std::cout << j << std::endl;


    // type cast from EuclideanVector to a std::vector
    std::vector<double> vj = j;

    // type cast from EuclideanVector to a std::vector
    std::list<double> lj = j;

    for (auto d : lj) {
        std::cout << d << std::endl;
    }

}

void official_test_two()
{

    cs6771::EuclideanVector a_rval{1};

    unsigned int ui = 2;
    cs6771::EuclideanVector a2{ui};

    const int ci = 3;
    cs6771::EuclideanVector a3{ci};

    const unsigned int cui = 4;
    cs6771::EuclideanVector a4{cui};

    short unsigned si = 5;
    cs6771::EuclideanVector a5{si};

    std::list<int> l {1,2,4};
    cs6771::EuclideanVector b{l.begin(),l.end()};

    std::vector<unsigned int> v2 {4,5,6,7};
    cs6771::EuclideanVector c{v2.begin(),v2.begin()+3};

    std::vector<double> a1 {5,4,3.1,2,1.0};
    cs6771::EuclideanVector d{a1.begin(),a1.end()};

    cs6771::EuclideanVector e{1,4};

    const int mag = 5;
    cs6771::EuclideanVector e2{ci,mag};

    cs6771::EuclideanVector e3{ui,4.9};

    double d1 = 3.14;
    cs6771::EuclideanVector e4{cui,d1};

}


void constructor_tests()
{
/*
    std::cout << ">>> running test one..." << std::endl;

    std::cout << "Constructing vectors using standard methods..." << std::endl;
    cs6771::EuclideanVector a(2);   // use a constructor
    cs6771::EuclideanVector b(3, 2.13);
    cs6771::EuclideanVector c(3, 1.0);
    cs6771::EuclideanVector d(0);


    std::cout << a << std::endl;
    std::cout << b << std::endl;


    std::cout << "Testing boolean operations" << std::endl;
    // testing affirmative
    assert((a==a) == 1);
    assert((b==b) == 1);
    // testing negative
    assert((a==b) == 0);
    assert((b==a) == 0);

    // testing affirmative
    assert((b!=a) == 1);
    assert((a!=b) == 1);
    // testing negative
    assert((b!=b) != 1);
    assert((a!=a) != 1);

    b+=b;

    std::cout << b << std::endl;
    //std::cout << b.get(1) << std::endl;
    b+=c;
    std::cout << b << std::endl;

    d = b + c;

    std::cout << d << std::endl;

    std::cout << "<<< test one passed." << std::endl;
    */
}
