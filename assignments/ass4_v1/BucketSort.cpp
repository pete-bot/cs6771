// cs6771 assignment 4
// peter kydd

#include <iostream>
#include <algorithm>
#include <vector>

#include <thread>
#include <mutex>

#include "BucketSort.h"


template<typename F>
void locked_scope(std::mutex& m, F fn)
{
    std::lock_guard<std::mutex> lg{m};
    fn();
}

void BucketSort::sort(unsigned int numCores) {
    
    // threading 
    std::mutex queue_mutex;
    std::vector<std::vector<unsigned int>* > queue;
    std::vector<std::thread> threads;

    // create a vector of 9 buckets
    std::vector< std::vector<unsigned int> > buckets(9);

    unsigned int zeroCounter = 0;
    unsigned int value_copy;

    // separate numbers into appropriate buckets with MSB radix   
    for (unsigned int value : numbersToSort){
        if(value == 0){
            ++zeroCounter;  
            continue;
        } 
        value_copy = value;
        
        // find first digit of value
        while(value_copy/10 > 0) value_copy = value_copy/10;
        buckets.at(value_copy-1).push_back(value);
    }

    auto pSort = [&]()
    {
        std::vector<unsigned int>* bucket;
        int sortFlag;
        
        while(!queue.empty()){
            
            sortFlag = 0;
            locked_scope(queue_mutex, [&]
            {
                if(!queue.empty()){
                    bucket = queue.back();
                    queue.pop_back();
                    sortFlag = 1;
                }
            });
            
            if(sortFlag) std::sort(bucket->begin(), bucket->end());
        }

    };

    // populate queue with pointers to buckets
    for (auto& bucket : buckets){
        queue.push_back(&bucket);
    }


    // create threads
    // want to run one sort locally, so we start at 1 
    for (unsigned int i = 1; i< numCores; ++i){
        threads.push_back(std::thread(pSort));
    }


    // run a sort locally
    while(!queue.empty()){
        
        std::vector<unsigned int>* bucket;
        int sortFlag = 0;

        locked_scope(queue_mutex, [&]
        {
            if(!queue.empty()){
                bucket = queue.back();
                queue.pop_back();
                sortFlag = 1;
            }
        });
            
        if(sortFlag) std::sort(bucket->begin(), bucket->end());
    }


    for (auto& t : threads) t.join();

    // create vector to return 
    std::vector<unsigned int> numbersToReturn;
    // reserve vector space to speed up joining
    numbersToReturn.reserve(numbersToSort.size());

    // place any zeros at the beginning of the set
    for(unsigned int i = 0; i < zeroCounter; i++ ){
        numbersToReturn.push_back(0);
    }

    // recombine elements into single vector to return
    for (auto bucket: buckets){
        numbersToReturn.insert(numbersToReturn.end(), bucket.begin(), bucket.end());
    }

    
    numbersToSort = numbersToReturn;
}
