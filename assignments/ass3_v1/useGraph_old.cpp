// a base tester program for the graph template class

#include "Graph.h"
#include "Graph.h"

// other includes
#include <iostream>
#include <cstdlib>
#include <cassert>

// test node creation, edge creation etc. 
void test_1(void);
void test_2(void);
void test_3(void);
void test_4(void);
void test_5(void);

int main(int argc, char* argv[]) {
	//test_1();
	//test_2();
	//test_3();
	//test_4();
	test_5();

	return EXIT_SUCCESS;
}




// basic tests on Graph<int, int> type
void test_u15(void)
{
	std::cout << ">>>> Running test_1." << std::endl;
	std::cout << "Graph<int, int>:" << std::endl;

	cs6771::Graph<int, int> g1;
	std::cout << "Adding a selection of unique nodes of int type." << std::endl;
	assert(g1.addNode(1) == 1);
	assert(g1.addNode(2) == 1);
	assert(g1.addNode(3) == 1);
	assert(g1.addNode(4) == 1);
	std::cout << "---PASSED---" << std::endl << std::endl;

	std::cout << "Adding a selection of repeated nodes of int type." << std::endl;
	assert(g1.addNode(1) == 0);
	assert(g1.addNode(2) == 0);
	assert(g1.addNode(3) == 0);
	assert(g1.addNode(4) == 0);
	std::cout << "---PASSED---" << std::endl << std::endl;	

	std::cout << "Adding a selection of unique edges of int type." << std::endl;
	assert(g1.addEdge(1,2,1) == 1);
	assert(g1.addEdge(1,3,2) == 1);
	assert(g1.addEdge(1,4,3) == 1);
	assert(g1.addEdge(2,3,4) == 1);
	assert(g1.addEdge(2,4,5) == 1);
	assert(g1.addEdge(3,4,6) == 1);
	assert(g1.addEdge(4,1,1) == 1);
	assert(g1.addEdge(4,2,2) == 1);
	assert(g1.addEdge(4,3,3) == 1);
	std::cout << "---PASSED---" << std::endl << std::endl;

	std::cout << "Adding a selection of repeated edges of int type." << std::endl; 	
	assert(g1.addEdge(1,2,1) == 0);
	assert(g1.addEdge(1,3,2) == 0);
	assert(g1.addEdge(1,4,3) == 0);
	assert(g1.addEdge(2,3,4) == 0);
	assert(g1.addEdge(2,4,5) == 0);
	assert(g1.addEdge(3,4,6) == 0);
	assert(g1.addEdge(4,1,1) == 0);
	assert(g1.addEdge(4,2,2) == 0);
	assert(g1.addEdge(4,3,3) == 0);
	std::cout << "---PASSED---" << std::endl << std::endl;


	std::cout << "Now testing that errors are being caught correctly." << std::endl;
	try
	{
		std::cout << "Add edge to two non-existant nodes" << std::endl;
		g1.addEdge(100,100,100);
	}
	catch (std::runtime_error &e)
	{
		std::cerr<< "ERROR: " << e.what() << std::endl;
	}
	catch(...)
	{
        std::cerr << "Unknown exception." << std::endl;
	}
	std::cout << "---PASSED---" << std::endl << std::endl;

	try
	{
		std::cout << "Add edge to non-existant origin, existing dest. " << std::endl;
		g1.addEdge(27, 1,100);
	}
	catch (std::runtime_error &e)
	{
		std::cerr<< "ERROR: " << e.what() << std::endl;
	}
	catch(...)
	{
        std::cerr << "Unknown exception." << std::endl;
	}
	std::cout << "---PASSED---" << std::endl << std::endl;

	try
	{
		std::cout << "Add edge to existing origin, non-existant dest. " << std::endl;
		g1.addEdge(1, 42,100);
	}
	catch (std::runtime_error &e)
	{
		std::cerr<< "ERROR: " << e.what() << std::endl;
	}
	catch(...)
	{
        std::cerr << "Unknown exception." << std::endl;
	}
	std::cout << "---PASSED---" << std::endl << std::endl;
	std::cout << "Graph stats: " << std::endl;
	g1.printGraphStats();
	std::cout<<">>>> test_1: PASSED" << std::endl<< std::endl;
}

// basic tests on Graph<std:;string, std:;string> type
void test_u16(void)
{
	std::cout << ">>>> Running test_2: " << std::endl;
	std::cout << "Graph<std::string, std::string>:" << std::endl;

	cs6771::Graph<std::string, std::string> g2;
	std::cout << "Adding a selection of unique nodes of std::string type." << std::endl;
	assert(g2.addNode("a") == 1);
	assert(g2.addNode("b") == 1);
	assert(g2.addNode("c") == 1);
	assert(g2.addNode("d") == 1);
	std::cout << "---PASSED---" << std::endl << std::endl;

	std::cout << "Adding a selection of repeated nodes of string type." << std::endl;
	assert(g2.addNode("a") == 0);
	assert(g2.addNode("b") == 0);
	assert(g2.addNode("c") == 0);
	assert(g2.addNode("d") == 0);
	std::cout << "---PASSED---" << std::endl << std::endl;	

	std::cout << "Adding a selection of unique edges of string type." << std::endl;
	assert(g2.addEdge("a","b","this") == 1);
	assert(g2.addEdge("a","c","is") == 1);
	assert(g2.addEdge("a","d","a") == 1);
	assert(g2.addEdge("b","c","test") == 1);
	assert(g2.addEdge("b","d","so:") == 1);
	assert(g2.addEdge("c","d","test!") == 1);
	assert(g2.addEdge("d","a","this") == 1);
	assert(g2.addEdge("d","b","is") == 1);
	assert(g2.addEdge("d","c","a") == 1);
	std::cout << "---PASSED---" << std::endl << std::endl;

	std::cout << "Adding a selection of repeated edges of string type." << std::endl; 	
	assert(g2.addEdge("a","b","this") == 0);
	assert(g2.addEdge("a","c","is") == 0);
	assert(g2.addEdge("a","d","a") == 0);
	assert(g2.addEdge("b","c","test") == 0);
	assert(g2.addEdge("b","d","so:") == 0);
	assert(g2.addEdge("c","d","test!") == 0);
	assert(g2.addEdge("d","a","this") == 0);
	assert(g2.addEdge("d","b","is") == 0);
	assert(g2.addEdge("d","c","a") == 0);
	std::cout << "---PASSED---" << std::endl << std::endl;


	std::cout << "Now testing that errors are being caught correctly." << std::endl;
	try
	{
		std::cout << "Add edge to two non-existant nodes" << std::endl;
		g2.addEdge("broken","test","one");
	}
	catch (std::runtime_error &e)
	{
		std::cerr<< "ERROR: " << e.what() << std::endl;
	}
	catch(...)
	{
        std::cerr << "Unknown exception." << std::endl;
	}
	std::cout << "---PASSED---" << std::endl << std::endl;

	try
	{
		std::cout << "Add edge to non-existant origin, existing dest. " << std::endl;
		g2.addEdge("broken", "a","one");
	}
	catch (std::runtime_error &e)
	{
		std::cerr<< "ERROR: " << e.what() << std::endl;
	}
	catch(...)
	{
        std::cerr << "Unknown exception." << std::endl;
	}
	std::cout << "---PASSED---" << std::endl << std::endl;

	try
	{
		std::cout << "Add edge to existing origin, non-existant dest. " << std::endl;
		g2.addEdge("d", "unknown destination","test string");
	}
	catch (std::runtime_error &e)
	{
		std::cerr<< "ERROR: " << e.what() << std::endl;
	}
	catch(...)
	{
        std::cerr << "Unknown exception." << std::endl;
	}
	std::cout << "---PASSED---" << std::endl << std::endl;
	std::cout << "Graph stats: " << std::endl;
	g2.printGraphStats();
	std::cout<<">>>> test_2: PASSED" << std::endl<< std::endl;
}

// basic tests on Graph<std::string, int> type
void test_u17(void)
{
	std::cout << ">>>> Running test_3: " << std::endl;
	std::cout << "Graph<std::string, int>:" << std::endl;

	cs6771::Graph<std::string, int> g3;
	std::cout << "Adding a selection of unique nodes of std::string type with int weights.." << std::endl;
	assert(g3.addNode("a") == 1);
	assert(g3.addNode("b") == 1);
	assert(g3.addNode("c") == 1);
	assert(g3.addNode("d") == 1);
	std::cout << "---PASSED---" << std::endl << std::endl;

	std::cout << "Adding a selection of repeated nodes of string type." << std::endl;
	assert(g3.addNode("a") == 0);
	assert(g3.addNode("b") == 0);
	assert(g3.addNode("c") == 0);
	assert(g3.addNode("d") == 0);
	std::cout << "---PASSED---" << std::endl << std::endl;	

	std::cout << "Adding a selection of unique edges of string type." << std::endl;
	assert(g3.addEdge("a","b",1) == 1);
	assert(g3.addEdge("a","c",2) == 1);
	assert(g3.addEdge("a","d",3) == 1);
	assert(g3.addEdge("b","c",4) == 1);
	assert(g3.addEdge("b","d",5) == 1);
	assert(g3.addEdge("c","d",6) == 1);
	assert(g3.addEdge("d","a",1) == 1);
	assert(g3.addEdge("d","b",2) == 1);
	assert(g3.addEdge("d","c",3) == 1);
	std::cout << "---PASSED---" << std::endl << std::endl;

	std::cout << "Adding a selection of repeated edges of string type." << std::endl; 	
	assert(g3.addEdge("a","b",1) == 0);
	assert(g3.addEdge("a","c",2) == 0);
	assert(g3.addEdge("a","d",3) == 0);
	assert(g3.addEdge("b","c",4) == 0);
	assert(g3.addEdge("b","d",5) == 0);
	assert(g3.addEdge("c","d",6) == 0);
	assert(g3.addEdge("d","a",1) == 0);
	assert(g3.addEdge("d","b",2) == 0);
	assert(g3.addEdge("d","c",3) == 0);
	std::cout << "---PASSED---" << std::endl << std::endl;


	std::cout << "Now testing that errors are being caught correctly." << std::endl;
	try
	{
		std::cout << "Add edge to two non-existant nodes" << std::endl;
		g3.addEdge("broken","test",100);
	}
	catch (std::runtime_error &e)
	{
		std::cerr<< "	>ERROR: " << e.what() << std::endl;
	}
	catch(...)
	{
        std::cerr << "Unknown exception." << std::endl;
	}
	std::cout << "---PASSED---" << std::endl << std::endl;

	try
	{
		std::cout << "Add edge to non-existant origin, existing dest. " << std::endl;
		g3.addEdge("broken", "a",42);
	}
	catch (std::runtime_error &e)
	{
		std::cerr<< "	>ERROR: " << e.what() << std::endl;
	}
	catch(...)
	{
        std::cerr << "Unknown exception." << std::endl;
	}
	std::cout << "---PASSED---" << std::endl << std::endl;

	try
	{
		std::cout << "Add edge to existing origin, non-existant dest. " << std::endl;
		g3.addEdge("d", "unknown destination",-1);
	}
	catch (std::runtime_error &e)
	{
		std::cerr<< "	>ERROR: " << e.what() << std::endl;
	}
	catch(...)
	{
        std::cerr << "Unknown exception." << std::endl;
	}
	std::cout << "---PASSED---" << std::endl;
	std::cout << "Graph stats: " << std::endl;
	g3.printGraphStats();
	std::cout<<">>>> test_3: PASSED" << std::endl<< std::endl;

	// CREATE USE COUNT TESTS:
	
	// std::cout << "use count on 'd': " << g3.getUseCountN("d")<< std::endl;

	// g3.replace("d", "z");

	// std::cout<< "getting node with new 'z' origin. ";
	// g3.printNValue("z");
	// std::cout << std::endl;
	// std::cout << "use NEW count on 'z': " << g3.getUseCountN("z")<< std::endl;
	
}


// tests for replace, merge and delete functions
void test_u18(void)
{
	std::cout << ">>>> Running test_4: " << std::endl;

	// cs6771::Graph<int, int> g1;
	// std::cout << "Adding a selection of unique nodes of int type." << std::endl;
	// assert(g1.addNode(1) == 1);
	// assert(g1.addNode(2) == 1);
	// assert(g1.addNode(3) == 1);
	// assert(g1.addNode(4) == 1);
	
	// std::cout << "Adding a selection of edges to the nodes" << std::endl;
	// assert(g1.addEdge(1,2,1) == 1);
	// assert(g1.addEdge(1,3,2) == 1);
	// assert(g1.addEdge(1,4,3) == 1);
	// assert(g1.addEdge(2,3,4) == 1);
	// assert(g1.addEdge(2,4,5) == 1);
	// assert(g1.addEdge(3,4,6) == 1);
	// assert(g1.addEdge(4,1,1) == 1);
	// assert(g1.addEdge(4,2,2) == 1);
	// assert(g1.addEdge(4,3,3) == 1);

	//g1.mergeReplace(1,2);
	//g1.mergeReplace(1,2);


	cs6771::Graph<std::string, int> g2;
	assert(g2.addNode("A") == 1);
	assert(g2.addNode("B") == 1);
	assert(g2.addNode("C") == 1);
	assert(g2.addNode("D") == 1);
	assert(g2.addNode("E") == 1);
	assert(g2.addNode("F") == 1);
	assert(g2.addNode("ZZZ") == 1);
	
		
	assert(g2.addEdge("A","ZZZ",0) == 1);
	assert(g2.addEdge("B","ZZZ",0) == 1);
	assert(g2.addEdge("A","ZZZ",1) == 1);
	assert(g2.addEdge("B","ZZZ",1) == 1);
	assert(g2.addEdge("A","ZZZ",2) == 1);
	assert(g2.addEdge("B","ZZZ",2) == 1);
	assert(g2.addEdge("A","ZZZ",3) == 1);
	assert(g2.addEdge("B","ZZZ",3) == 1);
	

	assert(g2.addEdge("A","D",1) == 1);
	assert(g2.addEdge("A","F",1) == 1);
	assert(g2.addEdge("A","C",1) == 1);
	assert(g2.addEdge("A","E",2) == 1);

	assert(g2.addEdge("B","A",1) == 1);
	assert(g2.addEdge("B","D",1) == 1);
	assert(g2.addEdge("B","C",1) == 1);
	assert(g2.addEdge("B","E",1) == 1);
	assert(g2.addEdge("B","F",1) == 1);


	assert(g2.addEdge("A","B",11) == 1);
	assert(g2.addEdge("A","B",12) == 1);
	assert(g2.addEdge("A","B",13) == 1);
	assert(g2.addEdge("A","B",14) == 1);

	assert(g2.addEdge("A","B",3) == 1);
	assert(g2.addEdge("D","B",3) == 1);
	assert(g2.addEdge("C","B",3) == 1);
	assert(g2.addEdge("E","B",3) == 1);
	assert(g2.addEdge("F","B",3) == 1);


	//assert(g2.addEdge("A","A",4) == 1);
	assert(g2.addEdge("D","A",4) == 1);
	assert(g2.addEdge("C","A",4) == 1);
	assert(g2.addEdge("E","A",4) == 1);
	assert(g2.addEdge("F","A",4) == 1);



	//g2.mergeReplace("B","A");
	//g2.mergeReplace("A","A");

	g2.printNConnections("B");

	g2.printNConnections("A");
	g2.mergeReplace("B", "A");
	
	g2.printNConnections("A");
	g2.printNConnections("C");
	g2.printNConnections("D");
	g2.printNConnections("E");
	g2.printNConnections("F");
	g2.printNConnections("ZZZ");

	// should throw an exception
	//

	try {
		g2.printNConnections("B");	
	} catch (std::runtime_error &e) {
		std::cerr << "	>ERROR: " << e.what() << std::endl;
	} catch(...){
        std::cerr << "Unknown exception." << std::endl;
	}

	g2.deleteNode("ZZZ");

	g2.printNConnections("A");	
	g2.deleteEdge("E", "A", 4);
	g2.deleteEdge("E", "A", 3);
	
	g2.printNConnections("E");
	
	g2.clear();
	
	std::cout << ">>>> test_4: PASSED " << std::endl<< std::endl;
}


// tests for delete, clear etc. 
void test_u19(void){
	std::cout << ">>>> Running test_5: " << std::endl;

	cs6771::Graph<int, int> g1;
	//std::cout << "Adding a selection of unique nodes of int type." << std::endl;
	assert(g1.addNode(1) == 1);
	assert(g1.addNode(2) == 1);
	assert(g1.addNode(3) == 1);
	assert(g1.addNode(4) == 1);
	g1.addNode(5);

	assert(g1.addEdge(1,2,1) == 1);
	assert(g1.addEdge(1,3,2) == 1);
	assert(g1.addEdge(1,4,3) == 1);
	assert(g1.addEdge(2,3,4) == 1);
	assert(g1.addEdge(2,4,5) == 1);
	assert(g1.addEdge(3,4,6) == 1);
	assert(g1.addEdge(4,1,1) == 1);
	assert(g1.addEdge(4,2,2) == 1);
	assert(g1.addEdge(4,3,3) == 1);
	assert(g1.addEdge(4,4,5) == 1);
	assert(g1.addEdge(4,5,5) == 1);
	g1.addEdge(1,5,2);
	
	g1.addEdge(5,4,6);
	g1.addEdge(5,4,7);
	g1.addEdge(5,4,8);
	g1.addEdge(5,4,9);
	g1.addEdge(5,4,10);


	//g1.printNodes();
	g1.edgeIteratorBegin(1);

	//auto edge = g1.edgeIteratorBegin(1);
	//std::cout << typeid(edge).name() << std::endl;
	
	/*
	for(auto node = g1.begin(); node != g1.end(); ++node){
		std::cout<< *node << std::endl; 
	}
	*/
	
	// edge->first
	for(auto edge = g1.edgeIteratorBegin(1); edge != g1.edgeIteratorEnd(); ++edge){
		std::cout<< edge->first << std::endl; 
	}
	

	std::cout << ">>>> test_5: PASSED " << std::endl<< std::endl;
}
