#ifndef _GRAPH_H
#define _GRAPH_H

#include <iostream>     // IO
//#include <map>
#include <memory>       // pointers
#include <algorithm>    // sort
#include <list>         // lists
#include <stdexcept>    // std exceptions

// REMOVE
//#include <cstdlib>

namespace cs6771 {
    // NodeIterator
    template<typename N>
        class NodeIterator
        {
            public:
                
                // define the iterator traits
                typedef std::input_iterator_tag          iterator_category;
                typedef std::ptrdiff_t                   difference_type;
                typedef N                                value_type;
                typedef N*                               pointer;
                typedef N&                               reference;

                reference operator*() const;        
                pointer operator->() const;        

                //preincrement
                NodeIterator& operator++(); 
                
                // post increment (does not return a reference)
                NodeIterator operator++(int);

                bool operator==(const NodeIterator& other) const;
                bool operator!=(const NodeIterator& other) const;

                NodeIterator(std::shared_ptr<std::list<std::shared_ptr<N> > > v = nullptr);

            private:
                std::shared_ptr <std::list<std::shared_ptr<N> > > sortedPointers;       

        };  // END NODE-ITERATOR TEMPLATE
    // -------------------------------------------------------------------------------
    // Implementation of NodeIterator 
    // -------------------------------------------------------------------------------

        // constructor
        template <typename N>       
            NodeIterator<N>::NodeIterator(std::shared_ptr<std::list<std::shared_ptr<N> > > v) :sortedPointers{v} {}
                
            
        template<typename N>
            typename NodeIterator<N>::reference NodeIterator<N>::operator*() const
            {
                return *(sortedPointers->front());
            }

        template <typename N> 
            typename NodeIterator<N>::pointer NodeIterator<N>::operator->() const
            {
                return &(operator*());
            }

        
        template <typename N> 
            bool NodeIterator<N>::operator==(const NodeIterator& other) const
            {
                //    return sortedPointers == other.sortedPointers;

                if (sortedPointers == other.sortedPointers) return true;
                if (sortedPointers == nullptr || other.sortedPointers == nullptr) return false;

                return (*sortedPointers == *other.sortedPointers);
            }

        template <typename N> 
            bool NodeIterator<N>::operator!=(const NodeIterator& other) const
            {
                return !operator==(other);
            }

        template <typename N> 
            NodeIterator<N>& NodeIterator<N>::operator++()
            {
                sortedPointers->pop_front();
                if (sortedPointers->size() == 0) sortedPointers = nullptr;
                return *this;
            }

    // EdgeIterator
    template<typename N, typename E>
        class EdgeIterator
        {
            public:
                
                // define the iterator traits

                // need to make this a const iterator
                typedef std::input_iterator_tag          iterator_category;
                typedef std::ptrdiff_t                   difference_type;
                typedef std::pair<N,E>                  value_type;
                typedef std::pair<N,E>*                 pointer;
                typedef std::pair<N,E>&                 reference;

                reference operator*() const;        
                pointer operator->() const;        

                //preincrement
                EdgeIterator& operator++(); 
                
                // post increment (does not return a reference)
                EdgeIterator operator++(int);

                bool operator==(const EdgeIterator& other) const;
                bool operator!=(const EdgeIterator& other) const;

                EdgeIterator(std::shared_ptr<std::list<std::pair<N, E> > > v = nullptr);

            private:
                std::shared_ptr<std::list<std::pair<N, E> > > sortedPointers;       



        };  // END EDGEITERATOR TEMPLATE
    // -------------------------------------------------------------------------------
    // Implementation of EdgeIterator 
    // -------------------------------------------------------------------------------

        // constructor
        template <typename N, typename E>       
            EdgeIterator<N, E>::EdgeIterator(std::shared_ptr<std::list<std::pair<N, E> > > v):sortedPointers{v} {}
                
            
        template <typename N, typename E>
            typename EdgeIterator<N, E>::reference EdgeIterator<N, E>::operator*() const
            {
                return sortedPointers->front();
            }

        template <typename N, typename E> 
            typename EdgeIterator<N, E>::pointer EdgeIterator<N, E>::operator->() const
            {
                return &(operator*());
            }

        
        template <typename N, typename E> 
            bool EdgeIterator<N, E>::operator==(const EdgeIterator& other) const
            {
                //    return sortedPointers == other.sortedPointers;

                if (sortedPointers == other.sortedPointers) return true;
                if (sortedPointers == nullptr || other.sortedPointers == nullptr) return false;

                return (*sortedPointers == *other.sortedPointers);
            }

        template <typename N, typename E> 
            bool EdgeIterator<N, E>::operator!=(const EdgeIterator& other) const
            {
                return !operator==(other);
            }

        template <typename N, typename E> 
            EdgeIterator<N, E>& EdgeIterator<N, E>::operator++()
            {
                sortedPointers->pop_front();
                if (sortedPointers->size() == 0) sortedPointers = nullptr;
                return *this;
            }

    template <typename N, typename E>
        class Graph
        {

            // public member variables and functions
            public:

                // BIG FIVE
                // default constructor
                Graph(){};
                //~Graph();

                // copy constructor
                Graph(const Graph &v);

                // move constructor
                Graph(const Graph &&v);

                // copy assignment
                Graph& operator=(const Graph &s);

                // move assignment
                Graph& operator=(const Graph &&i);


                bool addNode(const N&);
                bool addEdge(const N&, const N&, const E&);
                bool replace(const N&, const N&);
                void mergeReplace(const N&, const N&);
                void deleteNode(const N&) noexcept;
                void deleteEdge(const N&, const N&, const E&) noexcept;
                bool isNode(const N&) const;
                bool isConnected(const N&, const N&) const;
                
                void clear() noexcept;
                
                NodeIterator<N> begin() const;
                NodeIterator<N> end() const;

                EdgeIterator<N, E> edgeIteratorBegin(const N&) const;
                EdgeIterator<N, E> edgeIteratorEnd() const;

                void printNodes() const;
                void printEdges(const N&) const;
                
                
                // HELPER FUNCTIONS
                void printNValue(const N&);
                void printGraphStats();
                void printNodesVector(void) const;

                // determine if variable is in set.
                bool isInN_Set(const N&) const;
                bool isInE_Set(const E&) const;

                // get variable use count
                unsigned int getUseCountN(const N&) const;
                unsigned int getUseCountE(const E&) const;

                // get pointers to type
                std::shared_ptr<N> getNPointer(const N&) const;
                std::shared_ptr<E> getEPointer(const E&) const;

                // print connections to N
                void printNConnections(const N& n) const;

                // clean up expired pointers in the N/E sets
                void cleanSets();
                void cleanNodes();

            // private member functions and variables
            private:


                // private inner value-like class for Edge
                class Edge
                {

                    public:
                        Edge(const std::shared_ptr<N>&, const std::shared_ptr<N>&, const std::shared_ptr<E>&);
                        ~Edge();
                        N getOrigin() const;
                        void setOrigin(const std::shared_ptr<N>&);
                        void setDest(const std::shared_ptr<N>&);
                        N getDest() const;
                        E getValue() const;
                        
                        bool operator==(const Edge&) const;
                        bool operator!=(const Edge&) const;
                        bool compare(const E& e) const;

                        std::shared_ptr<N> getDestPtr() const;
                        std::shared_ptr<E> getValuePtr() const;
                        

                    private:

                        std::shared_ptr<N> origin_ptr_;
                        std::shared_ptr<N> dest_ptr_;
                        std::shared_ptr<E> value_ptr_;
                };

                // private inner value-like class for Node
                class Node
                {

                    public:
                        Node(const N& n);
                        ~Node();
                        N getOrigin() const;
                        std::shared_ptr<N> getOriginPtr() const;
                        bool operator==(const Node&) const;
                        bool operator!=(const Node&) const;
                        void addConnection(const std::weak_ptr<Edge>&);
                        bool containsEdge(const N&, const N&, const E&);
                        void copyConnections(const Node& old_n);
                        void printConnections(void) const;
                        void cleanNode();

                        std::vector<std::weak_ptr<Edge> > getConnections() const;

                        unsigned int getNumConnections() const;

                    private:

                        // PRIVATE DATA OF THE NODE CLASS
                        std::shared_ptr<N> origin_ptr_;
                        std::vector<std::weak_ptr<Edge> > connections_; 
                    
                };

                // private iterator class
               

                // XXX
                // return ref to a node in the node set
                auto getNode (const N& n) const -> Node;
                /// end XXX


                // check if node or edge is in set.
                bool nodeExists(const Node&) const;
                bool edgeExists(const Edge&) const;

                // function to update a nodes edges_ set. 
                bool updateNodeConnections(const N&, const std::weak_ptr<Edge>&);


                // PRIVATE MEMBER VARIABLES


                // weak pointers to sets of objects
                std::vector< std::weak_ptr<N> > N_set_;  // set of N elts
                std::vector< std::weak_ptr<E> > E_set_;  // set of E elts

                // connection set
                std::vector<Node> nodes_;

                // edge set
                std::vector< std::shared_ptr<Edge> > edges_; 


        }; // END GRAPH TEMPLATE

    // -------------------------------------------------------------------------------
    // Implementation of Graph class
    // -------------------------------------------------------------------------------

        // copy constructor
        template <typename N, typename E>
            Graph<N,E>::Graph(const Graph &g)
            {
               /*
                //std::cout<<"Copy constructor not complete" << std::endl;
                N_set_ = g.N_set_;  // set of N elts
                E_set_ = g.E_set_;  // set of E elts

                // connection set
                nodes_ = g.nodes_;

                // edge set
                edges_ = g.edges_;
                */
            }

        //move  constructor
        template <typename N, typename E>
            Graph<N,E>::Graph(const Graph &&g)
            {

                std::cout<<"move constructor not complete" << std::endl;
            }

        // copy assignment
        template <typename N, typename E>
            Graph<N,E>& Graph<N,E>::operator=(const Graph &g)
            {
                if(this != &g){
                    N_set_ = g.N_set_;  // set of N elts
                    E_set_ = g.E_set_;  // set of E elts

                    // connection set
                    nodes_ = g.nodes_;

                    // edge set
                    edges_ = g.edges_;
                std::cout<<"Copy assignment not complete" << std::endl;
                }

                return *this;
            }

        // move assignment
        template <typename N, typename E>
            Graph<N,E>& Graph<N,E>::operator=(const Graph &&g)
            {
                if(this != &g){
                }
                std::cout<<"move assignment not complete" << std::endl;
                return *this;
            }

        
        // if the element is not in set and is inserted, return true
        // else return false
        template <typename N, typename E>
            bool Graph<N,E>::addNode(const N& n)
            {

                // XXX
                // is this sufficient?
                if(isInN_Set(n)) return false;

                Node newNode{n};
                std::weak_ptr<N> wp = newNode.getOriginPtr();
                N_set_.push_back(wp);

                nodes_.push_back(newNode);

                return true;
            }

        template <typename N, typename E>
            bool Graph<N,E>::addEdge(const N& origin, const N& dest, const E& e)
            {

                // check for origin/dest node in N set
                if(!isInN_Set(origin)) throw std::runtime_error("ERROR: Graph::addEdge(): origin not in set.");
                if(!isInN_Set(dest)) throw std::runtime_error("ERROR: Graph::addEdge(): dest not in set.");

                // create our shared N pointers here 
                std::shared_ptr<N> origin_sh = getNPointer(origin);
                std::shared_ptr<N> dest_sh = getNPointer(dest);
                    
                std::shared_ptr<E> e_sh;

                // either get or create our shared E pointers here
                if(isInE_Set(e)){
                    //std::cout << "addEdge: e was IN E_set." << std::endl;// call getShared
                    e_sh = getEPointer(e);
                    //std::cout << "e_sh:" << *e_sh << std::endl;
                }else{
                    //std::cout << "addEdge: e was NOT in E_set." << std::endl;
                    e_sh = std::make_shared<E>(e);
                    std::weak_ptr<E> e_weak = e_sh;
                    E_set_.push_back(e_weak);
                }

                // make sure that e_sh has been allocated correctly. (needed?)
                if(e_sh == nullptr){
                    throw std::runtime_error("addEdge: e_sh is NULL");
                }

                // create a new edgeObject
                Edge newEdge{origin_sh, dest_sh, e_sh};

                if(edgeExists(newEdge)){
                    //std::cout << "addEdge: edge exists in set" << std::endl;
                    return false;
                }

                // add new edge to edges_ set
                std::shared_ptr<Edge> newEdge_ptr= std::make_shared<Edge>(newEdge);
                edges_.push_back(newEdge_ptr);

                
                // update node with weak pointer to edge
                std::weak_ptr<Edge> newEdge_wp = newEdge_ptr;
                updateNodeConnections(origin, newEdge_wp);
                

                // update edge count
                return true;
            }

        template <typename N, typename E>
            bool Graph<N,E>::replace(const N& old_n, const N& new_n)
            {
                
                // make sure that old_n is in the set
                if(!isInN_Set(old_n)) throw std::runtime_error("Graph<N,E>::replace(): old_n not in set.");

                // check to see if new_n already exists
                if(isInN_Set(new_n)) return false;
                
                // update pointer with new value.                
                *getNPointer(old_n) = new_n;

                return true;
            }

            // XXX




        template <typename N, typename E>
            void Graph<N,E>::mergeReplace(const N& old_n, const N& new_n)
            {

                // make sure that nodes to be modified are in set is in the set
                if(!isInN_Set(old_n)) throw std::runtime_error("Graph<N,E>::mergeReplace(): old_n not in set.");
                if(!isInN_Set(new_n)) throw std::runtime_error("Graph<N,E>::mergeReplace(): new_n not in set.");
            
                // make sure we are not trying to merge with ourself.
                if( old_n == new_n) return;


                // XXX - put the following into a function
                // grab old_n - we will later use this iterator to erase the 
                auto old_n_Node_it = std::find_if(nodes_.begin(), nodes_.end(),
                        [&old_n] (Node node)
                        {
                            if (node.getOrigin() == old_n) {
                                return true;
                            }
                            return false;
                        }
                        );
                if(old_n_Node_it == nodes_.end()) throw std::runtime_error("mergeReplace(): old_n_Node not found.");
                
                // grab new_n
                auto new_n_Node_it = std::find_if(nodes_.begin(), nodes_.end(),
                        [&new_n] (Node node)
                        {
                            if (node.getOrigin() == new_n) {
                                return true;
                            }
                            return false;
                        }
                        );
                if(new_n_Node_it == nodes_.end()) throw std::runtime_error("mergeReplace(): new_n_Node not found.");
     
                new_n_Node_it->copyConnections(*old_n_Node_it);

                // convert dest_ptr to new_n_Node_ptr

                

                // correct edge dest. values in edges that end on mergee
                for(auto sh_ptr: edges_){
                    if( (sh_ptr->getOrigin() != new_n_Node_it->getOrigin()) && (sh_ptr->getDest() == old_n_Node_it->getOrigin()) ) {
                        sh_ptr->setDest(new_n_Node_it->getOriginPtr());
                    }
                }
              

                auto remove_it = std::remove_if(edges_.begin(), edges_.end(),
                        [&old_n_Node_it] (const std::weak_ptr<Edge>& w_ptr)
                        {
                            if (auto sp = w_ptr.lock()) {
                                //std::cout << sp->getOrigin() << "->" << sp->getDest() << std::endl;
                                if( (sp->getOrigin() == old_n_Node_it->getOrigin()) 
                                    || (sp->getDest() == old_n_Node_it->getOrigin()) ){
                                    //std::cout<<"removing: " << sp->getOrigin() << "->" << sp->getDest() << std::endl;
                                    return true;
                                }
                            } 
                                return false;
                            }
                            );

                // remove left over edges/node
                nodes_.erase(old_n_Node_it);
                edges_.erase(remove_it, edges_.end());
                
                cleanSets();
                cleanNodes();
            }





        template <typename N, typename E>
            void Graph<N,E>::deleteNode(const N& n) noexcept
            {
                
                if(!isInN_Set(n)) return;

                auto old_node = std::find_if(nodes_.begin(), nodes_.end(),
                        [&n] (Node node)
                        {
                            if (node.getOrigin() == n) {
                                return true;
                            }
                            return false;
                        }
                        );
                
                auto remove_it = std::remove_if(edges_.begin(), edges_.end(),
                        [&n] (const std::weak_ptr<Edge>& w_ptr)
                        {
                            if (auto sp = w_ptr.lock()) {
                                if( (sp->getOrigin() == n) || (sp->getDest() == n) ){
                                    return true;
                                }
                            } 
                                return false;
                            }
                            );

                // remove left over edges/node
                nodes_.erase(old_node);
                edges_.erase(remove_it, edges_.end());
                
                //std::cout<< "Cleaning nodes"<<std::endl;
                cleanSets();
                cleanNodes();
            }
            
        template <typename N, typename E>
            void Graph<N,E>::deleteEdge(const N& origin, const N& dest, const E& e) noexcept
            {
        
        
                
                if(!isInN_Set(origin)) return;
                if(!isInN_Set(dest)) return;
                if(!isInE_Set(e)) return;
                
                auto old_edge = std::find_if(edges_.begin(), edges_.end(),
                        [&origin, &dest, &e] (const std::weak_ptr<Edge>& w_ptr)
                        {
                            if (auto sp = w_ptr.lock()) {
                                if( (sp->getOrigin() == origin) 
                                    && (sp->getDest() == dest) 
                                    && (sp->getValue() == e)){
                                    return true;
                                }
                            } 
                                return false;
                        }
                        );
                edges_.erase(old_edge);
                cleanSets();
                cleanNodes();
            }
                
        template <typename N, typename E>
            bool Graph<N,E>::isNode(const N& n) const
            {
                return isInN_Set(n);
            }

        template <typename N, typename E>
            bool Graph<N,E>::isConnected(const N& origin, const N& dest) const
            {
                // make sure that nodes to be modified are in set is in the set
                if(!isInN_Set(origin)) throw std::runtime_error("Graph<N,E>::isConnected(): origin not in set.");
                if(!isInN_Set(dest  )) throw std::runtime_error("Graph<N,E>::isConnected(): dest not in set.");
            
                auto connection = std::find_if(edges_.begin(), edges_.end(),
                        [&origin, &dest] (const std::weak_ptr<Edge>& w_ptr)
                        {
                            if (auto sp = w_ptr.lock()) {
                                if( (sp->getOrigin() == origin) 
                                    && (sp->getDest() == dest) ){
                                    return true;
                                }
                            } 
                                return false;
                        }
                        );

                if(connection == edges_.end()){
                    return false;
                }
             

                return true;
            }

        template <typename N, typename E>
            void Graph<N,E>::clear() noexcept
            {
                nodes_.clear();
                edges_.clear();

                N_set_.clear();
                E_set_.clear();
            }



        template <typename N, typename E>
            void Graph<N,E>::printNodes() const
            {
                //std::cout << "printing sorted N values" << std::endl;
                for (auto first = begin(); first != end(); ++first){
                    std::cout << *first << std::endl;
                } 
            }
        

        template <typename N, typename E>
            NodeIterator<N> Graph<N,E>::begin() const
            {
                
                // create temp vector of nodes to sort over
                std::vector<Node> toSort(nodes_);

                std::sort(toSort.begin(), toSort.end(),
                    [](const Node& a, const Node& b)
                    {
                        if(a.getNumConnections() == b.getNumConnections()){
                            return a.getOrigin() < b.getOrigin();
                        } else {
                            return a.getNumConnections() > b.getNumConnections();
                        }

                    });
                
                std::list<std::shared_ptr<N> > sortedN;
                for(auto elt : toSort){
                    sortedN.push_back(elt.getOriginPtr());
                } 

                if(sortedN.size() == 0){
                    return NodeIterator<N>(); 
                }
                return NodeIterator<N>(std::make_shared<std::list<std::shared_ptr<N> > >(sortedN));
            }
        
        
        template <typename N, typename E>
            NodeIterator<N> Graph<N,E>::end() const
            {
                // return a nullptr iterator type
                return NodeIterator<N>();
            }                



        template <typename N, typename E>
            void Graph<N,E>::printEdges(const N& n) const
            {
                if(!isInN_Set(n)) throw std::runtime_error("Graph<N,E>::printEdges(): n not in set.");
                
                std::cout << "Edges attached to Node " << n << std::endl;
            
                auto itStart = edgeIteratorBegin(n);
                auto itEnd =  edgeIteratorEnd();
                
                if( itStart == itEnd ){
                    std::cout << "(null)" << std::endl;
                    return;    
                }
                
                for (auto elt = itStart; elt != itEnd; ++elt){
                    std::cout << elt->first << " " << elt->second << std::endl;
                } 
            }





        template <typename N, typename E>
            EdgeIterator<N, E> Graph<N,E>::edgeIteratorBegin(const N& n) const
            {
                
                // find node
                auto node = std::find_if(nodes_.begin(), nodes_.end(),
                        [&n] (Node node)
                        {
                            if (node.getOrigin() == n) {
                                return true;
                            }
                            return false;
                        }
                        );

                if(node == nodes_.end()) throw std::runtime_error("Graph<N,E>::edgeIteratorBegin(): n not in set.");

                // make a copy of its connections set.
                std::vector<std::weak_ptr<Edge> > connections = node->getConnections();

                std::sort(connections.begin(), connections.end(),
                    [](const std::weak_ptr<Edge>& a, const std::weak_ptr<Edge>& b)
                    {
                        if ( a.expired() || b.expired() ){
                            return false;
                        }

                        auto a_sp = a.lock();
                        auto b_sp = b.lock();

                        if(a_sp->compare(b_sp->getValue())){
                            return a_sp->getDest() < b_sp->getDest();
                        } else {
                            return a_sp->getValue() < b_sp->getValue();
                        }
                        
                        
                    });
                
                std::list< std::pair<N,E> > sortedEdges;

              
                for(auto elt : connections){
                    if(auto elt_sp = elt.lock()){
                        std::pair<N,E> newEdge(elt_sp->getDest(), elt_sp->getValue());
                        sortedEdges.push_back(newEdge);

                    } else {
                        throw std::runtime_error("edgeIteratorBegin(): invalid edge in connections set");
                    }

                }
    
                // create temp vector of nodes to sort over
                std::vector<Node> toSort(nodes_);

                std::sort(toSort.begin(), toSort.end(),
                    [](const Node& a, const Node& b)
                    {
                        if(a.getNumConnections() == b.getNumConnections()){
                            return a.getOrigin() < b.getOrigin();
                        } else {
                            return a.getNumConnections() > b.getNumConnections();
                        }

                    });
                std::list<std::shared_ptr<N> > sortedN;
                for(auto elt : toSort){
                    sortedN.push_back(elt.getOriginPtr());
                } 


                if(sortedEdges.size() == 0) return EdgeIterator<N, E>();
                
                return EdgeIterator<N, E>(std::make_shared<std::list< std::pair<N,E> > >(sortedEdges));
            }




        template <typename N, typename E>
            EdgeIterator<N, E> Graph<N,E>::edgeIteratorEnd() const
            {
                return EdgeIterator<N, E>();
            }


    // -------------------------------------------------------------------------------
    // GRAPH HELPER FUNCTIONS
    // -------------------------------------------------------------------------------

        template <typename N, typename E>
            void Graph<N,E>::cleanNodes()
            {
                for(Graph<N,E>::Node &n: nodes_){
                    n.cleanNode();
                }
            }

        // print connections on node N
        template <typename N, typename E>
            void Graph<N,E>::printNConnections(const N& n) const
            {
                auto it = std::find_if(nodes_.begin(), nodes_.end(),
                    [&n] (Node node)
                    {
                        if (node.getOrigin() == n) {
                            return true;
                        }
                        return false;
                    }
                    );
                if(it == nodes_.end()) throw std::runtime_error("printNConnections(): old_n_Node not found.");
                
                it->printConnections();
            }

        // print an N value
        template <typename N, typename E>
            void Graph<N,E>::printNValue(const N& n)
            {
                // scan through weak pointers to determine if element N already in set.
                auto it = std::find_if(N_set_.begin(), N_set_.end(),
                        [&n] (const std::weak_ptr<N>& w_ptr)
                        {
                            if (auto sp = w_ptr.lock()) {
                                return (n == *sp);
                            }
                            return false;
                        }
                        );

                    if(it != N_set_.end()){
                        std::shared_ptr<N> sp = it->lock();
                        //std::cout << "value at n: " << *sp << std::endl;
                        return;
                    }
            }

        // print stats about the graph
        template <typename N, typename E>
            void Graph<N,E>::printGraphStats()
            {
                std::cout << "Number of nodes: " << nodes_.size() << std::endl;
                std::cout << "Number of edges: " << edges_.size() << std::endl;
            }

        template <typename N, typename E>
            bool Graph<N,E>::isInN_Set(const N& n) const
            {
                auto it = std::find_if(N_set_.begin(), N_set_.end(),
                        [&n] (const std::weak_ptr<N>& w_ptr)
                        {
                            if (auto sp = w_ptr.lock()) {
                               return (n == *sp);
                            }
                            return false;
                        }
                        );

                // element has been found and we stopped short of end of set
                if(it != N_set_.end()){
                    return true;
                }

                // iterator at end of set, n not found
                return false;
            }


        template <typename N, typename E>
            bool Graph<N,E>::isInE_Set(const E& e) const
            {
                auto it = std::find_if(E_set_.begin(), E_set_.end(),
                        [&e] (const std::weak_ptr<E>& w_ptr)
                        {
                            if (auto sp = w_ptr.lock()) {
                               return (e == *sp);
                            }
                            return false;
                        }
                        );

                // element has been found and we stopped short of end of set
                if(it != E_set_.end()){
                    return true;
                }

                // iterator at end of set, e not found
                return false;
            }

        template <typename N, typename E>
            unsigned int Graph<N,E>::getUseCountN(const N& n) const
            {
                auto it = std::find_if(N_set_.begin(), N_set_.end(),
                        [&n] (const std::weak_ptr<N>& w_ptr)
                        {
                            if (auto sp = w_ptr.lock()) {
                               return (n == *sp);
                            }
                            return false;
                        }
                        );

                // element has been found and we stopped short of end of set
                if(it != N_set_.end()){
                    std::shared_ptr<N> sp = it->lock();
                    unsigned int count = sp.use_count() - 1; 
                    return (count);
                }

                throw std::runtime_error("ERROR: Graph::getUseCountE(): E not in set.");
            }

        template <typename N, typename E>
            unsigned int Graph<N,E>::getUseCountE(const E& e) const
            {
                auto it = std::find_if(E_set_.begin(), E_set_.end(),
                        [&e] (const std::weak_ptr<E>& w_ptr)
                        {
                            if (auto sp = w_ptr.lock()) {
                               return (e == *sp);
                            }
                            return false;
                        }
                        );

                // element has been found and we stopped short of end of set
                if(it != E_set_.end()){
                    std::shared_ptr<E> sp = it->lock();
                    unsigned int count = sp.use_count() - 1; 
                    return (count);
                }

                throw std::runtime_error("ERROR: Graph::getUseCountE(): E not in set.");
            }

        template <typename N, typename E>
            std::shared_ptr<N> Graph<N,E>::getNPointer(const N& n) const
            {
                auto it = std::find_if(N_set_.begin(), N_set_.end(),
                        [&n] (const std::weak_ptr<N>& w_ptr)
                        {
                            if (auto sp = w_ptr.lock()) {
                               return (n == *sp);
                            }
                            return false;
                        }
                        );

                // element has been found and we stopped short of end of set
                if(it == N_set_.end()){
                    throw std::runtime_error("ERROR: Graph::getNPointer(): N not in set.");
                }

                return it->lock(); 
            }
 
        template <typename N, typename E>
            std::shared_ptr<E> Graph<N,E>::getEPointer(const E& e) const
            {
                auto it = std::find_if(E_set_.begin(), E_set_.end(),
                        [&e] (const std::weak_ptr<E>& w_ptr)
                        {
                            if (auto sp = w_ptr.lock()) {
                               return (e == *sp);
                            }
                            return false;
                        }
                        );

                // element has been found and we stopped short of end of set
                if(it == E_set_.end()){
                    throw std::runtime_error("ERROR: Graph::getEPointer(): E not in set.");
                }

                return it->lock();   
            }

        template <typename N, typename E>
            bool Graph<N,E>::nodeExists(const Node& node) const
            {
                auto it = std::find_if(nodes_.begin(), nodes_.end(),
                        [&node] (const std::weak_ptr<Node>& w_ptr)
                        {
                            if (auto sp = w_ptr.lock()) {
                               return (node == *sp);
                            }
                            return false;
                        }
                        );

                // element has been found and we stopped short of end of set
                if(it != nodes_.end()){
                    return true;
                }

                // iterator at end of set, n not found
                return false;
            }
            
        template <typename N, typename E>
            bool Graph<N,E>::edgeExists(const Edge& edge) const
            {
                
                auto it = std::find_if(edges_.begin(), edges_.end(),
                        [&edge] (const std::weak_ptr<Edge>& w_ptr)
                        {
                            if (auto sp = w_ptr.lock()) {
                               return (edge == *sp);
                                }
                                return false;
                            }
                            );

                // element has been found and we stopped short of end of set
                if(it != edges_.end()){
                    return true;
                }

                // iterator at end of set, n not found
                return false;
            }

        template <typename N, typename E>
            bool Graph<N,E>::updateNodeConnections(const N& n, const std::weak_ptr<Edge>& e)
            {
                auto it = std::find_if(nodes_.begin(), nodes_.end(),
                        [&n] (Node curr_node)
                        {
                            return curr_node.getOrigin() == n;
                        });

                // element has been found and we stopped short of end of set
                if(it == nodes_.end()){
                    return false;
                }

                it->addConnection(e);

                
                return true;
            }

        // remove expired pointers from N/E sets
        template <typename N, typename E>
            void Graph<N,E>::cleanSets()
            {
                // need to clean up N and E sets
                // scan through weak pointers to determine if element N already in set.
                auto remove_N = std::remove_if(N_set_.begin(), N_set_.end(),
                        [] (const std::weak_ptr<N>& w_ptr)
                        {
                            if (w_ptr.expired()){ 
                                //std::cout << "expired N_set pointer found." <<std::endl; 
                                return true;
                            }
                            return false;
                        }
                        );

                N_set_.erase(remove_N, N_set_.end());

                auto remove_E = std::remove_if(E_set_.begin(), E_set_.end(),
                        [] (const std::weak_ptr<E>& w_ptr)
                        {
                            if (w_ptr.expired()){ 
                                //std::cout << "expired E_set pointer found." <<std::endl; 
                                return true;
                            }
                            return false;
                        }
                        );

                E_set_.erase(remove_E, E_set_.end());
            }

    // -------------------------------------------------------------------------------
    // Implementation of Node class
    // -------------------------------------------------------------------------------


        // node constructor
        template <typename N, typename E>
            Graph<N,E>::Node::Node(const N& n)
            {
                //std::cout << "creating node: "<< n << std::endl;
                origin_ptr_ = std::make_shared<N>(n);
            }

        template <typename N, typename E>
            Graph<N,E>::Node::~Node()
            {
                origin_ptr_.reset();
            }

        template <typename N, typename E>
            void Graph<N,E>::Node::cleanNode()
            {
                auto remove_it = std::remove_if(connections_.begin(), connections_.end(),
                        [] (const std::weak_ptr<Edge>& w_ptr)
                        {
                            if (w_ptr.expired()){ 
                                //std::cout << "expired edge found." <<std::endl; 
                                return true;
                            }
                            return false; 
                        }
                        );
                connections_.erase(remove_it, connections_.end());
            }


        template <typename N, typename E>
            N Graph<N,E>::Node::getOrigin() const
            {
                return *origin_ptr_;
            }

        template <typename N, typename E>
            std::shared_ptr<N> Graph<N,E>::Node::getOriginPtr() const
            {
                return origin_ptr_;
            }

        // equality operator for edge types
        template <typename N, typename E>
            bool Graph<N,E>::Node::operator==(const Node& n) const
            {
                if( (n.origin_ptr_ == nullptr) || (origin_ptr_ == nullptr) ){
                    throw std::runtime_error("Node == operator: null value encountered.");
                }

                return ( (*n.origin_ptr_ == *origin_ptr_) && (n.connections_ == connections_) );
            }

        // in-equality operator for edge types
        template <typename N, typename E>
            bool Graph<N,E>::Node::operator!=(const Node& n) const
            {
                if( (n.origin_ptr_ == nullptr) || (origin_ptr_ == nullptr) ){
                    throw std::runtime_error("Node == operator: null value encountered.");
                }

                return !((*n.origin_ptr_ == *origin_ptr_) && (n.connections_ == connections_));
            }
        

        template <typename N, typename E>
            void Graph<N,E>::Node::addConnection(const std::weak_ptr<Edge>& e)
            {
                connections_.push_back(e);
            }

        template <typename N, typename E>
            bool Graph<N,E>::Node::containsEdge(const N& origin, const N& dest, const E& value)
            {
               auto it = std::find_if(connections_.begin(), connections_.end(),
                    [&origin, &dest, &value] (const std::weak_ptr<Edge>& w_ptr)
                    {
                        if (auto sp = w_ptr.lock()) {
                           return ( (sp->getOrigin() == origin) 
                            && (sp->getDest() == dest) && (sp->getValue() == value));
                        }
                        return false;
                    }
                    );

                // element has been found and we stopped short of end of set
                if(it == connections_.end()){
                    return false;
                }

                return true; 
            }

        template <typename N, typename E> 
            void Graph<N,E>::Node::printConnections(void) const
            {
                std::cout<< connections_.size() << " connections on: "<< *origin_ptr_ << std::endl;    
                for (auto w_ptr : connections_)
                {
                    //if(w_ptr.expired()) std::cout << "expired edge detected!" << std::endl;
                    if (auto sp = w_ptr.lock()) {
                       std::cout << "[ "<< sp->getOrigin() << "->" << sp->getDest()
                        << ": " << sp->getValue() << " ]" << std::endl;
                    }
                }
            }

        // XXX
        // copy (and modify) connections from one node to another, 
        // this will not copy duplicates or invalid edges (edge to itself)'
        template <typename N, typename E> 
            void Graph<N,E>::Node::copyConnections(const Node& old_n)
            {
                // iterate through the new_n
                // if an edge dest == old_n origin, remove
                auto it = std::remove_if(connections_.begin(), connections_.end(),
                    [&old_n] (const std::weak_ptr<Edge>& w_ptr)
                    {
                        if (auto sp = w_ptr.lock()) {
                            //std::cout<< sp->getDest() << "==" << old_n.getOrigin() << std::endl;
                            if(sp->getDest() == old_n.getOrigin()){
                                //std::cout << "removing edge: " <<  sp->getDest() << std::endl;
                                return true;
                            }
                        }
                        return false;
                    }
                    );
                connections_.erase(it, connections_.end());

          
                // copy over valid* edges
                // valid: non-duplicate, edges not between merging nodes. 
                for (auto w_ptr : old_n.connections_)
                {
                    if (auto sp = w_ptr.lock()) {
                        
                        // check if the current edge is between 
                        if( (sp->getDest() != *origin_ptr_) 
                            && (!containsEdge(*origin_ptr_, sp->getDest(), sp->getValue() ))) {
                            

                            // change origin of node
                            sp->setOrigin(origin_ptr_);
                            //std::cout << "pushing edge with dest: "<< sp->getDest() << std::endl;
                            
                            connections_.push_back(w_ptr);
                        } 
                    }
                }
            }

        template <typename N, typename E>
            unsigned int Graph<N,E>::Node::getNumConnections() const
            {
                return connections_.size();
            }

        template <typename N, typename E>
            std::vector<std::weak_ptr<typename Graph<N,E>::Edge> > Graph<N,E>::Node::getConnections() const
            {
                return connections_;
            }



    // -------------------------------------------------------------------------------
    // Implementation of Edge class
    // -------------------------------------------------------------------------------

        // edge constructor
        template <typename N, typename E>
            Graph<N,E>::Edge::Edge(const std::shared_ptr<N>& origin, const std::shared_ptr<N>& dest, const std::shared_ptr<E>& e)
            {
                origin_ptr_ = origin;
                dest_ptr_ = dest;
                value_ptr_ = e;
            }

        // edge destructor

        template <typename N, typename E>
            Graph<N,E>::Edge::~Edge()
            {
                origin_ptr_.reset();
                dest_ptr_.reset();
                value_ptr_.reset();
            }

        // return edge origin value
        template <typename N, typename E>
            N Graph<N,E>::Edge::getOrigin() const
            {
                return *origin_ptr_;
            }

        template <typename N, typename E>
            void Graph<N,E>::Edge::setOrigin(const std::shared_ptr<N>& n)
            {
                origin_ptr_ = n;
            }

        template <typename N, typename E>
            void Graph<N,E>::Edge::setDest(const std::shared_ptr<N>& n)
            {
                dest_ptr_ = n;
            }

        // return edge dest value
        template <typename N, typename E>
            N Graph<N,E>::Edge::getDest() const
            {
                return *dest_ptr_;
            }

        // return edge dest pointer
        template <typename N, typename E>
            std::shared_ptr<N> Graph<N,E>::Edge::getDestPtr() const
            {
                return dest_ptr_;
            }


        // return edge value
        template <typename N, typename E>
            E Graph<N,E>::Edge::getValue() const
            {
                return *value_ptr_;
            }

        // return edge value
        template <typename N, typename E>
            std::shared_ptr<E> Graph<N,E>::Edge::getValuePtr() const
            {
                return value_ptr_;
            }



        // equality operator for edge types
        template <typename N, typename E>
            bool Graph<N,E>::Edge::operator==(const Edge& e) const
            {
                if( (e.origin_ptr_ == nullptr) || (origin_ptr_ == nullptr) 
                    || (e.dest_ptr_ == nullptr) || (dest_ptr_ == nullptr)
                    || (e.value_ptr_ == nullptr) || (value_ptr_ == nullptr)){
                    throw std::runtime_error("Edge == operator: null value encountered.");
                }

                return ( (*e.origin_ptr_ == *origin_ptr_) && (*e.dest_ptr_ == *dest_ptr_) && (*e.value_ptr_ == *value_ptr_) );
            }

        // in-equality operator for edge types
        template <typename N, typename E>
            bool Graph<N,E>::Edge::operator!=(const Edge& e) const
            {
                if( (e.origin_ptr_ == nullptr) || (origin_ptr_ == nullptr) 
                    || (e.dest_ptr_ == nullptr) || (dest_ptr_ == nullptr)
                    || (e.value_ptr_ == nullptr) || (value_ptr_ == nullptr)){
                    throw std::runtime_error("Edge != operator: null value encountered.");
                }
                return !( (*e.origin_ptr_ == *origin_ptr_) && (*e.dest_ptr_ == *dest_ptr_) && (*e.value_ptr_ == *value_ptr_));
            }

        // return true if equal
        template <typename N, typename E >
            bool Graph<N,E>::Edge::compare(const E& e) const
            {
                if( (*value_ptr_ < e) || (e < *value_ptr_) ) {
                    return false;
                } 


                return true;
            }

    }

#endif	// _GRAPH_H
