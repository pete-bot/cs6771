// collected set of official graph tests.

#include "Graph.h"
#include "Graph.h"

#include <cassert>

//official tests
void test_1(void);
void test_2(void);
void test_3(void);
void test_4(void);
void test_5(void);
void test_6(void);
void test_7(void);
void test_8(void);
void test_9(void);
void test_10(void);
void test_11(void);
void test_12(void);
void test_13(void);
void test_14(void);

// unofficial tests
void test_u15(void);
void test_u16(void);
void test_u17(void);
void test_u18(void);
void test_u19(void);


int main(int argc, char* argv[]) {

    std::cout << "STARTING OFFICIAL TESTS..." << std::endl;
    
    /*

    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
    test_6();
    test_7();
        
    test_8();       // error here: references to nodes in orig. graph are not being decreased properly?
    test_9();       // as with test8
    
    test_10();
    test_11();
    test_12();
    test_13();
    test_14();
    

    */
    
    std::cout << "OFFICIAL TESTS... PASSED" << std::endl;


//    std::cout << "STARTING UN-OFFICIAL TESTS..." << std::endl;
//    test_u15();
//    test_u16();
//    test_u17();
//    test_u18();
//    test_u19();   
//    std::cout << "UN-OFFICIAL TESTS... PASSED" << std::endl;


    return 0;
}

// test 1: tests graph construction and node insertion 
void test_1(void)
{

    std::cout << ">>>> Running official test 1: " << std::endl;

    // create 3 graphs
    cs6771::Graph<int,int> g;
    cs6771::Graph<std::string,double> g2{};
    cs6771::Graph<std::shared_ptr<int>,std::string> g3{};
    
    // add some nodes to each graph. 
    g.addNode(1);
    int i = 2;
    g.addNode(i);
    double d = 3.41;
    g.addNode(static_cast<int>(d));
    
    std::string s = "world";
    g2.addNode(s);
    g2.addNode("Hello");
    
    std::shared_ptr<int> sp = std::make_shared<int>(5);
    g3.addNode(sp);
    g3.addNode(std::make_shared<int>(6));
    
    // print the nodes from each graph. 
    std::cout << "Graph g nodes:" << std::endl;
    g.printNodes();
    std::cout << "Graph g2 nodes:" << std::endl;
    g2.printNodes();
    std::cout << "isNode test:" << std::endl;
    std::cout << std::boolalpha << g3.isNode(sp) << std::endl;

    std::cout << ">>>> official test 1: PASSED " << std::endl<< std::endl;
}


// test 2: tests edge insertion and print ordering
void test_2(void)
{

    std::cout << ">>>> Running official test 2: " << std::endl;

    // create 3 graphs
    cs6771::Graph<int,int> g;
    cs6771::Graph<std::string,double> g2{};
    cs6771::Graph<std::shared_ptr<int>,std::string> g3{};
    
    // add some nodes to each graph. 
    g.addNode(1);
    int i = 2;
    g.addNode(i);
    double d = 3.41;
    g.addNode(static_cast<int>(d));
    
    g2.addNode("Hello");
    std::string s = "world";
    g2.addNode(s);
    
    std::shared_ptr<int> sp = std::make_shared<int>(5);
    g3.addNode(sp);
    g3.addNode(std::make_shared<int>(6));
    
    // add some edges 
    g.addEdge(2,1,3);
    int j = 3;
    g.addEdge(i,j,1);
    
    g2.addEdge("Hello","world",d);
    
    g.printEdges(2);
    g2.printEdges("Hello");
    g2.printEdges("world");
    
    std::cout << "Printing nodes in graph g to check print order" << std::endl;
    g.printNodes();

    std::cout << ">>>> official test 2: PASSED " << std::endl<< std::endl;
}

// test 3: tests error handling
void test_3(void)
{

    std::cout << ">>>> Running official test 3: " << std::endl;

    // create 3 graphs
    cs6771::Graph<int,int> g;
    cs6771::Graph<std::string,double> g2{};
    cs6771::Graph<std::shared_ptr<int>,std::string> g3{};
    
    // add some nodes to each graph. 
    g.addNode(1);
    int i = 2;
    g.addNode(i);
    double d = 3.41;
    g.addNode(static_cast<int>(d));
    
    g2.addNode("Hello");
    std::string s = "world";
    g2.addNode(s);
    
    std::shared_ptr<int> sp = std::make_shared<int>(5);
    g3.addNode(sp);
    g3.addNode(std::make_shared<int>(6));
    
    // try to add some duplicated data
    i = 1;
    std::cout << "testing adding duplicate data" << std::endl;
    std::cout << std::boolalpha << g.addNode(i) << std::endl;
    std::cout << g2.addNode("Hello") << std::endl;
    std::cout << g3.addNode(sp) << " " << g3.addNode(std::make_shared<int>(6)) << std::endl;
    
    // add some edges 
    g.addEdge(2,1,3);
    int j = 3;
    g.addEdge(i,j,1);
    g2.addEdge("Hello","world",d);
    
    std::cout << "testing adding duplicate edges" << std::endl;
    // try to add some duplicated edges
    std::cout << g.addEdge(2,1,3) << std::endl;
    // try to add an edge with a different weight
    std::cout << g.addEdge(2,1,2) << std::endl;
    
    std::cout << "testing adding edges between nodes not in the graph" << std::endl;
    // try to add an edge to somewhere not in the graph
    try {
        g.addEdge(7,1,3);
    } catch( const std::exception &ex ) {
        //std::cerr << ex.what() << std::endl;
        std::cout << "exception caught" << std::endl;
    }
    try {
        g.addEdge(2,7,3);
    } catch( const std::exception &ex ) {
        std::cout << "exception caught" << std::endl;
    }
    
    std::cout << "testing isConnected" << std::endl;
    try {
        std::cout << g2.isConnected("world","Hello") << std::endl;
        std::cout << g2.isConnected("hello","pluto") << std::endl;
    } catch( const std::exception &ex ) {
        std::cout << "exception caught" << std::endl;
    }
    
    std::cout << "testing print with an unknown node" << std::endl;
    try {
        g.printEdges(5);
    } catch( const std::exception &ex ) {
        std::cout << "exception caught" << std::endl;
    }

    std::cout << ">>>> official test 3: PASSED " << std::endl<< std::endl;
}


// test 4: tests data integrity 
void test_4(void)
{   

    std::cout << ">>>> Running official test 4: " << std::endl;

    cs6771::Graph<std::string,int> gCopy;
    
    // create some data to store as nodes.
    std::string s = "a";
    std::string t = "b";
    std::string u = "c";
    
    // add this data into the graph
    gCopy.addNode(s);
    gCopy.addNode(t);
    gCopy.addNode(u);
    
    gCopy.addEdge(u,t,1);
    gCopy.addEdge(u,t,2);
    
    std::cout << "Graph g before attempted change" << std::endl;
    gCopy.printNodes();
    // change the value of t and make sure that the graph still prints out the original value
    t = "d";
    std::cout << "Graph g after attempted change" << std::endl;
    gCopy.printNodes();
    
    cs6771::Graph<std::shared_ptr<std::string>,std::shared_ptr<int>> gPtr;
    std::shared_ptr<std::string> sPtr = std::make_shared<std::string>("a");
    std::shared_ptr<std::string> tPtr = std::make_shared<std::string>("b");
    std::shared_ptr<std::string> uPtr = std::make_shared<std::string>("c");
    gPtr.addNode(sPtr);
    gPtr.addNode(tPtr);
    gPtr.addNode(uPtr);
    
    // add an edge between u and t
    gPtr.addEdge(uPtr,tPtr,std::make_shared<int>(1));
    // add a second edge between u and t with a different weight
    gPtr.addEdge(uPtr,tPtr,std::make_shared<int>(2));
    
    // change the value of the data in the ptr
    *tPtr = "d";
    
    std::cout << "Confirming that data has changed if we are using ptrs" << std::endl;
    // this should have updated in the graph as well as it is a pointer. 
    // can confirm this using isNode
    std::cout << std::boolalpha << gCopy.isNode(t) << " " << gPtr.isNode(tPtr) << std::endl;

    std::cout << ">>>> official test 4: PASSED " << std::endl<< std::endl;
}

// test 5: tests replace data
void test_5(void)
{
    

    std::cout << ">>>> Running official test 5: " << std::endl;


    cs6771::Graph<std::string,int> gCopy;
    
    // create some data to store as nodes.
    std::string s = "a";
    std::string t = "b";
    std::string u = "c";
    
    // add this data into the graph
    gCopy.addNode(s);
    gCopy.addNode(t);
    gCopy.addNode(u);
    
    gCopy.addEdge(u,t,1);
    gCopy.addEdge(u,t,2);
    
    std::cout << "Graph before node replacement" << std::endl;
    gCopy.printNodes();
    
    // replace node
    gCopy.replace("a","e");
    std::cout << "Graph after node replacement" << std::endl;
    gCopy.printNodes();
    
    std::cout << "trying to replace node with an existing node" << std::endl;
    std::cout << std::boolalpha << gCopy.replace("b","c") << std::endl;
    
    std::cout << "trying to replace node with a node not in the graph" << std::endl;
    try {
        gCopy.replace("a","d");
    } catch( const std::exception &ex ) {
        //std::cerr << ex.what() << std::endl;
        std::cout << "exception caught" << std::endl;
    }
    std::cout << ">>>> official test 5: PASSED " << std::endl<< std::endl;
}

// test 6: tests merge replace
void test_6(void)
{

    std::cout << ">>>> Running official test 6: " << std::endl;

    cs6771::Graph<std::string,int> gCopy;
    
    // add this data into the graph
    gCopy.addNode("a");
    gCopy.addNode("b");
    gCopy.addNode("c");
    gCopy.addNode("d");
    
    gCopy.addEdge("b","a",3);
    gCopy.addEdge("b","a",5);
    gCopy.addEdge("c","a",3);
    
    std::cout << "Graph before node merge" << std::endl;
    gCopy.printNodes();
    gCopy.printEdges("b");
    gCopy.printEdges("c");
    gCopy.mergeReplace("b","c");
    std::cout << "Graph after node merge" << std::endl;
    gCopy.printNodes();
    gCopy.printEdges("c");
    
    std::cout << "checking that node b has been destroyed" << std::endl;
    try {
        gCopy.mergeReplace("b","c");
    } catch( const std::exception &ex ) {
        std::cout << "exception caught" << std::endl;
    }
    
    try {
        gCopy.mergeReplace("c","b");
    } catch( const std::exception &ex ) {
        std::cout << "exception caught" << std::endl;
    }
    std::cout << std::boolalpha << gCopy.isNode("b") << std::endl;

    std::cout << ">>>> official test 6: PASSED " << std::endl<< std::endl;
}

// test 7: tests deleting data
void test_7(void)
{

    std::cout << ">>>> Running official test 7: " << std::endl;

    cs6771::Graph<std::string,int> gCopy;
    
    // add this data into the graph
    gCopy.addNode("a");
    gCopy.addNode("b");
    gCopy.addNode("c");
    gCopy.addNode("d");
    
    gCopy.addEdge("b","a",3);
    gCopy.addEdge("b","a",5);
    gCopy.addEdge("c","a",3);
    
    std::cout << "Graph before node/edge deletion" << std::endl;
    gCopy.printNodes();
    gCopy.printEdges("b");
    gCopy.printEdges("c");
    
    std::cout << "deleteing edge between b and a" << std::endl;
    gCopy.deleteEdge("b","a",5);
    gCopy.printEdges("b");
    
    std::cout << "deleting node a" << std::endl;
    gCopy.deleteNode("a");
    gCopy.printEdges("b");
    std::cout << "Graph after node/edge deletion" << std::endl;
    gCopy.printNodes();
    
    try {
        gCopy.deleteNode("a");
    } catch( const std::exception &ex ) {
        std::cout << "exception caught 1" << std::endl;
    }
    
    try {
        gCopy.deleteEdge("c","a",5);
    } catch( const std::exception &ex ) {
        std::cout << "exception caught 2" << std::endl;
    }
    std::cout << ">>>> official test 7: PASSED " << std::endl<< std::endl;
}

// test 8: tests copy construction
void test_8(void)
{

    std::cout << ">>>> Running official test 8: " << std::endl;

    auto gHeap = new cs6771::Graph<std::string,int>{};
    
    // add this data into the graph
    gHeap->addNode("a");
    gHeap->addNode("b");
    gHeap->addNode("c");
    gHeap->addNode("d");
    
    gHeap->addEdge("b","a",3);
    gHeap->addEdge("b","a",5);
    gHeap->addEdge("b","d",4);
    gHeap->addEdge("c","a",3);
    
    std::cout << "original graph" << std::endl;
    gHeap->printNodes();
    gHeap->printEdges("b");
    
    auto gHeapCopy = *gHeap;
    gHeap->deleteNode("a");
    std::cout << "original graph after delete" << std::endl;
    gHeap->printNodes();
    gHeap->printEdges("b");
    std::cout << "copied graph after delete in other graph" << std::endl;
    gHeapCopy.printNodes();
    gHeapCopy.printEdges("b");
    
    delete gHeap;
    std::cout << "copied graph after other graph is deleted" << std::endl;
    gHeapCopy.printNodes();

    std::cout << ">>>> official test 8: PASSED " << std::endl<< std::endl;
}

// test 9: tests copy assignment
void test_9(void)
{

    std::cout << ">>>> Running official test 9: " << std::endl;

    auto gHeap = new cs6771::Graph<std::string,int>{};
    
    // add this data into the graph
    gHeap->addNode("a");
    gHeap->addNode("b");
    gHeap->addNode("c");
    gHeap->addNode("d");
    
    gHeap->addEdge("b","a",3);
    gHeap->addEdge("b","a",5);
    gHeap->addEdge("c","a",3);
    
    std::cout << "original graph" << std::endl;
    gHeap->printNodes();
    gHeap->printEdges("b");
    
    cs6771::Graph<std::string,int> gHeapCopy;
    gHeapCopy.addNode("z");
    std::cout << "Graph before copy assignment" << std::endl;
    gHeapCopy.printNodes();
    
    gHeapCopy = *gHeap; // copy assignment
    gHeap->deleteNode("a");
    std::cout << "original graph after delete" << std::endl;
    gHeap->printNodes();
    gHeap->printEdges("b");
    std::cout << "copied graph after delete in other graph" << std::endl;
    gHeapCopy.printNodes();
    gHeapCopy.printEdges("b");
    
    delete gHeap;
    std::cout << "copied graph after other graph is deleted" << std::endl;
    gHeapCopy.printNodes();

    std::cout << ">>>> official test 9: PASSED " << std::endl<< std::endl;
}

// test10: tests graph iterator
void test_10(void)
{

    std::cout << ">>>> Running official test 10: " << std::endl;

    cs6771::Graph<std::string,int> gCopy;
    
    // create some data to store as nodes.
    std::string s = "a";
    std::string t = "b";
    std::string u = "c";
    
    // add this data into the graph
    gCopy.addNode(s);
    gCopy.addNode(t);
    gCopy.addNode(u);
    
    gCopy.addEdge(u,t,1);
    gCopy.addEdge(u,t,2);   
    
    // trying iterator
    for (auto node : gCopy) {
        std::cout << node << std::endl;
    }
    std::cout << ">>>> official test 10: PASSED " << std::endl<< std::endl;
}

// test11: tests graph iterators
void test_11(void)
{

    std::cout << ">>>> Running official test 11: " << std::endl;

    cs6771::Graph<std::string,int> gCopy;
    
    // create some data to store as nodes.
    std::string s = "a";
    std::string t = "b";
    std::string u = "c";
    
    // add this data into the graph
    gCopy.addNode(s);
    gCopy.addNode(t);
    gCopy.addNode(u);
    
    gCopy.addEdge(u,t,1);
    gCopy.addEdge(u,t,2);   
    
    // trying iterator
    for (auto node = gCopy.begin(); node != gCopy.end(); ++node) {
        std::cout << *node << std::endl;
    }

    std::cout << ">>>> official test 11: PASSED " << std::endl<< std::endl;
}


// test 12: tests edgeIterators
void test_12(void)
{

    std::cout << ">>>> Running official test 12: " << std::endl;

    cs6771::Graph<std::string,int> gCopy;
    
    // create some data to store as nodes.
    std::string s = "a";
    std::string t = "b";
    std::string u = "c";
    
    // add this data into the graph
    gCopy.addNode(s);
    gCopy.addNode(t);
    gCopy.addNode(u);
    
    gCopy.addEdge(u,t,1);
    gCopy.addEdge(u,t,2);   
    
    // trying iterator over edges attached to this node. 
    for (auto edge = gCopy.edgeIteratorBegin("c"); edge != gCopy.edgeIteratorEnd(); ++edge) {
        std::cout << (*edge).first << " " << (*edge).second << std::endl;
    }
    
    std::cout << "trying -> overload on iterator" << std::endl;
    for (auto edge = gCopy.edgeIteratorBegin("c"); edge != gCopy.edgeIteratorEnd(); ++edge) {
        std::cout << edge->first << " " << edge->second << std::endl;
    }
    std::cout << ">>>> official test 12: PASSED " << std::endl<< std::endl;
}

// test 13: const correctness
void test_13(void)
{

    std::cout << ">>>> Running official test 13: " << std::endl;

    cs6771::Graph<std::string,int> gCopy;
    
    // create some data to store as nodes.
    std::string s = "a";
    std::string t = "b";
    std::string u = "c";
    
    // add this data into the graph
    gCopy.addNode(s);
    gCopy.addNode(t);
    gCopy.addNode(u);
    
    gCopy.addEdge(u,t,1);
    gCopy.addEdge(u,t,2);
    
    const auto& constGraph = gCopy;
    
    std::cout << std::boolalpha << constGraph.isNode("a") << std::endl;
    std::cout << std::boolalpha << constGraph.isConnected("a","b") << std::endl;
    std::cout << std::boolalpha << constGraph.isConnected("c","b") << std::endl;
    
    std::cout << "Const graph: " << std::endl;
    constGraph.printNodes();
    for (auto n : constGraph) {
        constGraph.printEdges(n);
    }
    
    for (auto n = constGraph.begin(); n != constGraph.end(); ++n) {
        constGraph.printEdges(*n);
    }
    
    for (auto e = constGraph.edgeIteratorBegin("c"); e != constGraph.edgeIteratorEnd(); ++e) {
        std::cout << (*e).first << " " << (*e).second << std::endl;
    }
    std::cout << ">>>> official test 13: PASSED " << std::endl<< std::endl;
}

// Test 14: tests iterators with std::algorithm
void test_14(void)
{

    std::cout << ">>>> Running official test 14: " << std::endl;

    cs6771::Graph<std::string,int> gCopy;
    
    // create some data to store as nodes.
    std::string s = "a";
    std::string t = "b";
    std::string u = "c";
    
    // add this data into the graph
    gCopy.addNode(s);
    gCopy.addNode(t);
    gCopy.addNode(u);
    
    gCopy.addEdge(u,t,1);
    gCopy.addEdge(u,t,2);
    
    auto result = std::find(gCopy.begin(), gCopy.end(), "a");
    if (result != gCopy.end()) {
        std::cout << "Node a found" << std::endl;
    }
    
    result = std::find(gCopy.begin(), gCopy.end(), "d");
    if (result == gCopy.end()) {
        std::cout << "Node d not found" << std::endl;
    }
    
    gCopy.clear();
    
    result = std::find(gCopy.begin(), gCopy.end(), "a");
    if (result != gCopy.end()) {
        std::cout << "Node a found" << std::endl;
    } else {
        std::cout << "Node a not found" << std::endl;
    }
    
    std::cout << ">>>> official test 14: PASSED " << std::endl<< std::endl;
}




// basic tests on Graph<int, int> type
void test_u15(void)
{
    std::cout << ">>>> Running test_1." << std::endl;
    std::cout << "Graph<int, int>:" << std::endl;

    cs6771::Graph<int, int> g1;
    std::cout << "Adding a selection of unique nodes of int type." << std::endl;
    assert(g1.addNode(1) == 1);
    assert(g1.addNode(2) == 1);
    assert(g1.addNode(3) == 1);
    assert(g1.addNode(4) == 1);
    std::cout << "---PASSED---" << std::endl << std::endl;

    std::cout << "Adding a selection of repeated nodes of int type." << std::endl;
    assert(g1.addNode(1) == 0);
    assert(g1.addNode(2) == 0);
    assert(g1.addNode(3) == 0);
    assert(g1.addNode(4) == 0);
    std::cout << "---PASSED---" << std::endl << std::endl;  

    std::cout << "Adding a selection of unique edges of int type." << std::endl;
    assert(g1.addEdge(1,2,1) == 1);
    assert(g1.addEdge(1,3,2) == 1);
    assert(g1.addEdge(1,4,3) == 1);
    assert(g1.addEdge(2,3,4) == 1);
    assert(g1.addEdge(2,4,5) == 1);
    assert(g1.addEdge(3,4,6) == 1);
    assert(g1.addEdge(4,1,1) == 1);
    assert(g1.addEdge(4,2,2) == 1);
    assert(g1.addEdge(4,3,3) == 1);
    std::cout << "---PASSED---" << std::endl << std::endl;

    std::cout << "Adding a selection of repeated edges of int type." << std::endl;  
    assert(g1.addEdge(1,2,1) == 0);
    assert(g1.addEdge(1,3,2) == 0);
    assert(g1.addEdge(1,4,3) == 0);
    assert(g1.addEdge(2,3,4) == 0);
    assert(g1.addEdge(2,4,5) == 0);
    assert(g1.addEdge(3,4,6) == 0);
    assert(g1.addEdge(4,1,1) == 0);
    assert(g1.addEdge(4,2,2) == 0);
    assert(g1.addEdge(4,3,3) == 0);
    std::cout << "---PASSED---" << std::endl << std::endl;


    std::cout << "Now testing that errors are being caught correctly." << std::endl;
    try
    {
        std::cout << "Add edge to two non-existant nodes" << std::endl;
        g1.addEdge(100,100,100);
    }
    catch (std::runtime_error &e)
    {
        std::cerr<< "ERROR: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cerr << "Unknown exception." << std::endl;
    }
    std::cout << "---PASSED---" << std::endl << std::endl;

    try
    {
        std::cout << "Add edge to non-existant origin, existing dest. " << std::endl;
        g1.addEdge(27, 1,100);
    }
    catch (std::runtime_error &e)
    {
        std::cerr<< "ERROR: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cerr << "Unknown exception." << std::endl;
    }
    std::cout << "---PASSED---" << std::endl << std::endl;

    try
    {
        std::cout << "Add edge to existing origin, non-existant dest. " << std::endl;
        g1.addEdge(1, 42,100);
    }
    catch (std::runtime_error &e)
    {
        std::cerr<< "ERROR: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cerr << "Unknown exception." << std::endl;
    }
    std::cout << "---PASSED---" << std::endl << std::endl;
    std::cout << "Graph stats: " << std::endl;
    //g1.printGraphStats();
    std::cout<<">>>> test_1: PASSED" << std::endl<< std::endl;
}

// basic tests on Graph<std:;string, std:;string> type
void test_u16(void)
{
    std::cout << ">>>> Running test_2: " << std::endl;
    std::cout << "Graph<std::string, std::string>:" << std::endl;

    cs6771::Graph<std::string, std::string> g2;
    std::cout << "Adding a selection of unique nodes of std::string type." << std::endl;
    assert(g2.addNode("a") == 1);
    assert(g2.addNode("b") == 1);
    assert(g2.addNode("c") == 1);
    assert(g2.addNode("d") == 1);
    std::cout << "---PASSED---" << std::endl << std::endl;

    std::cout << "Adding a selection of repeated nodes of string type." << std::endl;
    assert(g2.addNode("a") == 0);
    assert(g2.addNode("b") == 0);
    assert(g2.addNode("c") == 0);
    assert(g2.addNode("d") == 0);
    std::cout << "---PASSED---" << std::endl << std::endl;  

    std::cout << "Adding a selection of unique edges of string type." << std::endl;
    assert(g2.addEdge("a","b","this") == 1);
    assert(g2.addEdge("a","c","is") == 1);
    assert(g2.addEdge("a","d","a") == 1);
    assert(g2.addEdge("b","c","test") == 1);
    assert(g2.addEdge("b","d","so:") == 1);
    assert(g2.addEdge("c","d","test!") == 1);
    assert(g2.addEdge("d","a","this") == 1);
    assert(g2.addEdge("d","b","is") == 1);
    assert(g2.addEdge("d","c","a") == 1);
    std::cout << "---PASSED---" << std::endl << std::endl;

    std::cout << "Adding a selection of repeated edges of string type." << std::endl;   
    assert(g2.addEdge("a","b","this") == 0);
    assert(g2.addEdge("a","c","is") == 0);
    assert(g2.addEdge("a","d","a") == 0);
    assert(g2.addEdge("b","c","test") == 0);
    assert(g2.addEdge("b","d","so:") == 0);
    assert(g2.addEdge("c","d","test!") == 0);
    assert(g2.addEdge("d","a","this") == 0);
    assert(g2.addEdge("d","b","is") == 0);
    assert(g2.addEdge("d","c","a") == 0);
    std::cout << "---PASSED---" << std::endl << std::endl;


    std::cout << "Now testing that errors are being caught correctly." << std::endl;
    try
    {
        std::cout << "Add edge to two non-existant nodes" << std::endl;
        g2.addEdge("broken","test","one");
    }
    catch (std::runtime_error &e)
    {
        std::cerr<< "ERROR: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cerr << "Unknown exception." << std::endl;
    }
    std::cout << "---PASSED---" << std::endl << std::endl;

    try
    {
        std::cout << "Add edge to non-existant origin, existing dest. " << std::endl;
        g2.addEdge("broken", "a","one");
    }
    catch (std::runtime_error &e)
    {
        std::cerr<< "ERROR: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cerr << "Unknown exception." << std::endl;
    }
    std::cout << "---PASSED---" << std::endl << std::endl;

    try
    {
        std::cout << "Add edge to existing origin, non-existant dest. " << std::endl;
        g2.addEdge("d", "unknown destination","test string");
    }
    catch (std::runtime_error &e)
    {
        std::cerr<< "ERROR: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cerr << "Unknown exception." << std::endl;
    }
    std::cout << "---PASSED---" << std::endl << std::endl;
    std::cout << "Graph stats: " << std::endl;
    //g2.printGraphStats();
    std::cout<<">>>> test_2: PASSED" << std::endl<< std::endl;
}

// basic tests on Graph<std::string, int> type
void test_u17(void)
{
    std::cout << ">>>> Running test_3: " << std::endl;
    std::cout << "Graph<std::string, int>:" << std::endl;

    cs6771::Graph<std::string, int> g3;
    std::cout << "Adding a selection of unique nodes of std::string type with int weights.." << std::endl;
    assert(g3.addNode("a") == 1);
    assert(g3.addNode("b") == 1);
    assert(g3.addNode("c") == 1);
    assert(g3.addNode("d") == 1);
    std::cout << "---PASSED---" << std::endl << std::endl;

    std::cout << "Adding a selection of repeated nodes of string type." << std::endl;
    assert(g3.addNode("a") == 0);
    assert(g3.addNode("b") == 0);
    assert(g3.addNode("c") == 0);
    assert(g3.addNode("d") == 0);
    std::cout << "---PASSED---" << std::endl << std::endl;  

    std::cout << "Adding a selection of unique edges of string type." << std::endl;
    assert(g3.addEdge("a","b",1) == 1);
    assert(g3.addEdge("a","c",2) == 1);
    assert(g3.addEdge("a","d",3) == 1);
    assert(g3.addEdge("b","c",4) == 1);
    assert(g3.addEdge("b","d",5) == 1);
    assert(g3.addEdge("c","d",6) == 1);
    assert(g3.addEdge("d","a",1) == 1);
    assert(g3.addEdge("d","b",2) == 1);
    assert(g3.addEdge("d","c",3) == 1);
    std::cout << "---PASSED---" << std::endl << std::endl;

    std::cout << "Adding a selection of repeated edges of string type." << std::endl;   
    assert(g3.addEdge("a","b",1) == 0);
    assert(g3.addEdge("a","c",2) == 0);
    assert(g3.addEdge("a","d",3) == 0);
    assert(g3.addEdge("b","c",4) == 0);
    assert(g3.addEdge("b","d",5) == 0);
    assert(g3.addEdge("c","d",6) == 0);
    assert(g3.addEdge("d","a",1) == 0);
    assert(g3.addEdge("d","b",2) == 0);
    assert(g3.addEdge("d","c",3) == 0);
    std::cout << "---PASSED---" << std::endl << std::endl;


    std::cout << "Now testing that errors are being caught correctly." << std::endl;
    try
    {
        std::cout << "Add edge to two non-existant nodes" << std::endl;
        g3.addEdge("broken","test",100);
    }
    catch (std::runtime_error &e)
    {
        std::cerr<< "   >ERROR: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cerr << "Unknown exception." << std::endl;
    }
    std::cout << "---PASSED---" << std::endl << std::endl;

    try
    {
        std::cout << "Add edge to non-existant origin, existing dest. " << std::endl;
        g3.addEdge("broken", "a",42);
    }
    catch (std::runtime_error &e)
    {
        std::cerr<< "   >ERROR: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cerr << "Unknown exception." << std::endl;
    }
    std::cout << "---PASSED---" << std::endl << std::endl;

    try
    {
        std::cout << "Add edge to existing origin, non-existant dest. " << std::endl;
        g3.addEdge("d", "unknown destination",-1);
    }
    catch (std::runtime_error &e)
    {
        std::cerr<< "   >ERROR: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cerr << "Unknown exception." << std::endl;
    }
    std::cout << "---PASSED---" << std::endl;
    std::cout << "Graph stats: " << std::endl;
    //g3.printGraphStats();
    std::cout<<">>>> test_3: PASSED" << std::endl<< std::endl;

    // CREATE USE COUNT TESTS:
    
    // std::cout << "use count on 'd': " << g3.getUseCountN("d")<< std::endl;

    // g3.replace("d", "z");

    // std::cout<< "getting node with new 'z' origin. ";
    // g3.printNValue("z");
    // std::cout << std::endl;
    // std::cout << "use NEW count on 'z': " << g3.getUseCountN("z")<< std::endl;
    
}


// tests for replace, merge and delete functions
void test_u18(void)
{
    std::cout << ">>>> Running test_4: " << std::endl;

    // cs6771::Graph<int, int> g1;
    // std::cout << "Adding a selection of unique nodes of int type." << std::endl;
    // assert(g1.addNode(1) == 1);
    // assert(g1.addNode(2) == 1);
    // assert(g1.addNode(3) == 1);
    // assert(g1.addNode(4) == 1);
    
    // std::cout << "Adding a selection of edges to the nodes" << std::endl;
    // assert(g1.addEdge(1,2,1) == 1);
    // assert(g1.addEdge(1,3,2) == 1);
    // assert(g1.addEdge(1,4,3) == 1);
    // assert(g1.addEdge(2,3,4) == 1);
    // assert(g1.addEdge(2,4,5) == 1);
    // assert(g1.addEdge(3,4,6) == 1);
    // assert(g1.addEdge(4,1,1) == 1);
    // assert(g1.addEdge(4,2,2) == 1);
    // assert(g1.addEdge(4,3,3) == 1);

    //g1.mergeReplace(1,2);
    //g1.mergeReplace(1,2);


    cs6771::Graph<std::string, int> g2;
    assert(g2.addNode("A") == 1);
    assert(g2.addNode("B") == 1);
    assert(g2.addNode("C") == 1);
    assert(g2.addNode("D") == 1);
    assert(g2.addNode("E") == 1);
    assert(g2.addNode("F") == 1);
    assert(g2.addNode("ZZZ") == 1);
    
        
    assert(g2.addEdge("A","ZZZ",0) == 1);
    assert(g2.addEdge("B","ZZZ",0) == 1);
    assert(g2.addEdge("A","ZZZ",1) == 1);
    assert(g2.addEdge("B","ZZZ",1) == 1);
    assert(g2.addEdge("A","ZZZ",2) == 1);
    assert(g2.addEdge("B","ZZZ",2) == 1);
    assert(g2.addEdge("A","ZZZ",3) == 1);
    assert(g2.addEdge("B","ZZZ",3) == 1);
    

    assert(g2.addEdge("A","D",1) == 1);
    assert(g2.addEdge("A","F",1) == 1);
    assert(g2.addEdge("A","C",1) == 1);
    assert(g2.addEdge("A","E",2) == 1);

    assert(g2.addEdge("B","A",1) == 1);
    assert(g2.addEdge("B","D",1) == 1);
    assert(g2.addEdge("B","C",1) == 1);
    assert(g2.addEdge("B","E",1) == 1);
    assert(g2.addEdge("B","F",1) == 1);


    assert(g2.addEdge("A","B",11) == 1);
    assert(g2.addEdge("A","B",12) == 1);
    assert(g2.addEdge("A","B",13) == 1);
    assert(g2.addEdge("A","B",14) == 1);

    assert(g2.addEdge("A","B",3) == 1);
    assert(g2.addEdge("D","B",3) == 1);
    assert(g2.addEdge("C","B",3) == 1);
    assert(g2.addEdge("E","B",3) == 1);
    assert(g2.addEdge("F","B",3) == 1);


    //assert(g2.addEdge("A","A",4) == 1);
    assert(g2.addEdge("D","A",4) == 1);
    assert(g2.addEdge("C","A",4) == 1);
    assert(g2.addEdge("E","A",4) == 1);
    assert(g2.addEdge("F","A",4) == 1);



    //g2.mergeReplace("B","A");
    //g2.mergeReplace("A","A");

    /*
    g2.printNConnections("B");

    g2.printNConnections("A");
    g2.mergeReplace("B", "A");
    
    g2.printNConnections("A");
    g2.printNConnections("C");
    g2.printNConnections("D");
    g2.printNConnections("E");
    g2.printNConnections("F");
    g2.printNConnections("ZZZ");
    */

    // should throw an exception
    //

    try {
        g2.printNConnections("B");  
    } catch (std::runtime_error &e) {
        std::cerr << "  >ERROR: " << e.what() << std::endl;
    } catch(...){
        std::cerr << "Unknown exception." << std::endl;
    }

    g2.deleteNode("ZZZ");

    //g2.printNConnections("A");  
    g2.deleteEdge("E", "A", 4);
    g2.deleteEdge("E", "A", 3);
    
    //g2.printNConnections("E");
    
    g2.clear();
    
    std::cout << ">>>> test_4: PASSED " << std::endl<< std::endl;
}


// tests for delete, clear etc. 
void test_u19(void){
    std::cout << ">>>> Running test_5: " << std::endl;

    cs6771::Graph<int, int> g1;
    //std::cout << "Adding a selection of unique nodes of int type." << std::endl;
    assert(g1.addNode(1) == 1);
    assert(g1.addNode(2) == 1);
    assert(g1.addNode(3) == 1);
    assert(g1.addNode(4) == 1);
    g1.addNode(5);

    assert(g1.addEdge(1,2,1) == 1);
    assert(g1.addEdge(1,3,2) == 1);
    assert(g1.addEdge(1,4,3) == 1);
    assert(g1.addEdge(2,3,4) == 1);
    assert(g1.addEdge(2,4,5) == 1);
    assert(g1.addEdge(3,4,6) == 1);
    assert(g1.addEdge(4,1,1) == 1);
    assert(g1.addEdge(4,2,2) == 1);
    assert(g1.addEdge(4,3,3) == 1);
    assert(g1.addEdge(4,4,5) == 1);
    assert(g1.addEdge(4,5,5) == 1);
    g1.addEdge(1,5,2);
    
    g1.addEdge(5,4,6);
    g1.addEdge(5,4,7);
    g1.addEdge(5,4,8);
    g1.addEdge(5,4,9);
    g1.addEdge(5,4,10);


    //g1.printNodes();
    g1.edgeIteratorBegin(1);

    //auto edge = g1.edgeIteratorBegin(1);
    //std::cout << typeid(edge).name() << std::endl;
    
    /*
    for(auto node = g1.begin(); node != g1.end(); ++node){
        std::cout<< *node << std::endl; 
    }
    
    // edge->first
    for(auto edge = g1.edgeIteratorBegin(1); edge != g1.edgeIteratorEnd(); ++edge){
        std::cout<< edge->first << std::endl; 
    }
    
    */
    g1.printEdges(5);

    std::cout << ">>>> test_5: PASSED " << std::endl<< std::endl;
}


