template <typename T>
	// iterator class - this will be an inner class of the Graph
	class GraphIterator
		{
			 public:
		        // define the iterator traits
		        typedef std::input_iterator_tag          iterator_category;
		        typedef std::ptrdiff_t                     difference_type;
		        typedef T                                  value_type;
		        typedef T*                                 pointer;
		        typedef T&                                 reference;

				reference operator*() const;        // TODO Q14
		        pointer operator->() const;         // TODO Q18

		        //preincrement
		        GraphIterator& operator++(); 
				
				// post increment (does not retern a reference)
				GraphIterator operator++(int);

		        // TODO Q15
		        bool operator==(const GraphIterator& other) const;
		        bool operator!=(const GraphIterator& other) const;

		        // TODO Q10
		        GraphIterator(std::shared_ptr<std::list<std::shared_ptr<T> > > v = nullptr);
		    private:
		        std::shared_ptr<std::vector<std::shared_ptr<T> > > sortedPointers;       
		}

// constructor
template<typename T>
	GraphIterator<T>::GraphIterator(std::shared_ptr<std::list<std::shared_ptr<T> > > v) :
    	sortedPointers{v} {}

// destructor?
template<typename T>
    GraphIterator<T>::~GraphIterator()
    {
    }




template<typename T>
typename GraphIterator<T>::reference GraphIterator<T>::operator*() const
{
    return *(sortedPointers->front());
}

// TODO Q18
template<typename T>
typename GraphIterator<T>::pointer GraphIterator<T>::operator->() const
{
    return &(operator*());
}

// TODO Q15
template<typename T>
bool GraphIterator<T>::operator==(const GraphIterator& other) const
{
    //    return sortedPointers == other.sortedPointers;

    if (sortedPointers == other.sortedPointers) return true;
    if (sortedPointers == nullptr || other.sortedPointers == nullptr) return false;

    return (*sortedPointers == *other.sortedPointers);
}

// TODO Q15
template<typename T>
bool GraphIterator<T>::operator!=(const GraphIterator& other) const
{
    return !operator==(other);
}


// TODO Q10


    // TODO Q13
    template <typename T>
GraphIterator<T>& GraphIterator<T>::operator++()
{
    sortedPointers->pop_front();
    if (sortedPointers->size() == 0) sortedPointers = nullptr;
    return *this;
}



