// peter kydd
// a program that reads instructions and values in from a file and executes
// uses the stack, vector STL

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <stack>
#include <queue>
#include <vector>
#include <cmath>

// typedef to improve readbility
typedef std::stack <std::string> stringStack;

namespace calculator{
    void parseToken(stringStack& newStack, std::string token, std::ifstream& inputFile );
    void addHandler(stringStack& newStack);
    void subHandler(stringStack& newStack);
    void multHandler(stringStack& newStack);
    void divisionHandler(stringStack& newStack);
    void sqrtHandler(stringStack& newStack);
    void popHandler(stringStack& newStack);
    void reverseHandler(stringStack& newStack);
    void repeatHandler(stringStack& newStack, std::ifstream& inputFile );
    void printOutput(std::string temp1, std::string temp2, std::string result, char operatorToken);
    void printOutput(std::string temp1, std::string result);

    inline bool isDouble(std::string token){ return token.find('.') != std::string::npos; }
    inline std::string add(int a, int b){ return std::to_string(static_cast<int>(a+b));}
    inline std::string add(double a, double b){ return std::to_string(double(a+b));}
    inline std::string sub(int a, int b){ return std::to_string(static_cast<int>(a-b));}
    inline std::string sub(double a, double b){ return std::to_string(static_cast<double>(a-b));}
    inline std::string mult(int a, int b){ return std::to_string(static_cast<int>(a*b));}
    inline std::string mult(double a, double b){ return std::to_string(static_cast<double>(a*b));}
    inline std::string division(int a, int b){ return std::to_string(static_cast<int>(a/b));}
    inline std::string division(double a, double b){ return std::to_string(static_cast<double>(a/b));}
}

int main(int argc, char* argv[]){

    // setup the print out format for the precision required.
    std::cout.setf(std::ios::fixed,std::ios::floatfield);
    std::cout.precision(3);
    std::string firstArg;

    if (argc > 1){
        firstArg = argv[1];
        stringStack newStack;
        std::string token;

        std::ifstream inputFile(firstArg);
        if(inputFile.is_open()){
            
            while (inputFile >> token ){
                calculator::parseToken(newStack, token,  inputFile);
            }
            
            inputFile.close();
        
        } else {
            std::cout << "Unable to open file." << std::endl;
        }
    
    } else {
        std::cout << "No file entered. Please enter a file name as an argument." << std::endl;
    }

    return EXIT_SUCCESS;
}

namespace calculator{
    void parseToken(stringStack& newStack, std::string token, std::ifstream& inputFile ){
        // check token for keyword
        
        if(isdigit(token.front())){
            newStack.push(token);                

        }else if(token.find("add") != std::string::npos ){
            addHandler(newStack);    

        }else if(token.find("sub") != std::string::npos ){
            subHandler(newStack);    
            
        }else if(token.find("mult") != std::string::npos ){
            multHandler(newStack);

        }else if(token.find("div") != std::string::npos ){
            divisionHandler(newStack);    

        }else if(token.find("sqrt") != std::string::npos ){
            sqrtHandler(newStack);    
            
        }else if(token.find("pop") != std::string::npos ){
            popHandler(newStack);    
            
        }else if(token.find("reverse") != std::string::npos ){
            reverseHandler(newStack);    
            
        }else if(token.find("repeat") != std::string::npos ){
            repeatHandler(newStack, inputFile);
            
        }else{
            std::cout << ">>> unknown instruction detected." << std::endl;
        }
    }

    void addHandler(stringStack& newStack){
        std::cout.precision(3);
        
        // make sure there are sufficient variables on the stack
        if((newStack.size() >= 2) ){

            // pull two strings from the stack
            std::string temp1 = newStack.top();
            newStack.pop();
            std::string temp2 = newStack.top();
            newStack.pop();
            std::string result;

            // check if either variable is a double
            if( isDouble(temp1) || isDouble(temp2) ){
                result = add(std::stod(temp1), std::stod(temp2));
            } else {
                result = add(std::stoi(temp1), std::stoi(temp2));
            }
        
            printOutput(temp1, temp2, result, '+');
            newStack.push(result);
        } else{
            std::cout << "Invalid instruction - too few variables on stack to complete 'add'." << std::endl;
        }
    }

    void subHandler(stringStack& newStack){

        if((newStack.size() >= 2) ){


            // pull two strings from the stack
            std::string temp1 = newStack.top();
            newStack.pop();
            std::string temp2 = newStack.top();
            newStack.pop();
            std::string result;

            // check if either variable is a double
            if( isDouble(temp1) || isDouble(temp2) ){
                result = sub(std::stod(temp1), std::stod(temp2));
            } else {
                result = sub(std::stoi(temp1), std::stoi(temp2));
            }

            printOutput(temp1, temp2, result, '-');
            newStack.push(result);

        } else{
            std::cout << "Invalid instruction - too few variables on stack to complete 'sub'." << std::endl;
        }
    }

    void multHandler(stringStack& newStack){


        if((newStack.size() >= 2) ){

            // pull two strings from the stack
            std::string temp1 = newStack.top();
            newStack.pop();
            std::string temp2 = newStack.top();
            newStack.pop();
            std::string result;

            // check if either variable is a double
            if( isDouble(temp1) || isDouble(temp2) ){
                result = mult(std::stod(temp1), std::stod(temp2));
            } else {
                result = mult(std::stoi(temp1), std::stoi(temp2));
            }

            printOutput(temp1, temp2, result, '*');
            newStack.push(result);

        } else{
            std::cout << "Invalid instruction - too few variables on stack to complete 'mult'." << std::endl;
        }
    }

    void divisionHandler(stringStack& newStack){

        if((newStack.size() >= 2) ){

            // pull two strings from the stack
            std::string temp1 = newStack.top();
            newStack.pop();
            std::string temp2 = newStack.top();
            newStack.pop();
            std::string result;

            // check if either variable is a double
            if( isDouble(temp1) || isDouble(temp2) ){
                if( (std::stod(temp1) == 0) || (std::stod(temp2) == 0) ){
                    std::cout << "Division by 0 not supported." << std::endl;
                    return;
                }
                result = division(std::stod(temp1), std::stod(temp2));
            } else {
                if( (std::stoi(temp1) == 0) || (std::stoi(temp2) == 0) ){
                    std::cout << "Division by 0 not supported." << std::endl;
                    return;
                }
                result = division(std::stoi(temp1), std::stoi(temp2));
            }

            printOutput(temp1, temp2, result, '/');
            newStack.push(result);


        } else{
            std::cout << "Invalid instruction - too few variables on stack to complete 'mult'." << std::endl;
        }
    }



    void sqrtHandler(stringStack& newStack){

        if(newStack.size() >= 1){
            std::string temp1 = newStack.top();
            newStack.pop();

            std::string result;
            // int
            if(!isDouble(temp1)){
                int tempResult = sqrt(std::stoi(temp1));
                result = std::to_string(tempResult);
            // double
            } else{
                result = std::to_string(sqrt(std::stod(temp1)));
            }

            printOutput(temp1, result);
            newStack.push(result);
           
        } else {
            std::cout << "Invalid instruction - too few variables on stack to complete 'sqrt'." << std::endl;
        }
    }

    void popHandler(stringStack& newStack){
        
        if(newStack.size()>=1){
            newStack.pop();
        } else {
            std::cout << "Invalid instruction - too few variables on stack to complete 'pop'." << std::endl;
        }
    }

    void reverseHandler(stringStack& newStack){
        int stackSize = newStack.size();

        if(stackSize >= 1){

            // create a queue to store strings to be reversed
            std::queue<std::string> q;
            std::string revNumStr = newStack.top();
            newStack.pop();

            int reverseCount = 0;

            if(!(revNumStr.find('.') != std::string::npos)){
                reverseCount = std::stoi(revNumStr);
            // double
            } else if ( revNumStr.find('.') != std::string::npos ){
                reverseCount = std::stod(revNumStr);
            }

            if( reverseCount <= stackSize){
                // populate queue with appropriate number of strings
                for(int i = 0; i < reverseCount; i++){
                    q.push(newStack.top());
                    newStack.pop();
                }

                // pop strings off queue
                while (!q.empty()){
                    newStack.push(q.front());
                    q.pop();
                }

            }else{
                std::cout << "Invalid reverse value - not enough data on stack to reverse " << reverseCount << " positions." << std::endl;
            }

        } else {
            std::cout << "Invalid instruction - too few variables on stack to complete 'pop'." << std::endl;
        }
    }

    void repeatHandler(stringStack& newStack, std::ifstream& inputFile){

        int repeatNumber = 0;

        if(newStack.size() >= 1){
            std::string temp1 = newStack.top();
            newStack.pop();

            if(!isDouble(temp1)){
                repeatNumber = std::stoi(temp1);
            } else if ( temp1.find('.') != std::string::npos ){
                repeatNumber = std::stod(temp1);
            }else{
                std::cout << "Invalid instruction for repeat :" << temp1 << std::endl; 
            }

        } else {
            std::cout << "Invalid instruction - too few variables on stack to complete 'repeat'." << std::endl;
        }

        std::vector<std::string> repeatVector;
        std::string token;

        while (inputFile >> token ){
            if (token.compare("endrepeat") == 0){
                break;
            }
            repeatVector.push_back(token);
        }
       
        int vectorSize = repeatVector.size();

        //iterate through vector n times
        for(int i = 0; i< repeatNumber; i++){
            for(int j = 0; j<vectorSize; j++){
                parseToken(newStack, repeatVector[j], inputFile );
            }
        }
    }

    void printOutput(std::string temp1, std::string temp2, std::string result, char operatorToken){
        
        // if temp1 is an int
        if( ! isDouble(temp1)){
            std::cout << std::stoi(temp1);
        }else{
            std::cout << std::stod(temp1);
        }
        std::cout << " " << operatorToken << " "; 

        // if temp2 is an int
        if( ! isDouble(temp2)){
            std::cout << std::stoi(temp2);
        }else{
            std::cout << std::stod(temp2);
        }
        std::cout << " = ";

        // if result is an int
        if( ! isDouble(result)){
            std::cout << std::stoi(result);
        }else{
            std::cout << std::stod(result);
        }

        std::cout << std::endl;    
    }

    void printOutput(std::string temp1, std::string result){
        
        std::cout << "sqrt ";
        // if temp1 is an int
        if( ! isDouble(temp1)){
            std::cout << std::stoi(temp1);
        }else{
            std::cout << std::stod(temp1);
        }
        std::cout << " = ";

        // if result is an int
        if( ! isDouble(result)){
            std::cout << std::stoi(result);
        }else{
            std::cout << std::stod(result);
        }

        std::cout << std::endl;    
    }
}