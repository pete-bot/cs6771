// a program that demonstrates the properties of an output stream 

#include <iostream>
#include <cstdlib>

int main(int argc, char* argv[]){

	std::string s = "4.00";
	std::string t = "3.14";

	std::cout << std::stod(s) << std::endl << std::stod(t) << std::endl;

	return EXIT_SUCCESS;
}
