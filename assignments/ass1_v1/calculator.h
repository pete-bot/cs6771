// h file for ass1: calculator.cpp

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <stack>
#include <queue>
#include <vector>
#include <cmath>

typedef std::stack <std::string> stringStack;

// function to determine if the given string contains a double
inline bool isDouble(std::string token){ return token.find('.') != std::string::npos; }

void parseToken(stringStack& newStack, std::string token, std::ifstream& inputFile );

// handler for the add function - deals with stack interfacing etc.
void addHandler(stringStack& newStack);

// overloeaded add functions
inline std::string add(int a, int b){ return std::to_string(int(a+b));}
inline std::string add(double a, double b){ return std::to_string(double(a+b));}


// handler for the add function - deals with stack interfacing etc.
void subHandler(stringStack& newStack);

// overloaded sub functions
inline std::string sub(int a, int b){ return std::to_string(int(a-b));}
inline std::string sub(double a, double b){ return std::to_string(double(a-b));}


void multHandler(stringStack& newStack);

// overloaded mult functions
inline std::string mult(int a, int b){ return std::to_string(int(a*b));}
inline std::string mult(double a, double b){ return std::to_string(double(a*b));}


void divisionHandler(stringStack& newStack);

// overloaded division functions
inline std::string division(int a, int b){ return std::to_string(int(a/b));}
inline std::string division(double a, double b){ return std::to_string(double(a/b));}


// handle sqrt instruction
void sqrtHandler(stringStack& newStack);

// handle reverse instructions
void popHandler(stringStack& newStack);

// handle reverse instructions
void reverseHandler(stringStack& newStack);

// handle repeat instruction
void repeatHandler(stringStack& newStack, std::ifstream& inputFile );

// overloaded print functions
void printOutput(std::string temp1, std::string temp2, std::string result, char operatorToken);
void printOutput(std::string temp1, std::string result);


