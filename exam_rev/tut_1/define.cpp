#include <iostream>
#include <typeinfo>
int i;

int f() { return i++;  }
int g() { return i == 1 ? i + 3 : i - 1;  }
int h() { return i == 2 ? i - 3 : i + 2;  }

int main() {
    //std::cout << (f() + g()) * h() << std::endl;

    //const int &i = 1;
    //std::cout << typeid('a').name() << std::endl;
    /*
     *
     short a[] = {1, 2, 3, '\0'};
     short *p = a;
     while (*p)
     std::cout << (*p)++ <<std::endl; // printf("%d\n", *p++) in C
     */

    int nums[] = {1, 2, 3, 4};
    const int *first = nums;
    const int *last = nums + 4;

    for (const int *ip = first ; ip != last; ++ip)
        std::cout << ip << ": " << *ip << std::endl;

    return 0;
}
