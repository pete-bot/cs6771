#include <iostream>
#include <fstream>

using namespace std;

int main() {
    int i = 400;
    double d = 6.14159;
    ofstream ofs("myfile.txt");
    ofs << i << " " << d;
    ofs.close();
    ifstream ifs("myfile.txt");
    ifs >> i >> d;
    ifs.close();
    cout << i << " " << d << endl;
}

