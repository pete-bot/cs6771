#include <cstdlib>
#include <iostream>

#include "threeInts.hpp"

int main( int argc, char* argv[] )
{

    int a, b, c;
    std::cout << "Please enter three ints: " << std::endl;
    std::cin >> a >> b >> c;

    std::cout << "You entered: " << a << " " << b << " " << c << std::endl;
    order3(a, b, c);
    std::cout << "The ordered values are: " << a << " " << b << " " << c << std::endl;

    return EXIT_SUCCESS;
}

void order3(int &a, int &b, int &c) {
    if (a < c) {
        int t = a;
        a = c;
        c = t;

    }
    if (a < b) {
        int t = a;
        a = b;
        b = t;

    }
    if (b < c) {
        int t = b;
        b = c;
        c = t;

    }

}
