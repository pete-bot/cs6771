#include <cstdlib>
#include <vector>   // vectors
#include <algorithm> // std::sort()
#include <string>
#include <iostream>
#include <array>
#include <list>
#include <map>
#include <deque>
#include <fstream>
#include <iterator>


int main ( int argc, char* argv[] )
{
    std::vector<std::string> strings({"one", "two", "three"});
    std::array <int, 3> numbers{ {1,2,3} };

    std::list<int> intList;
    std::back_insert_iterator<std::list<int>> listIt(intList);


  //  std::cout << "Printing vector. " << std::endl;
  //  for(auto elt: strings) std::cout << elt << std::endl;


  //  std::cout << "Copying into list..." << std::endl;
    std::copy(numbers.begin(), numbers.end(), listIt);

    intList.sort();

    std::map<std::string, int> intMap;

    for(unsigned int index = 0; index < strings.size(); ++index){
        intMap.insert(std::pair<std::string, int>(strings.at(index),index ));
    }

//    for (auto elt : intMap ) std::cout << elt.first << ": " << elt.second << std::endl;


    std::vector<int> v1(10, 1);
    std::vector<int> v2(5, 2);
    //auto first = v1.cbegin();
    
    // swap elements in both containers. iterators are still valid, but in the other container now
    v2.swap(v1);
    
    /*
    auto last = v2.cend();
    while (first != last) {
        std::cout << *first << std::endl;
        ++first;
    }
    */

    //for (auto elt : v2) std::cout << elt << std::endl; 

    /*
    std::string has all of the benefits of an STL container. iterators, stl functions (begin, end, size etc)
    */

    std::pair<std::string, int> p1("Months", 12);

    std::pair<std::string, int> p2;
    p2.first = "Months";
    p2.second = 12;

    std::pair<std::string, int> p3{"Months", 12};

    auto p4 = std::make_pair("Months", 12);

    std::cout << p2.first << ": "<< p2.second << std::endl;

    std::cout << p4.first << ": " << p4.second << std::endl;

    std::map<std::string, double> books;

    std::pair<std::string, double> mary_pop("Mary Poppins", 12.95);
    std::pair<std::string, double> jur_park("Jurassic Park", 19.95);
    std::pair<std::string, double> nineteen_eightfour("1984", 9.99);

    books.insert(mary_pop);
    books.insert(jur_park);
    books.insert(nineteen_eightfour);

    for(auto elt : books){
        std::cout << elt.first << ": " << elt.second << std::endl; 
    }


    std::vector<int> ivec(1, 1);
    std::map< std::vector<int>, std::vector<int>> ivmap;
    ivmap[ivec].push_back(2);
    (*ivmap[ivec].begin())++;
    std::cout << ivmap[ivec][0] << std::endl;

    std::vector<int> ivec_1;
    for (int i = 0; i < 20; ++i) ivec_1.push_back(i);
    std::deque<int> ideq;


    //for(auto elt : ivec_1) ideq.push_front(elt);
    for (int i = 19; i >= 0; --i) ideq.push_back(ivec_1.at(i));
    for(auto elt : ideq ) std::cout << elt << std::endl;
    
    std::ofstream ofs("nums.txt");
    for(auto elt : ideq ) ofs << elt << std::endl;
    ofs.close();

    std::cout << "********" << std::endl;


    // iterate over ivec_1 and remove any elts larger than 14
    auto remove_it = std::remove_if(ivec_1.begin(), ivec_1.end(),
        [](int i)
        {
            return i>14;
        });

    ivec_1.erase( remove_it, ivec_1.end());

    for(auto elt : ivec_1) std::cout << elt << std::endl;

    // permutations of "permute" 
    std::string s("permute");
    while(std::next_permutation(s.begin(), s.end())){
        std::cout << s << std::endl;
    }


    // shuffle et al

    std::vector<int> ivec1;
    for (int i = 1; i <= 20; ++i)
    ivec1.push_back(i);

   std::random_shuffle(ivec1.begin(), ivec1.end());
   std::vector<int> ivec2(ivec1);
   std::copy(ivec1.begin(), ivec1.end(), std::ostream_iterator<int>(std::cout, " "));
   std::cout << std::endl;
  
   // sort the first 10 elts of the set
   std::partial_sort(ivec1.begin(), ivec1.begin()+10, ivec1.end());
   std::copy(ivec1.begin(), ivec1.end(), std::ostream_iterator<int>(std::cout, " "));
   std::cout << std::endl;

   // anything left of n < nth elt,  anything right of elt, > nth elt 
   std::nth_element(ivec2.begin(), ivec2.begin()+10, ivec2.end());
   std::copy(ivec2.begin(), ivec2.end(), std::ostream_iterator<int>(std::cout, " "));
   std::cout << std::endl;




    return EXIT_SUCCESS;
}
