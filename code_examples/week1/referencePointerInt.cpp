#include <iostream>

int main() {
	int i = 1;
	int j = 10;

	int &iRef = i;
	
	// prints 1
	std::cout << "i = " << i << std::endl;
	std::cout << "iRef = " << iRef << std::endl;
	std::cout << "j = " << j << std::endl;
	
	std::cout << "iRef is a reference to i, iRef = j, which copies j into i" << std::endl;
	iRef = j;	// set the iRef to 10, i.e. i is now == 1
	
	j++; // increase j
	
	// print i, it will now be 10.
	std::cout << "i = " << i << std::endl;
	std::cout << "iRef = " << iRef << std::endl;
	std::cout << "j = " << j << std::endl;
	
	std::cout << "reset i to 1, j to 10" << std::endl;
	i = 1; // reset i to be one. 
	j = 10;
	
	int *iPtr = &i;		// create a pointer to i. 
	
	// prints i, it will be 1
	std::cout << "i = " << i << std::endl;
	std::cout << "*iPtr = " << *iPtr << std::endl;
	std::cout << "j = " << j << std::endl;
	
	iPtr = &j;			// change the pointer (not the dereference)
	j++;
	
	std::cout << "Create a pointer to i, then change the pointer to j, iPTr = &j" << std::endl;
	
	// prints i, it will still be 1
	std::cout << "i = " << i << std::endl;
	std::cout << "*iPtr = " << *iPtr << std::endl;
	// and j will be 11. 
	std::cout << "j = " << j << std::endl;
}
