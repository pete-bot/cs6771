// mixedSignedInts.cpp

#include <iostream>

int main() {
	signed int a = -1;
	unsigned int b = 1;
	std::cout << a * b << std::endl; // 4294967295 on williams

	// -1 is promoted to unsigned int
	// binary representation stays the same:
	// 111111111111111111111111111 (32 bits),
	// but unsigned value is: 4294967295
	// therefore
	// 4294967295 * 1 = 4294967295
}

