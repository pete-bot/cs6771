// tutorial questions
// create an instance of vectors of strings etc. 

#include <iostream>		// cout, cin etc.
#include <cstdlib>		// EXIT_SUCCESS
#include <vector>		// std::vector
#include <array>		// std::array
#include <iterator>		// iterators for array?
#include <algorithm>	// std::sort
#include <list>
#include <map>

typedef std::vector<std::string> string_vector;

int main(int argc, char* argv[]){

	string_vector strVector;
	strVector.push_back(std::string("three"));
	strVector.push_back(std::string("one"));
	strVector.push_back(std::string("two"));

	std::cout << "Printing strVector..." << std::endl;
	for (string_vector::const_iterator i = strVector.begin(); i != strVector.end(); ++i){
    	std::cout << *i << std::endl;
	}
	std::cout << std::endl;

	
	// or alternately, the following method is more compact
	string_vector strVector2 = {"two", "one", "three"};
	 
	
	std::cout << "Printing strVector2..." << std::endl;
	for (string_vector::const_iterator i = strVector2.begin(); i != strVector2.end(); ++i){
    	std::cout << *i << std::endl;
	}
	std::cout << std::endl;
	

	std::array<int, 10> intArray = {3,1,5, 43, 11, 52 ,12 ,75, 98, 21};

	std::cout << "Printing intArray..." << std::endl;
	for (auto i : intArray){
    	std::cout << i << std::endl;
	}
	std::cout << std::endl;

	// try sorting our arrays
	std::sort(strVector.begin(), strVector.end());
	std::sort(intArray.begin(), intArray.end());
	

	std::cout << "Printing std::sort()'ed strVector..." << std::endl;
	for (string_vector::const_iterator i = strVector.begin(); i != strVector.end(); ++i){
    	std::cout << *i << std::endl;
	}
	std::cout << std::endl;
	


	std::cout << "Printing std::sort()'ed intArray..." << std::endl;
	for (auto i : intArray){
    	std::cout << i << std::endl;
	}
	std::cout << std::endl;


	// the sorts will both work since there is function overloading allowing them to have 
	// multiple effects depending on their arguments

	// in the string case, it appears to sort based on the appearance of characters starting
	// at the left side as most significant

	std::array<int, 10> intArray2 = {3,1,5, 43, 11, 52 ,12 ,75, 98, 21};
	

	std::list <int> intList;

	// populate intList
	for (auto i : intArray2){
    	intList.push_back(i);
	}

	std::cout << "Printing intList..." << std::endl;
	for (auto i : intList){
		std::cout << i << std::endl;
	}


	intList.sort();

	std::cout << "Printing list.sort()'ed intList..." << std::endl;
	for (auto i : intList){
		std::cout << i << std::endl;
	}


	// CORRECT WAY - USE cbegin
	// create a std::map - key = string, value = index 
	std::map<std::string, int> intMap;

	std::vector<std:;string>::const_iterator k;

	// counter for the index value in map
	int j = 0;
	for(auto k = strVector.cbegin(); k != strVector.cend(); ++k){
		intMap.insert(std::pair<std::string, int>(*k, j++));
	}

/*h
	for (auto i : strVector){
		
	}// push back operator:L
	std::back_insert_iterator <std::list<int>> back_it(intList);

	// instead we could do:

	for (auto it = intArray.begin(); it!= intArray.end(); ++i){
		
	}

*/
	// BETTER TO USE A CONST VARIABLE FOR ITERATOR -
	// USE CBEGIN OVER A NON CONST - THISIS CONST CORRECTNESS AND WILL HELP WHEN YOU 
	// GO TO 





	return EXIT_SUCCESS;

}