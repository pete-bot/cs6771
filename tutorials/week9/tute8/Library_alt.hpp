#include <memory>
#include <vector>
#include <algorithm>
#include <iostream>
#include <list>

//-----------------------------------------------------------------------------------
// Version where the iterator is a wrapper over an underlying
// std::vector<> iterator.
// -----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
// library to store objects of type T and descriptions of type U
//-----------------------------------------------------------------------------------

template <typename T, typename U>
class Library
{
public:
	// mutators to change the contents of the library
	void add(const T& item);
	unsigned int remove(const T& item);
	void addRelated(const T& from, const T& to, const U& desc);
	void printRelated(const T& from);
	bool inLibrary(const T& item);

    // forward declaration.
    class Iterator;
	Iterator begin();
	Iterator end();

    /* // Should define const iterators
	ConstIterator begin() const;
	ConstIterator end() const;
	ConstIterator cbegin() const;
	ConstIterator cend() const;
    */
private:

    //-------------------------------------------------------------------------------
	// private inner class to hold objects of type T in the library.
    //-------------------------------------------------------------------------------
	class ItemContainer
    {
	public:

		// constructor for creating a new ItemContainer to store an item
		ItemContainer(const T& item) : itemPtr{std::make_shared<T>(item)} {}

		T& getItem() { return *itemPtr; }
		const T& getItem() const { return *itemPtr; }
		void addRelated(const ItemContainer& to, const U& desc);
		std::shared_ptr<T> getItemPtr() const { return itemPtr; }
		void printRelated();

	private:

        //-------------------------------------------------------------------------------
		// private inner class to hold information about related works.
        //-------------------------------------------------------------------------------
		class RelatedWork
        {
		public:
			RelatedWork(const ItemContainer& link, const U& desc);
			void printItemAndDescription();
		private:
			// private data members of RelatedWork
			U relatedWorkDescription;
			std::shared_ptr<T> relatedWorkLink;
		};


		// private data members of ItemContainer
		std::shared_ptr<T> itemPtr;
		std::vector<RelatedWork> relatedWorks;
	};

	// private data member of Library Class
	std::vector<ItemContainer> items;


    //-----------------------------------------------------------------------------------
    // LibraryItemIterator class declaration
    //-----------------------------------------------------------------------------------

public:
    // We haven't done inheritance yet
//    class Iterator : public std::iterator<std::forward_iterator_tag, T, std::ptrdiff_t, T*, T&>
    class Iterator
    {
    public:

        typedef std::ptrdiff_t                     difference_type;

        // TODO - Q8. What iterator category tag?
        typedef std::forward_iterator_tag          iterator_category;

        typedef T                                  value_type;
        typedef T*                                 pointer;
        typedef T&                                 reference;

        // Note: when inheriting from std::iterator need to use
        // 'typename Iterator::' to access reference defined in base
        // class:
        // http://stackoverflow.com/questions/1643035/propagating-typedef-from-based-to-derived-class-for-template
        // http://stackoverflow.com/questions/25549652/c-why-is-there-injected-class-name

        typename Iterator::reference operator*() const;        // TODO Q14
        typename Iterator::pointer operator->() const;         // TODO Q18

        Iterator& operator++();  // TODO Q13
        // Should define postfix increment operator

        // TODO Q15
        bool operator==(const Iterator& other) const;
        bool operator!=(const Iterator& other) const;

        // TODO Q10

//        typedef std::is_const<std::remove_reference<T
        Iterator(typename std::vector<ItemContainer>::iterator it);
   private:
        typename std::vector<ItemContainer>::iterator it_;
   };
public:


};

//-----------------------------------------------------------------------------------
// Library member functions
//-----------------------------------------------------------------------------------

// method to add a new item to the library.
template <typename T, typename U>
void Library<T,U>::add(const T& item)
{
	ItemContainer newItem{item};
	items.push_back(newItem);
}

// method to add a related works to an existing item
template <typename T, typename U>
void Library<T,U>::addRelated(const T& from, const T& to, const U& desc)
{
    // find from itemContainer.
    auto fromIC = std::find_if(items.begin(), items.end(),
                               [&from] (const ItemContainer& ic)
                               {
                                   return from == ic.getItem();
                               });
    // find to
    auto toIC = std::find_if(items.begin(), items.end(),
                             [&to] (const ItemContainer& ic)
                             {
                                 return to == ic.getItem();
                             });
    // add desc
    fromIC->addRelated(*toIC,desc);
}

// method to remove an item from the library.
template <typename T, typename U>
unsigned int Library<T,U>::remove(const T& item)
{
	// TODO: remove the item from the library
	return items.size();
}

// method to print the related works for a given item.
template <typename T, typename U>
void Library<T,U>::printRelated(const T& from)
{
	// find from itemContainer.
	auto fromIC = std::find_if(items.begin(), items.end(),
                               [&from] (const ItemContainer& ic)
                               {
                                   return from == ic.getItem();
                               });
	fromIC->printRelated();
}

// method to check if an item is already in the library
template <typename T, typename U>
bool Library<T,U>::inLibrary(const T& item)
{
	auto ic = std::find_if(items.begin(), items.end(),
                           [&item] (const ItemContainer& ic)
                           {
                               return item == ic.getItem();
                           });
	if (ic == items.end()) return false;
	return true;
}


//-----------------------------------------------------------------------------------
// TODO - Q11 - Create begin() and end() member functions

// iterator begin
template <typename T, typename U>
typename Library<T,U>::Iterator Library<T,U>::begin()
{
    return Iterator{items.begin()};
}


template <typename T, typename U>
typename Library<T,U>::Iterator Library<T,U>::end()
{
    return Iterator{items.end()};
}

//-----------------------------------------------------------------------------------
// Library::ItemContainer member functions
//-----------------------------------------------------------------------------------

// method to add a related works object to the item container.
template <typename T, typename U>
void Library<T,U>::ItemContainer::addRelated(const ItemContainer& to, const U& desc)
{
	RelatedWork rw{to,desc};
	relatedWorks.push_back(rw);
}

// method to print each related item in a container.
template <typename T, typename U>
void Library<T,U>::ItemContainer::printRelated()
{
	for (auto related : relatedWorks)
    {
		related.printItemAndDescription();
	}
}

//-----------------------------------------------------------------------------------
// Library::ItemContainer::RelatedWork member functions
//-----------------------------------------------------------------------------------

// constructor for a related works object
template <typename T, typename U>
Library<T,U>::ItemContainer::RelatedWork::RelatedWork(const ItemContainer& link, const U& desc) :
    relatedWorkDescription{desc}
{
	relatedWorkLink = link.getItemPtr();
}

// method to print the item and description of a related work.
template <typename T, typename U>
void Library<T,U>::ItemContainer::RelatedWork::printItemAndDescription()
{
	std::cout << *(relatedWorkLink) << " - " << relatedWorkDescription << std::endl;
}


//-----------------------------------------------------------------------------------
// LibraryItemIterator definitions
//-----------------------------------------------------------------------------------

// TODO Q10
template <typename T, typename U>
Library<T,U>::Iterator::Iterator(typename std::vector<typename Library<T,U>::ItemContainer>::iterator it) :
    it_(it)
{ }

// TODO Q14
template <typename T, typename U>
typename Library<T,U>::Iterator::reference Library<T,U>::Iterator::operator*() const
{
    return it_->getItem();
}

// TODO Q18
template <typename T, typename U>
typename Library<T,U>::Iterator::pointer Library<T,U>::Iterator::operator->() const
{
    return &(operator*());
}

// TODO Q15
template <typename T, typename U>
bool Library<T,U>::Iterator::operator==(const Library<T,U>::Iterator& other) const
{
    return it_ == other.it_;
}

// TODO Q15
template <typename T, typename U>
bool Library<T,U>::Iterator::operator!=(const Library<T,U>::Iterator& other) const
{
    return !operator==(other);
}

// TODO Q13
template <typename T, typename U>
typename Library<T,U>::Iterator& Library<T,U>::Iterator::operator++()
{
    ++it_;
    return *this;
}


