#include <type_traits>
#include <memory>
#include <vector>
#include <algorithm>
#include <iostream>
#include <list>

//-----------------------------------------------------------------------------------
// Version where the iterator is a template based wrapper over an
// underlying std::vector<> iterator. The Library iterator has
// template parameters so that we can generate const and non-const
// versions of the iterator from the same class.
// -----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
// library to store objects of type T and descriptions of type U
//-----------------------------------------------------------------------------------

template <typename T, typename U>
class Library
{
public:
	// mutators to change the contents of the library
	void add(const T& item);
	unsigned int remove(const T& item);
	void addRelated(const T& from, const T& to, const U& desc);
	void printRelated(const T& from);
	bool inLibrary(const T& item);

private:

    //-------------------------------------------------------------------------------
	// private inner class to hold objects of type T in the library.
    //-------------------------------------------------------------------------------
	class ItemContainer
    {
	public:

		// constructor for creating a new ItemContainer to store an item
		ItemContainer(const T& item) : itemPtr{std::make_shared<T>(item)} {}

		T& getItem() { return *itemPtr; }
		const T& getItem() const { return *itemPtr; }
		void addRelated(const ItemContainer& to, const U& desc);
		std::shared_ptr<T> getItemPtr() const { return itemPtr; }
		void printRelated();

	private:

        //-------------------------------------------------------------------------------
		// private inner class to hold information about related works.
        //-------------------------------------------------------------------------------
		class RelatedWork
        {
		public:
			RelatedWork(const ItemContainer& link, const U& desc);
			void printItemAndDescription();
		private:
			// private data members of RelatedWork
			U relatedWorkDescription;
			std::shared_ptr<T> relatedWorkLink;
		};


		// private data members of ItemContainer
		std::shared_ptr<T> itemPtr;
		std::vector<RelatedWork> relatedWorks;
	};

	// private data member of Library Class
	std::vector<ItemContainer> items;


    //-----------------------------------------------------------------------------------
    // LibraryItemIterator class declaration
    //-----------------------------------------------------------------------------------

public:
    // We haven't done inheritance yet
/*
    class Iterator : public std::iterator<std::forward_iterator_tag,
                                          std::remove_const<T>::type,
                                          std::ptrdiff_t, T*, T&>
*/

    template<typename BaseType, typename IterType>
    class Iterator
    {
    public:

        typedef std::ptrdiff_t                              difference_type;

        // TODO - Q8. What iterator category tag?
        typedef std::forward_iterator_tag                   iterator_category;

        typedef typename std::remove_const<BaseType>::type  value_type;
        typedef BaseType*                                   pointer;
        typedef BaseType&                                   reference;

        typename Iterator::reference operator*() const;        // TODO Q14
        typename Iterator::pointer operator->() const;         // TODO Q18

        Iterator& operator++();  // TODO Q13
        // Should define postfix increment operator

        // TODO Q15
        bool operator==(const Iterator& other) const;
        bool operator!=(const Iterator& other) const;

        // TODO Q10

        Iterator(IterType it);
   private:
        IterType it_;
   };
public:

    // Iterator is simply a wrapper over an underlying
    // std::vector<ItemContainer> iterator (const and non-const)

    typedef Iterator<T, typename std::vector<ItemContainer>::iterator> iterator;
    typedef Iterator<const T, typename std::vector<ItemContainer>::const_iterator> const_iterator;

    // Declare the begin/end functions (and const versions)
	iterator begin();
	iterator end();
	const_iterator begin() const;
	const_iterator end() const;
	const_iterator cbegin() const;
	const_iterator cend() const;
};

//-----------------------------------------------------------------------------------
// Library member functions
//-----------------------------------------------------------------------------------

// method to add a new item to the library.
template <typename T, typename U>
void Library<T,U>::add(const T& item)
{
	ItemContainer newItem{item};
	items.push_back(newItem);
}

// method to add a related works to an existing item
template <typename T, typename U>
void Library<T,U>::addRelated(const T& from, const T& to, const U& desc)
{
    // find from itemContainer.
    auto fromIC = std::find_if(items.begin(), items.end(),
                               [&from] (const ItemContainer& ic)
                               {
                                   return from == ic.getItem();
                               });
    // find to
    auto toIC = std::find_if(items.begin(), items.end(),
                             [&to] (const ItemContainer& ic)
                             {
                                 return to == ic.getItem();
                             });
    // add desc
    fromIC->addRelated(*toIC,desc);
}

// method to remove an item from the library.
template <typename T, typename U>
unsigned int Library<T,U>::remove(const T& item)
{
	// TODO: remove the item from the library
	return items.size();
}

// method to print the related works for a given item.
template <typename T, typename U>
void Library<T,U>::printRelated(const T& from)
{
	// find from itemContainer.
	auto fromIC = std::find_if(items.begin(), items.end(),
                               [&from] (const ItemContainer& ic)
                               {
                                   return from == ic.getItem();
                               });
	fromIC->printRelated();
}

// method to check if an item is already in the library
template <typename T, typename U>
bool Library<T,U>::inLibrary(const T& item)
{
	auto ic = std::find_if(items.begin(), items.end(),
                           [&item] (const ItemContainer& ic)
                           {
                               return item == ic.getItem();
                           });
	if (ic == items.end()) return false;
	return true;
}


//-----------------------------------------------------------------------------------
// TODO - Q11 - Create begin() and end() member functions

// iterator begin
template <typename T, typename U>
typename Library<T,U>::iterator Library<T,U>::begin()
{
    return iterator{items.begin()};
}


template <typename T, typename U>
typename Library<T,U>::iterator Library<T,U>::end()
{
    return iterator{items.end()};
}

template <typename T, typename U>
typename Library<T,U>::const_iterator Library<T,U>::begin() const
{
    return const_iterator{items.begin()};
}


template <typename T, typename U>
typename Library<T,U>::const_iterator Library<T,U>::end() const
{
    return const_iterator{items.end()};
}

template <typename T, typename U>
typename Library<T,U>::const_iterator Library<T,U>::cbegin() const
{
    return begin();
}


template <typename T, typename U>
typename Library<T,U>::const_iterator Library<T,U>::cend() const
{
    return end();
}


//-----------------------------------------------------------------------------------
// Library::ItemContainer member functions
//-----------------------------------------------------------------------------------

// method to add a related works object to the item container.
template <typename T, typename U>
void Library<T,U>::ItemContainer::addRelated(const ItemContainer& to, const U& desc)
{
	RelatedWork rw{to,desc};
	relatedWorks.push_back(rw);
}

// method to print each related item in a container.
template <typename T, typename U>
void Library<T,U>::ItemContainer::printRelated()
{
	for (auto related : relatedWorks)
    {
		related.printItemAndDescription();
	}
}

//-----------------------------------------------------------------------------------
// Library::ItemContainer::RelatedWork member functions
//-----------------------------------------------------------------------------------

// constructor for a related works object
template <typename T, typename U>
Library<T,U>::ItemContainer::RelatedWork::RelatedWork(const ItemContainer& link, const U& desc) :
    relatedWorkDescription{desc}
{
	relatedWorkLink = link.getItemPtr();
}

// method to print the item and description of a related work.
template <typename T, typename U>
void Library<T,U>::ItemContainer::RelatedWork::printItemAndDescription()
{
	std::cout << *(relatedWorkLink) << " - " << relatedWorkDescription << std::endl;
}


//-----------------------------------------------------------------------------------
// LibraryItemIterator definitions
//-----------------------------------------------------------------------------------

// TODO Q10
template <typename T, typename U>
template<typename BT, typename IT>
Library<T,U>::Iterator<BT,IT>::Iterator(IT it) :
    it_(it)
{ }

// TODO Q14
template <typename T, typename U>
template<typename BT, typename IT>
typename Library<T,U>::template Iterator<BT,IT>::reference
Library<T,U>::Iterator<BT,IT>::operator*() const
{
    return it_->getItem();
}

// TODO Q18
template <typename T, typename U>
template<typename BT, typename IT>
typename Library<T,U>::template Iterator<BT,IT>::pointer
Library<T,U>::Iterator<BT,IT>::operator->() const
{
    return &(operator*());
}

// TODO Q15
template <typename T, typename U>
template<typename BT, typename IT>
bool
Library<T,U>::Iterator<BT,IT>::operator==(const Library<T,U>::Iterator<BT,IT>& other) const
{
    return it_ == other.it_;
}

// TODO Q15
template <typename T, typename U>
template<typename BT, typename IT>
bool
Library<T,U>::Iterator<BT,IT>::operator!=(const Library<T,U>::Iterator<BT,IT>& other) const
{
    return !operator==(other);
}

// TODO Q13
template <typename T, typename U>
template<typename BT, typename IT>
typename Library<T,U>::template Iterator<BT,IT>&
Library<T,U>::Iterator<BT,IT>::operator++()
{
    ++it_;
    return *this;
}



