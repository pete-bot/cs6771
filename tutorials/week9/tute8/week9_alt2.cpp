/**
 * Create a templated value-like library class to store books, movies etc
 * Each library item has the data and a set of related works
 * Each related work object has a ptr to the related works item.
 */

#include <sstream>
#include <iostream>
#include <typeinfo>
#include "Library_alt2.hpp"

//--------------------------------------------------------------------------
// class to store the description of an item.
//--------------------------------------------------------------------------
class Description
{
public:
	Description(const std::string& d) : desc{d} {}
	friend std::ostream& operator<<(std::ostream&, const Description &);
private:
	std::string desc;
};

// function to print out a description object
std::ostream& operator<<(std::ostream &os, const Description &d)
{
	os << d.desc;
	return os;
}

//--------------------------------------------------------------------------
// class to store a Book
//--------------------------------------------------------------------------
class Book
{
public:
	Book(const std::string& b) : name{b} {}
	friend bool operator==(const Book&, const Book &);
	friend std::ostream& operator<<(std::ostream&, const Book &);
private:
	std::string name;
};

// method to compare books
bool operator==(const Book& a, const Book & b)
{
	return a.name == b.name;
}

// method to print the book details
std::ostream& operator<<(std::ostream &os, const Book &b)
{
	os << b.name;
	return os;
}

//--------------------------------------------------------------------------
// main
//--------------------------------------------------------------------------

int main() {

	// create a book library
	Library<Book,Description> bookLibrary;

	// add some book objects
	Book harryPotter{"Harry Potter"};
	Book percyJackson{"Percy Jackson"};
	bookLibrary.add(harryPotter);
	bookLibrary.add(percyJackson);

	// create a description and add it between the two books
	Description d{"Percy Jackson is a similar childrens book that readers of Harry Potter may like"};
	bookLibrary.addRelated(harryPotter,percyJackson,d);

	// print books related to Harry Potter
	bookLibrary.printRelated(harryPotter);

	std::cout << std::boolalpha << "confirming that Percy Jackson is in library: "
              << bookLibrary.inLibrary(percyJackson) << std::endl;

    std::cout << std::endl << "----- New for week 9 -----" << std::endl << std::endl;

    //-------------------------------------------------------------------------
    // Testing the iterator
    //-------------------------------------------------------------------------


    // TODO Q12
	auto it = bookLibrary.begin();
    auto itEnd = bookLibrary.end();

    ++it;
    if (it != itEnd)
        std::cout << "Iterator output: " << *it << std::endl;

    // Iterator over a const version of the book to ensure that a
    // const iterator is used
    std::cout << "Const Iterator loop:" << std::endl;
    const auto& bookLibrary2 = bookLibrary;
    unsigned int bookid = 0;
    for (auto& book : bookLibrary2) {
        std::cout << "\t" << book << std::endl;

        // commented out -  produces compiler error because book& is const.
        // book = Book("New book"); 

        std::cout << book << std::endl;
    }

    // Iterate over non-const version so we can modify the values
    std::cout << "Non-Const Iterator loop:" << std::endl;
    bookid = 0;
    for (auto& book : bookLibrary) {
        std::stringstream ss;
        ss << "NewBook " << ++bookid;
        std::cout << "\t" << book << " => ";
        book = Book(ss.str());
        std::cout << book << std::endl;
    }


    // show that it is a forward iterator by updating the stored value.
    // and then reprinting the related works.
    std::cout << std::endl << "Showing that we can modify a deferenced iterator:" << std::endl;
    *it = Book("Lord of the Rings");
    bookLibrary.printRelated(harryPotter);

    std::cout << std::endl << "Returning a pointer: ";
    auto ptrTest = it.operator->();
    std::cout << ptrTest << " " << *ptrTest << std::endl << std::endl;

}
