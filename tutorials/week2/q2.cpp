// q2.cpp
// program to read in the output from teh previous question (from a file)
// take that data, read it into unsigned ints  in ascending order with the call by reference func.
// output data to term

// questions: is there a better way to parse inpout (what if the file does not
// have a specific format?)
// is this the correct way of passing by reference

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include "order.h"


void order3(unsigned int& a, unsigned int& b, unsigned int& c);


int main(int argc, char* argv[]){

	std::string line, dummyLineA, dummyLineB, dummyLineC;
	std::ifstream inputFile("q1.output");
	
	unsigned int a, b, c;

	if(inputFile.is_open()){

		// need more elegant way of doing this - regex maybe?
		while(inputFile >> dummyLineA >> dummyLineB >> dummyLineC >> a >> b >> c){
			
			// call function by reference - is thsi correct?
			
			// old version
			//order3(a, b, c);
			order3_ref(a, b, c);
			std::cout << a << ' ' << b << ' ' << c << std::endl;
		}

		inputFile.close();



	} else {
		std::cout << "Unable to open file." << std::endl;
	}

	return EXIT_SUCCESS;
}



/*
void order3(unsigned int& a, unsigned int& b, unsigned int& c) {
	if (a > c) {
		int t = a;
		a = c;
		c = t;
	}
	if (b > c) {
		int t = b;
		b = c;
		c = t;
	}
	if (a > b) {
		int t = a;
		a = b;
		b = t;
	}
}
*/