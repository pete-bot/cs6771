// q1.cpp
// program to take in three user entered values
// uses call-by-value function order3 to order the input numbers in descending order
// write the numbers out to a file
// test

// Questions - how vital are namespaces? what do they do (aside from limiting the scope)

// standard C libraries are available and are prefixed with c and have no .h suffix, ex: stdlib.h -> cstdlib
// #include <stdlib.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include "order.h"


// function definitions
void order3(int *a, int *b, int *c);

int main (int argc, char** argv ){
    // create some variables
    int input1, input2, input3;

    // output instruction to user via output stream cout (stdout)
    std::cout << "Please enter 3 numbers, separated by a space. ex: 1 2 3" << std::endl;


    // method1:
    // take in values via input stream cin (stdin)
    //( this expects a specific input from the user)
    std::cin >> input1 >> input2 >> input3; 
    
    // or std::cin >> input1 >> input2 >> input3;

    std::cout << "Your numbers were: " << input1 << ' ' << input2 << ' ' << input3 << std::endl;

    // old version
    //order3(&input1, &input2, &input3);
    order3_val(&input1, &input2, &input3);

    std::cout << "Writing to file..." << std::endl;
    std::cout << "Your sorted numbers (decending)are: " << input1 << ' ' << input2 << ' ' << input3 << std::endl;


    std::ofstream outputFile;
    outputFile.open("q1.output");
    
    if(outputFile.is_open()){
        outputFile << "Your numbers were: " << input1 << ' ' << input2 << ' ' << input3 << std::endl;
        //outputFile << input1 << ' ' << input2 << ' ' << input3 << std::endl;
        outputFile.close();

    } else {
        std::cout << "Unable to open file." << std::endl;
    }


    return EXIT_SUCCESS;
}
