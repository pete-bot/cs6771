/*
   Write a .cpp and .h(pp) file to do the following:
   Create a class representing a Book, the following code can be used to begin:
   class Book {

   Write the function definition for a constructor that takes values for name, author, isbn and price
   and uses a member initializer list to set the value of each data member.
   Write the overloaded == operator to compare if two book objects are identical
   Write the overloaded != operator that calls the already overloaded == operator to return true
   when two book objects are not identical.
   Write the overloaded << operator to print out the name, author, isbn and price of a book using std::cout
   Write the overloaded type conversion operator to enable the conversion of the Book class to
   a std::string in the form of "author, name"
   Create a main function that creates a std::vector<book>,
   add a few Book objects into the vector with different isbn numbers and prices
   Write the overloaded < operator to allow you to sort books by their isbn number.
   Test this in your main method using std::sort
   Call std::sort again with a lamda function as the predicate that sorts the books by price.
   */

#include <iostream>
#include <sstream>
#include <string>

class Book{
    public:

        // primary constructor
        Book(const std::string& name, const std::string& author, int isbn, double price);

        // book destructor
        ~Book();

        // conversion operator
        // no argument for this operator
        operator std::string() const;

        // getters
        int getIsbn() const { return isbn_; }
        double getPrice() const  { return price_; }



        // *** friend functions *** //

        // overlaod the << operator
        friend std::ostream& operator<<(std::ostream&, const Book&); // output overload

        // overload equality operator
        friend bool operator==(const Book& a, const Book& b);
        // overload not-equal operator, use the equality operator overload
        friend bool operator!=(const Book& a, const Book& b);
        // compare books by isbn number
        friend bool operator<(const Book& a, const Book& b);



    private:
        std::string name_;
        std::string author_;
        int isbn_;
        double price_;

        // static book reference count
        static unsigned int bookCount_;
};
