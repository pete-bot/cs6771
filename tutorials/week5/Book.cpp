// book class implementation

#include "Book.hpp"

// set static class variable
unsigned int Book::bookCount_ = 0;

Book::Book(const std::string& name, const std::string& author, int isbn, double price)
    : name_{name}, author_{author}, isbn_{isbn}, price_{price}
{
    ++bookCount_;
}

// book destructor
Book::~Book()
{
    --bookCount_;
}

// conversion operator
Book::operator std::string( ) const
{
    std::stringstream ss;
    ss << author_ << ", " << name_ << std::endl;
    return ss.str();

}


// *** friend functions *** //

// overlaod the << operator
std::ostream& operator<<(std::ostream& out, const Book& b)
{
    out <<"name: " << b.name_ << ", "
        <<"author: " << b.author_ << ", "
        <<"isbn: " << b.isbn_ << ", "
        << "price: " << b.price_ << ".";

    return out;
}


// compare books by isbn number
bool operator<(const Book& a, const Book& b)
{
    // need compare on the isbn number of book a vs book b
    return a.isbn_ < b.isbn_;
}


// overload equality operator
bool operator==(const Book& a, const Book& b )
{
    if( !(a.name_ == b.name_) ) return false;
    if( !(a.author_ == b.author_ ) ) return false;
    if( !(a.isbn_ == b.isbn_) ) return false;
    if( !(a.price_ == b.price_) ) return false;

    return true;
}

// overload not-equal operator, use the equality operator overload
bool operator!=(const Book& a, const Book& b)
{
    return !(a==b);
}
