// Car class
/*

   Create a constructor for the car that takes in the manufacturer name (e.g. Toyota) and the number of seats. Ensure that your constructor uses a member initializer list and uniform initialisation. Why is it important to use a member initializer list? Why is uniform initialisation prefered in C++11?
   Create a default constructor that delegates to the previous constructor using the values of "unknown" and 4
   Create const member functions to get the manufacturer and number of seats.
        What does it mean for a class or function to be const correct?
        - const is a tool for teh programmer to 1
    Create a static data member to keep count of the number of car objects created.
        Modify your constructors to ensure that the count increases when a new object is created.
        Do you need to increase the object count in your delegating constructor?
   Ensure that your static object count is initised to 0,
        where should you do this, in the header file or the cpp file?
   Create a static function to return the object count.
        What does it mean for an function or data member to be static?
        Is the static data member part of the object or the class?
   Create a destructor that decreases the object count when an object is destroyed
   Create a copy constructor, be sure to increase the object count.
   Create a move constructor, should you increase the object count too?
   Create a copy assignment operator, should the object count change?
   Create a move assignment operator, should the object count change?
   */

#ifndef CAR_H
#define CAR_H

#include <string>

class Car {
    public:
        // primary constructor
        Car(const std::string& manufacturer, unsigned int numSeats);

        // return a const reference to a string, manufacturer, from a const function
        // ie, it will notmodify the internals of the data structure
        const std::string& getManufacturer() const;

        // const func to return the number of seats in a car
        unsigned int getNumSeats() const;

        // function to return thenumber of car objects that have been created.
        static unsigned int getObjectCount();

        // destructor
        ~Car();

        // copy constructor
        Car(const Car&);

        // move constructor
        Car(Car&&);

        // copy assignment operator
        Car& operator=(const Car&);

        // move assignment operator
        Car& operator=(Car&&);




    private:

        // use the trailing '_' to differentiate between member variables and function params
        std::string manufacturer_;
        unsigned int numSeats_;

        // static variables keep their value even when they go our of scope
        // static object counter init in the cpp file
        static unsigned int numCars_;

};


#endif // CAR_H
