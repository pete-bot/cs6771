// cpp file for Car.h
// peter kydd

#include "Car.h"


// static initialisation
unsigned int Car::numCars_ = 0;

// primary constructor
// use initialiser lists to instantiate member vars. update the class objects
Car::Car(const std::string& manufacturer, unsigned int numSeats):
    manufacturer_{manufacturer}, numSeats_{numSeats}
{
    ++numCars_;
};

// default constructor
// delegates to primary constructor
Car::Car() : Car{"unknown", 4}{};

// destructor
Car::~Car()
{
    --numCars_;
}

// copy constructor
Car::Car(const Car& oldCar) : manufacturer_{oldCar.manufacturer_},  numSeats_{oldCar.numSeats_}
{
    ++numCars_;
}

// move constructor
Car::Car(Car&& oldCar): manufacturer_{std::move(oldCar.manufacturer_)}, numSeats_{oldCar.numSeats_}
{
    // we increment the object counter
    // when move operator is called, the destructor is also called - then the destructor
    // will decrement class object cont
    ++numCars_;
}

// copy assignment operator
// this will call the copy constructor to create
Car& Car::operator=(const Car& oldCar)
{
    manufacturer_ = oldCar.manufacturer_;
    numSeats_ = oldCar.numSeats_;

    // return a pointer to the new Car object
    return *this;
}

// move assignment operator
Car& Car::operator=(Car&& oldCar)
{
    manufacturer_ = std::move(oldCar.manufacturer_);
    numSeats_ = oldCar.numSeats_;

    return *this;

}

// return a const reference to a string, manufacturer, from a const function
// ie, it will notmodify the internals of the data structure
const std::string& Car::getManufacturer() const{ return manufacturer_;}

// const func to return the number of seats in a car
unsigned int Car::getNumSeats() const {return numSeats_;}

// function to return thenumber of car objects that have been created.
unsigned int Car::getObjectCount(){return numCars_;}


