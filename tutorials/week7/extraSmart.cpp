// week 7 tutorial questions

#include "variadicPrint.hpp"

// allocates space on the heap for the object being passed in
// returns a unique pointer to that object
template <typename T>
std::unique_ptr<T> make_unique(T t) {
	return std::unique_ptr<T>(new T(t));
}




template <typename T>
std::unique_ptr<T> make_unique(std::unique_ptr<T>& t){
	return std::unique_ptr<T>(new T(*t));
}


// return whatever object is passed in
template <typename T>
T getBase(T t) {
	return t;
}

// returns the base object, even if it is a pointer that is passed in 
template <typename T>
auto getBase(T *t) -> decltype(*t) {
	return *t;
}

// as above, handle unique_ptr
template <typename T>
auto getBase(std::unique_ptr<T>& p) -> decltype(*p) {
	return *p;
}





int main (int argc, char* argv[])
{

	// experiment to create a mem leak
	//auto dptr = new double(3.14);

	std::cout << "Testing pointer use on tamplates"<< std::endl;
	auto dptr = make_unique(3.14);
	std::cout << *dptr << std::endl;
	
	// clean up after ourselves
	// delete dptr;

	auto dptr2 = make_unique(dptr);
	std::cout << *dptr2 << std::endl;

	std::cout << std::endl;
	std::cout << "Testing return template fn."<< std::endl;

	int i = 9;
	int* iptr = &i;
	std::cout << getBase(i) << std::endl;
	std::cout << getBase(iptr) << std::endl;


	std::cout << "Testing return template fn. on a unique ptr"<< std::endl;
	std::cout << getBase(dptr) << std::endl;



	return EXIT_SUCCESS;
}