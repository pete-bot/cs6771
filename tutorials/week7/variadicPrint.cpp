// week 7 tutorial questions

#include "variadicPrint.hpp"


// function templates 
// general template
template <typename T>
bool printIfWholeNumber(const T&) 
{
	return false;
}

// specialised int template
// note the lack of a type in teh angle brackets ie <typename T>
template <> 
bool printIfWholeNumber<int>(const int& i) 
{
	std::cout << i;
	return true;
}

// create a specialised <vector> function template
template <typename T>
unsigned int printAndCountWholeNumbers(const std::vector<T>& v)
{
	int count = 0;
	for (auto element : v){

		// check if element is a whole value (there are otehr ways to do this - this seems best?)
		if(std::floor(element) == element){
			std::cout << element << " ";
			++count;
		}

	}
	return count;
}




// template that will call the appropriate function from above 
template <typename T>
unsigned int printAndCountWholeNumbers(const T& d) 
{
	if (printIfWholeNumber(d)) {
		std::cout << " ";
		return 1;
	}
	return 0;
}

// variadic function - takes in n params, evaluates recursively
template <typename T, typename... U>
unsigned int printAndCountWholeNumbers(T head, U... tail) 
{
	
	// call fn on head element
	unsigned int count = printAndCountWholeNumbers(head);
	count += printAndCountWholeNumbers(tail...);

	return count;

	// call a function to work out if the "head" is a whole number
	// call a function to process the tail
	// return the number of whole numbers in the variadic parameters
}


int main (int argc, char* argv[])
{
/*
	std::cout << "Testing variadic print on various types." << std::endl;
	std::cout << "double: " << printAndCountWholeNumbers(double(4.0)) << std::endl;
	std::cout << "char: " << printAndCountWholeNumbers('c') << std::endl;
	std::cout << "std::string: " << printAndCountWholeNumbers(std::string("test")) << std::endl;
	
	// this will print "4 int: 1", since the function is called and evaluated first
	std::cout << "int: " << printAndCountWholeNumbers(int(4)) << std::endl;
	//std::cout << "double: " << printAndCountWholeNumbers(double(4.0)) << std::endl;
*/

    std::cout << "Testing on set." << std::endl;
    auto c = printAndCountWholeNumbers(1, 2.5, 3, 4.5, 5.5, 6, 7.0, -5, "2" );
    std::cout <<  "count = " << c << std::endl;



    /*
	the following calls treat the vector as a single object, and therefore the 'correct'
	function template is never called above.
    */

    std::cout << "Testing on std::vector<double>" << std::endl;
    std::vector<double> d = {1.2, 32.0, 3.2, 5.30, 5.4, 6, -5};
	auto dc = printAndCountWholeNumbers(d);
	std::cout << "count = " << dc << std::endl;

	std::cout << "Testing on std::vector<unsigned int>" << std::endl;
    std::vector<unsigned int> vui = {65, 343, 21, 3};
	dc = printAndCountWholeNumbers(vui);
	std::cout << "count = " << dc << std::endl;




	return EXIT_SUCCESS;
}


